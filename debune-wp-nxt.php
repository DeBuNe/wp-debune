<?php

/*
Plugin Name: DeBuNe WP NXT
Plugin URI: http://wp-nxt.com
Description: DeBuNe-WP Plugin related to NXT and NXT-Like systems
Text Domain: debune-wp-nxt
Implemented
- Authentification in Wordpress by NXT token (from nxtAuth by Alexander - scor2k)
- Graphical reporting system for NXT and NXT-like accounts/assets
Version: 0.1
Update: 2015-03-04
Author: DeBuNe
Author Email: capodieci@gmail.com
License:

  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  
*/

//
// declaring or retreiving global variables for this plugin
//

global $wpdb;

/**
 * define the plugin path
 */
$phpVer = (int)substr(implode('',explode('.',phpversion())), 0, 3);
define(DEBUNE_PHPVERSION, $phpVer);

/**
 * define the plugin path
 */
define(DEBUNE_WP_PLUGIN_PATH, plugin_dir_path( __FILE__ ));

/**
 * define the plugin's text domain for all translations
 */
define(DEBUNE_WP_NXT_SLUG, 'debune-wp-nxt');

/**
 * define the plugin urls
 */
define(DEBUNE_WP_NXT_URL, plugins_url() . "/" . DEBUNE_WP_NXT_SLUG);
define(DEBUNE_WP_NXT_URL_MODELS, plugins_url() . "/" . DEBUNE_WP_NXT_SLUG . "/js/models/");
define(DEBUNE_WP_NXT_URL_CHART_MODELS, plugins_url() . "/" . DEBUNE_WP_NXT_SLUG . "/js/models/charts/");

/**
 * define table prefix for debune related data tables
 */
define(DEBUNE_TABLE_PREFIX, 'wp_');
define(WP_TABLE_PREFIX, $wpdb->prefix);

/**
 * early include Activerecord framework so it's available in all plugin classes!
 * @note see http://www.phpactiverecord.org/
 */
if ( !class_exists('Activerecord') ) {
    require_once __DIR__ . '/vendor/php-activerecord/ActiveRecord.php';
}

// load available cacher libraries
$phpLoadedExtensions = get_loaded_extensions();
if ( !class_exists('XCache') && in_array("XCache", $phpLoadedExtensions) ) {
    include_once('classes/cachers/XCache.php');
}
if ( !class_exists('DBCache') ) {
    include_once('classes/cachers/DBCache.php');
}

/**
 * note: the whole DeBuNe API can be overridden!
 */
if ( !class_exists('deBuNe_NXT_API') ) {
    include_once('classes/deBuNe_NXT_API.php');
}
/**
 * available chart functions
 * @note these functions are all packaged into a class for security reasons (only functions defined within this class can be executed!)
 */
if ( !class_exists('deBuNe_chart_functions') ) {
    include_once('classes/deBuNe_chart_functions.php');
}


include_once('functions/nxt-functions.php');
include_once('functions/wp-functions.php');
    
/**
 * Class containing wp-plugin's main logic:
 * plugin init
 * activerecord connection pool init
 * db schema init/update (db tables are created upon first plugin activation and updated every time below constant "debune_db_version" changes)
 * @note to avoid instantiating this class over and over when we want to access some functions, a global $DeBuNeWPNXT has been initialized and can be used 'application wide'
 */
class DeBuNeWPNXT {

	/*--------------------------------------------*
	 * Constants
	 *--------------------------------------------*/
        const name = 'DeBuNe WP NXT';
        const slug = DEBUNE_WP_NXT_SLUG;
        const debune_db_version = '3.3';
        const debune_table_prefix = "wp_";
	
	/**
	 * Constructor
	 */
	function __construct() {
            //register an activation hook for the plugin
            register_activation_hook( __FILE__, array( &$this, 'install_debune_wp_nxt' ) );
            // we use the uninstall.php procedure because this hook is not working..
            //register_uninstall_hook( __FILE__, array( &$this, 'uninstall_debune_wp_nxt' ) );
            
            // add a Hook to be able to extend this plugin
            do_action('debune_init_action');

            //Hook up to the init action
            add_action( 'init', array( &$this, 'init_debune_wp_nxt' ) );
            
            //Scripts that need to be loaded on specific contextes
            add_action( 'wp_print_scripts', array( &$this, 'load_page_scripts' ));            
	}
  
	/**
	 * Runs when the plugin is activated
	 */  
	function install_debune_wp_nxt() {
            // if no option has been defined it means it's the first time we activate this plugin
                $this->install_debune_tables();
                $this->insert_initial_data();

            // TODO as we don't know yet what needs to be updated when a new NRS API is released, this function in empty for now!
            if (get_site_option( 'debune_nxt_api_version' ) && get_site_option( 'debune_nxt_api_version' ) != deBuNe_NXT_API::nxt_API_version) {
                $this->update_debune_API();
            }
            
            // update debune options (wordpress)
            if (get_option('debune_db_version')) {
                update_option( 'debune_db_version', self::debune_db_version );
            } else {
                add_option( 'debune_db_version', self::debune_db_version );
            }
            if (get_option('debune_nxt_api_version')) {
                update_option( 'debune_nxt_api_version', deBuNe_NXT_API::nxt_API_version );
            } else {
                add_option( 'debune_nxt_api_version', deBuNe_NXT_API::nxt_API_version );
            }
	}
        
	/**
	 * Runs when the plugin is deleted from plugins admin page (uninstalled)
	 */  
	function uninstall_debune_wp_nxt() {
            // if no option has been defined it means it's the first time we activate this plugin
            $this->uninstall_debune_tables();
            $this->cleanup_wp_database();
            // update debune options (wordpress)
            delete_option('debune_db_version');
            delete_option('debune_nxt_api_version');
            delete_option('nxtHostname');
            delete_option('nxtHH');
            delete_option('nxtPort');
            delete_option('nxtUpdatePeers');
	}
        
        /**
         * update db tables and constants (API) if actual version differs from classes version
         */
        function debune_update_db_check() {
            global $DeBuNeNXT;
            if ( get_site_option( 'debune_db_version' ) != $DeBuNeNXT::debune_db_version ) {
                $DeBuNeNXT->update_debune_tables();
                update_option( 'debune_db_version', $DeBuNeNXT::debune_db_version );
            }

            if (get_site_option( 'debune_nxt_api_version' ) && get_site_option( 'debune_nxt_api_version' ) != deBuNe_NXT_API::nxt_API_version) {
                $this->update_debune_API();
                update_option( 'debune_nxt_api_version', deBuNe_NXT_API::nxt_API_version );
            }
        }
        
        function update_debune_tables() {
            // update debune tables
            $sql = "
                SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
                SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
                SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
                
                SET SQL_MODE=@OLD_SQL_MODE;
                SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
                SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
                ";
            
            $connection = $this->getDbConn('production');
            $connection->query($sql);
        }
        
        /**
         * TODO as we don't know yet what needs to be updated when a new NRS API is released, this function in empty for now!
         */
        function update_debune_API() {
        }
        
        /**
         * create required tables to run DeBuNe Plugin
         */
        function install_debune_tables() {
            // create debune tables
            $sql = "
                    SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
                    SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
                    SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

                    --
                    -- Table structure for table `".self::debune_table_prefix."debune_accounts`
                    --

                    CREATE TABLE IF NOT EXISTS `".self::debune_table_prefix."debune_accounts` (
                      `id` varchar(24) NOT NULL COMMENT 'accountRS',
                      `account` varchar(20) DEFAULT NULL COMMENT 'numeric account',
                      `publicKey` varchar(64) DEFAULT NULL COMMENT '64 bit pub key',
                      `balanceNQT` varchar(15) DEFAULT NULL,
                      `guaranteedBalanceNQT` varchar(15) DEFAULT NULL,
                      `created_at` datetime DEFAULT NULL,
                      `updated_at` datetime DEFAULT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='nxt account';

                    -- --------------------------------------------------------

                    --
                    -- Table structure for table `".self::debune_table_prefix."debune_assets`
                    --

                    CREATE TABLE IF NOT EXISTS `".self::debune_table_prefix."debune_assets` (
                      `id` varchar(20) NOT NULL COMMENT 'accountRS',
                      `asset` varchar(20) DEFAULT NULL COMMENT 'numeric account',
                      `accountRS` varchar(24) DEFAULT NULL COMMENT '64 bit pub key',
                      `account` varchar(20) DEFAULT NULL,
                      `created_at` datetime DEFAULT NULL,
                      `updated_at` datetime DEFAULT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='nxt asset';

                    -- --------------------------------------------------------

                    --
                    -- Table structure for table `".self::debune_table_prefix."debune_cache`
                    --

                    CREATE TABLE IF NOT EXISTS `".self::debune_table_prefix."debune_cache` (
                      `id` varchar(255) NOT NULL,
                      `cached_value` longtext,
                      `ttl` int(11) DEFAULT '0',
                      `blockchain_timestamp` varchar(45) DEFAULT NULL,
                      `created_at` datetime DEFAULT NULL,
                      `updated_at` datetime DEFAULT NULL
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table to permanently cache data from the blockchain';

                    -- --------------------------------------------------------

                    --
                    -- Table structure for table `".self::debune_table_prefix."debune_constants`
                    --

                    CREATE TABLE IF NOT EXISTS `".self::debune_table_prefix."debune_constants` (
                      `id` varchar(20) NOT NULL,
                      `constant_data` text
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='persistent blockchain and debune constants';

                    -- --------------------------------------------------------

                    --
                    -- Table structure for table `".self::debune_table_prefix."debune_transactions`
                    --

                    CREATE TABLE IF NOT EXISTS `".self::debune_table_prefix."debune_transactions` (
                      `id` varchar(30) NOT NULL COMMENT 'maps to transaction',
                      `senderRS` varchar(24) DEFAULT NULL,
                      `recipientRS` varchar(24) DEFAULT NULL,
                      `type` int(11) DEFAULT NULL,
                      `subtype` int(11) DEFAULT NULL,
                      `amountNQT` varchar(15) DEFAULT NULL,
                      `timestamp` int(11) DEFAULT NULL,
                      `fullHash` varchar(64) DEFAULT NULL,
                      `transactionIndex` int(11) DEFAULT NULL,
                      `confirmations` int(11) DEFAULT NULL,
                      `notes` text
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='transactions table';

                    --
                    -- Indexes for dumped tables
                    --

                    --
                    -- Indexes for table `".self::debune_table_prefix."debune_accounts`
                    --
                    ALTER TABLE `".self::debune_table_prefix."debune_accounts`
                      ADD PRIMARY KEY (`id`);

                    --
                    -- Indexes for table `".self::debune_table_prefix."debune_assets`
                    --
                    ALTER TABLE `".self::debune_table_prefix."debune_assets`
                      ADD PRIMARY KEY (`id`);

                    --
                    -- Indexes for table `".self::debune_table_prefix."debune_cache`
                    --
                    ALTER TABLE `".self::debune_table_prefix."debune_cache`
                      ADD PRIMARY KEY (`id`);

                    --
                    -- Indexes for table `".self::debune_table_prefix."debune_constants`
                    --
                    ALTER TABLE `".self::debune_table_prefix."debune_constants`
                      ADD PRIMARY KEY (`id`);

                    --
                    -- Indexes for table `".self::debune_table_prefix."debune_transactions`
                    --
                    ALTER TABLE `".self::debune_table_prefix."debune_transactions`
                      ADD PRIMARY KEY (`id`);
                ";
            $this->init_dbConn();
            $connection = $this->getDbConn('production');
            $connection->query($sql);
        }
        
        function insert_initial_data() {
            // import available online peers from peerexplorer
            $nxt = new deBuNe_NXT_API();
            if (!$nxt->updatePeers(false)) {
                $this->plugin_activation_error("cannot download list of available NRS peers. please check your Internet connection and that you can reach http://www.peerexplorer.com");
            }
            // all import hosts into db 
            $nxt->setHosts(true);
            // push NXT constants into the db (this happens only during plugin activation and NRS API release updates)
            global $nxtConstants;
            if (is_null($nxtConstants)) {
                $nxtConstants = new nxt_constants();
            }
            $nxtConstants->updateConstants();
        }
        
        /**
         * delete plugin's tables
         */
        function uninstall_debune_tables() {
            // create debune tables
            $sql = "
                    SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
                    SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
                    SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
                    
                    DROP TABLE  ".self::debune_table_prefix."debune_accounts, 
                                ".self::debune_table_prefix."debune_assets, 
                                ".self::debune_table_prefix."debune_cache, 
                                ".self::debune_table_prefix."debune_constants, 
                                ".self::debune_table_prefix."debune_transactions;

                    SET SQL_MODE=@OLD_SQL_MODE;
                    SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
                    SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
                ";
            $this->init_dbConn();
            $connection = $this->getDbConn('production');
            $connection->query($sql);
        }
        
        /**
         * cleanup all WordPress plugin's associated options
         */
        private function cleanup_wp_database() {
            // update debune options (wordpress)
            delete_option('debune_db_version');
            delete_option('debune_nxt_api_version');
            delete_option('nxtHostname');
            delete_option('nxtHH');
            delete_option('nxtPort');
            delete_option('nxtUpdatePeers');
        }
        
        /**
         * init db connection through AR and autoload models
         * @global type $wpdb
         * @staticvar type $conn
         * @return type
         */
        function init_dbConn() {
                global $wpdb;
                static $conn;
                if ($conn === null) {
                    // initialize Activerecord db connection and autoload AR classes (models)
                    $connections = array(
                            'development' => 'mysql://invalid',
                            'production' => 'mysql://'.$wpdb->dbuser.':'.$wpdb->dbpassword.'@'.$wpdb->dbhost.'/'.$wpdb->dbname,
                            'dbcache' => 'mysql://'.$wpdb->dbuser.':'.$wpdb->dbpassword.'@'.$wpdb->dbhost.'/'.$wpdb->dbname);
                    $conn = ActiveRecord\Config::initialize(function($cfg) use ($connections) {
                        $cfg->set_model_directory(__DIR__ . '/classes/ar-models');
                        $cfg->set_connections($connections);
                    });
                }
                return $conn;
        }

        /**
         * get a db connection (can be used to run PDO queries instead of using AcriveRecord)
         * @param type $connName name of connection chosen within the connection's pool (see init_dbConn function)
         */
        function getDbConn($connName) {
            $ar_adapter = ActiveRecord\ConnectionManager::get_connection($connName);
            return $ar_adapter->connection;
        }
  
	/**
	 * Runs when the plugin is initialized
	 */
	function init_debune_wp_nxt() {
                if (!defined('DEBUNE_DEBUG')) define( 'DEBUNE_DEBUG', false );
                
                /*
                 *  session management: always use sessions with ajax calls
                 */
                session_start();  // always start (or restart a session)
                if (!(defined('DOING_AJAX') && DOING_AJAX)) {
                    // restart the session every time we entirely reload a page (ergo, all ajax calls within the page can share the same session)
                    foreach ($_SESSION as $key => $var) {
                        unset($_SESSION[$key]);
                    }                    
                }
            
                // Setup localization
                load_plugin_textdomain( DEBUNE_WP_NXT_SLUG, false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

                // init db connection and autoload AR models
                global $debuneDBConnections;
                $debuneDBConnections = $this->init_dbConn();
                
                /**
                 * put nxtConstants into a global variable so we don't have to create/destroy a new object all the time
                 */
                global $nxtConstants;
                $nxtConstants = new nxt_constants();
                
                // Load JavaScript and stylesheets
                $this->register_scripts_and_styles();
                
                // Register the shortcode [debune_nxt_chart]
                add_shortcode( 'debune', array( &$this, 'render_shortcode' ) );

                if ( is_admin() ) {
                        //this will run when in the WordPress admin
                } else {
                        //this will run when on the frontend
                }
                
                // Update db schema if required
                $this->debune_update_db_check();
	}
        
	function render_shortcode($atts) {
		// Extract the attributes
		extract(shortcode_atts(array(
			'output' => '',
			'objtype' => '',
			'id' => '',
			'attributes' => '',
			'formatted' => '',
                          // the chart configuration array must contain a json object:
                          // {
                          //    "model": "chart model's name (eg. highstock-candle-volume)"
                          // }
                          'title' => '',
                          'chartoptions' => '',
			), $atts));
		// you can now access the attribute values using $attr1 and $attr2
                switch ($atts['output']) {
                    case 'attribute':
                        $shortcode = $this->render_single_attribute($atts);
                        break;
                    case 'chart':
                        $shortcode = $this->render_chart($atts);
                        break;

                    default:
                        $shortcode = "this shortcode output does not exists!";
                        break;
                }
                return $shortcode;
	}
        
        /**
         * function to render a single NXT attribute using a shortcode
         * @param type $objType
         * @param type $objID
         * @param type $attribute
         * @param type $formatted if we want data to be formatted according to their type
         * @return type
         */
        private function render_single_attribute($params) {
                $objType = sanitize_text_field($params['objtype']);
                $objID = sanitize_text_field($params['id']);
                $attribute = sanitize_text_field($params['attributes']);
                $formatted = sanitize_text_field($params['formatted']);
            
                $className = "debune_".$objType;
                if ($model = $className::findWithBlockData(array($objID),array($objType=>$objID))) {
                    if (!is_null($formatted)) {
                        $res = $model->formattedAttribute($attribute);
                    } else {
                        $res = $model->$attribute;
                    }
                } else {
                    $res = __( 'Object not found. probably a connection error with the blockchain',DEBUNE_WP_NXT_SLUG );
                }
                return $res;
        }
        
        /**
         * function to render a single NXT attribute using a shortcode
         * @param type $objType
         * @param type $objID
         * @param type $attribute
         * @param type $formatted if we want data to be formatted according to their type
         * @return type
         */
        private function render_chart($params) {
                // here add the html structure for the chart
                // 1. one div for the chart container (div id = $objID class = debune-highchart)
                // 2. every parameter as a hidden text field
                // note: the plugin, on document ready, checks if on the page exists an element with class debune-highchart and initializes the chart
                // note2: as we could have more than one chart in the same page, every chart must be named (global js variable or array with all charts) with the dom id of its container
                $objType = sanitize_text_field($params['objtype']);
                $objID = sanitize_text_field($params['id']);
                $attribute = sanitize_text_field($params['attributes']);
                // can be 'Yes' or any other text (will be considered = 'No')
                $formatted = sanitize_text_field($params['formatted']);
                $chartOptions = sanitize_text_field($params['chartoptions']);
                $title = sanitize_text_field($params['title']);
                
                $attributes = explode('|',$attribute);
                // this hash array contains all options to select and customize the chart
                // standard options are:
                // - 'model' the chart's json model name (eg. 'highstock-candle-volume')
                // - 'width' the chart's container width
                // - 'height' the chart's container height
                $chartOptions = json_decode($chartOptions, true);

                // map object name ('account', 'asset') to its AR class name
                switch ($objType) {
                    case 'account':
                        $className = "debune_account_transactions";
                        break;

                    default:
                        return __( 'this objType does not exists',DEBUNE_WP_NXT_SLUG );
                }
                
                //TODO add here filters
                $chartQueryParams = array(
                    'className' => $className,
                    'attributes' => $attributes,
                    'filter' => ''
                );
                // TODO json object containing a custom chart configuration
                $chartConfig = array();
                
                // this couldn't be included in chartoptions directly from the shortcode because if the string contains special chars like quotes, it screws everything up
                $chartOptions['title'] = $title;
                
                $params = array(
                    'chartID' => $objID. "_" . uniqid(), // adding an uniqueid to the DOM id, guarantees uniqueness of this chart (eg. if we want to put two different charts for the same NXT account on the same page)
                    'formatAttributes' => $formatted,
                    'chartQueryParams' => $chartQueryParams,
                    'chartConfig' => $chartConfig,
                    'chartOptions' => $chartOptions
                    
                );
                
                return $this->chart_view($params);
        }
        
        /**
         * html template for charts
         * @note it must be returned as an html string and not echoed as real html (rule for shortcodes)
         * @param type $params
         * @return string
         */
        private function chart_view($params) {
            $html = '<div class="debune-chart" style="display: block">';
            $html .= '  <div style="width: '. $params['chartOptions']['width'].'; height: '.$params['chartOptions']['height'].';" id="'.$params['chartID'].'"></div>';
            $html .= '  <div class="debune-credits">';
            $html .= '      <img src="'.DEBUNE_WP_NXT_URL.'/css/DeBuNe_logo_32x32.png" alt="DeBuNe logo" height="12" width="12">';
            $html .= '      <a href="http://www.debune.org" target="_new">DeBuNe.org</a>';
            $html .= '  </div>';
            $html .= '  <script type="text/javascript">';
            $html .= '      jQuery(document).ready(function(){';
            $html .= '          jQuery("#'.$params['chartID'].'").debunejQChart('.encodeArray($params).');';
            $html .= '      });';
            $html .= '  </script>';
            $html .= '</div>';
            return $html;
        }
        
        /**
         * load page specific scripts (eg. angular apps) and css only in relative pages!
         */
        function load_page_scripts() {
            if ( is_admin() ) {
                $screen = get_current_screen();
                // Angular Apps
                switch ($screen->id) {
                    case "debune_chart":
                        $this->load_file( DEBUNE_WP_NXT_SLUG . '-angular-debuneTransactionsGrid', '/js/ng-apps/ng-debuneTransactionsGrid.js', true, array(DEBUNE_WP_NXT_SLUG . 'angularjs') );
                        break;
                    case "tools_page_debune-shortcode-generator":
                    case "post":
                    case "page":
                        $this->load_file( DEBUNE_WP_NXT_SLUG . '-angular-debuneShortcodes', '/js/ng-apps/ng-debuneShortcodes.js', true, array(DEBUNE_WP_NXT_SLUG . 'angularjs') );
                        break;

                    default:
                        break;
                }

            }

        }
        
	/**
	 * Registers and enqueues stylesheets for the administration panel and the
	 * public facing site.
	 */
	private function register_scripts_and_styles() {
            $ngversion = "1.3.15";
            $minified = DEBUNE_DEBUG ? '' : '.min';
            /*
             * TODO theoretically we should get all known frameworks from a CDN, but often CDNs are down, so we've downloaded all locally...
             * CDN sucks!
            wp_enqueue_script('lodash', "https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js",array('jquery'));
            wp_enqueue_script('angularjs', "https://ajax.googleapis.com/ajax/libs/angularjs/{$ngversion}/angular{$minified}.js",array('jquery'));
            wp_enqueue_script('angularjs-touch', "https://cdnjs.cloudflare.com/ajax/libs/angular.js/{$ngversion}/angular-touch{$minified}.js",array('angularjs'));
            wp_enqueue_script('angularjs-animate', "https://cdnjs.cloudflare.com/ajax/libs/angular.js/{$ngversion}/angular-animate{$minified}.js",array('angularjs'));
             * 
             */
            
            // js frameworks and helpers
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'lodash', "/vendor/js/lodash/lodash{$minified}.js", true );
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'ng-lodash', "/vendor/js/ng-lodash/ng-lodash{$minified}.js", true, array(DEBUNE_WP_NXT_SLUG . 'angularjs') );
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'angularjs', "/vendor/js/angular-{$ngversion}/angular{$minified}.js", true );
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'angularjs-touch', "/vendor/js/angular-{$ngversion}/angular-touch{$minified}.js", true, array(DEBUNE_WP_NXT_SLUG . 'angularjs') );
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'angularjs-animate', "/vendor/js/angular-{$ngversion}/angular-animate{$minified}.js", true, array(DEBUNE_WP_NXT_SLUG . 'angularjs') );
            //$this->load_file( DEBUNE_WP_NXT_SLUG . 'angularjs-route', "/vendor/js/angular-{$ngversion}/angular-route{$minified}.js", true, array(DEBUNE_WP_NXT_SLUG . 'angularjs') );
            
            $this->load_file( DEBUNE_WP_NXT_SLUG . '-angular-ui-grid', '/vendor/js/ui-grid/ui-grid-unstable.js', true, array(DEBUNE_WP_NXT_SLUG . 'angularjs') );
            $this->load_file( DEBUNE_WP_NXT_SLUG . '-angular-ui-grid-css', '/vendor/js/ui-grid/ui-grid-unstable.css');
            // Angular Services
            $this->load_file( DEBUNE_WP_NXT_SLUG . '-angular-wp-services', '/js/ng-apps/services/ng-wp-services.js', true, array(DEBUNE_WP_NXT_SLUG . 'angularjs') );
            
            //wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css');
            $this->load_file( DEBUNE_WP_NXT_SLUG . '-font-awesome', "/css/font-awesome{$minified}.css" );
            
            // global javascript (for all pages/interfaces)
            $this->load_file( DEBUNE_WP_NXT_SLUG . '-global-script', '/js/global.js', true );
            $this->load_file( DEBUNE_WP_NXT_SLUG . '-global-css', '/css/global.css' );
            $postID = isset($_GET['post']) ? sanitize_key($_GET['post']) : '';
            $this->localize_debune_script(DEBUNE_WP_NXT_SLUG, '-global-script', array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'ngpath' => DEBUNE_WP_PLUGIN_PATH . '/js',
                'wp_post' => array('ID'=>$postID),
                'wp_nonce' => wp_create_nonce( 'debune-ajax-nonce' ),
                'DEBUNE_DEBUG' => DEBUNE_DEBUG ? 1 : 0,
              ));

            // highcharts framework
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'highstock', "/vendor/js/highstock/highstock.js", true, array('jquery') );
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'highstock-theme', "/vendor/js/highstock/themes/grid.js", true, array(DEBUNE_WP_NXT_SLUG . 'highstock') );
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'highstock-exporting', "/vendor/js/highstock/modules/exporting.js", true, array(DEBUNE_WP_NXT_SLUG . 'highstock') );
            // load and localize the chart jQuery plugin
            $this->load_file( DEBUNE_WP_NXT_SLUG . 'debune-jqchart', "/js/jQuery-plugins/debune-jqchart.js", true, array(DEBUNE_WP_NXT_SLUG . 'highstock') );
            
            if ( is_admin() ) {
                    // admin only js
                    $this->load_file( DEBUNE_WP_NXT_SLUG . '-admin-script', '/js/admin.js', true );
                    $this->load_file( DEBUNE_WP_NXT_SLUG . '-admin-style', '/css/admin.css' );
                    /*
                     * STEF uncomment to localize this script
                    $this->localize_debune_script(DEBUNE_WP_NXT_SLUG, 'admin-script', array(
                        'param1' => 'aa',
                        'param2' => 'bb',
                    ));
                     * 
                     */
            } else {
                    // front end only js
                    $this->load_file( DEBUNE_WP_NXT_SLUG . '-script', '/js/widget.js', true );
                    $this->load_file( DEBUNE_WP_NXT_SLUG . '-style', '/css/widget.css' );
            } // end if/else
	} // end register_scripts_and_styles
        
        /**
         * helper function to localize a script (pass a php array of parameters to the js upon script initialization)
         * @param type $script_prefix
         * @param type $scriptName
         * @param type $config_array
         */
        private function localize_debune_script($script_prefix, $scriptName, $config_array){
            wp_localize_script($script_prefix.$scriptName, 'localizedData', $config_array);
        }
	
	/**
	 * Helper function for registering and enqueueing scripts and styles.
	 *
	 * @name	The 	ID to register with WordPress
	 * @file_path		The path to the actual file
	 * @is_script		Optional argument for if the incoming file_path is a JavaScript source file.
	 */
	private function load_file( $name, $file_path, $is_script = false, $depends = array('jquery') ) {
            $url = plugins_url($file_path, __FILE__);
            $file = plugin_dir_path(__FILE__) . $file_path;

            if( file_exists( $file ) ) {
                if( $is_script ) {
                        wp_register_script( $name, $url, $depends, true );
                        wp_enqueue_script( $name );
                } else {
                        wp_register_style( $name, $url, array(), true );
                        wp_enqueue_style( $name );
                } // end if
            } // end if
	} // end load_file

        /**
         * a way to gracefully die if plugin cannot be activated due to an error (eg. no network connection)
         * @param type $errorMsg
         */
        private function plugin_activation_error($errorMsg) {
            deactivate_plugins( basename( __FILE__ ) );
            wp_die('<p>The <strong>DeBune WP NXT</strong> plugin cannot be activated because '.$errorMsg,  array( 'response'=>200, 'back_link'=>TRUE ) );
        }
  
} // end class

// declare a global variable and instanciate a DeBuNe Class Object
global $DeBuNeNXT;
$DeBuNeNXT = new DeBuNeWPNXT();
