/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */


/**
 * 
 * @param {type} $
 * @returns {undefined}
 */
(function($){
    "use strict";

    /* FIX TRIM FOR IE8 */
    if ( typeof String.prototype.trim !== 'function' ) {
            String.prototype.trim = function() {
                    return this.replace(/^\s+|\s+$/g, '');
            };
    }
    
    /**
     * get all objects for a NXT object type (eg. account)
     * @param {type} nxtObjType "account" or "asset"
     * @param {type} verifyOnly true/false if set to true, only the existence of the object into the blockchain will be returned (true or false)
     * @param {type} ajaxLoaderAfter DOM object after which we want to add the ajax loader (while performing the ajax call)
     * @param {type} callBackSuccess(data) function to be called when ajax request succeed: note it passes the resulting data
     * @param {type} callBackFail(errorThrown) function to be called when ajax request fails: note it passes the error data
     * @returns {jqXHR} the jqXR object (eg. in case we want control over all pending ajax calls)
     */
    var debuneGetNXTObjects = function( nxtObjID, nxtObjType, verifyOnly, ajaxLoaderAfter, callBackSuccess, callBackFail ) {
        if (typeof nxtObjType !== "undefined") {
            var jqXHR = $.ajax({
                url: ajaxurl,
                type: "POST",
                data: {
                    'action':'debuneGetTypes_ajax_request',
                    'nxtObjType' : nxtObjType,
                    'nxtObjID' : nxtObjID,
                    'verifyOnly' : verifyOnly === true ? 1 : 0
                },
                beforeSend: function (xhr) {
                    if ($("#debuneAccountMetaboxSpinner").length === 0) {
                        ajaxLoaderAfter.after( '<div id="debuneAccountMetaboxSpinner" class="spinner" style="display: inherit;"></div>' );
                    } else {
                        $( "#debuneAccountMetaboxSpinner" ).show();
                    }
                },
                complete: function (jqXHR, textStatus) {
                    //$( "#debuneAccountMetaboxSpinner" ).hide();
                },
                success:function(data) {
                    if ($.isFunction(callBackSuccess)) {
                        callBackSuccess(data);
                    } else {
                        $( "#debuneAccountMetaboxSpinner" ).hide();
                    }
                },
                error: function(jqXHR, exception){
                    if ($.isFunction(callBackFail)) {
                        callBackFail(jqXHR, exception);
                    } else {
                        $( "#debuneAccountMetaboxSpinner" ).hide();
                        console.log(exception);
                    }
                }
            });
            return jqXHR;
        }
        return null;
    };
    
    /**
     * populates form fields and toggle the chart metaboxes
     * @param {type} nxtObjType
     * @returns {undefined}
     */
    var toggleDebuneChartMetaboxes = function(nxtObjID, nxtObjType) {
        switch($("#debune_chart_obj_type").val()) {
            case "asset":
                //show asset metabox and hide account metabox
                $("#debune-chart-account-meta-box").hide();
                $("#debune-chart-asset-meta-box").show();
                $( "#debuneAccountMetaboxSpinner" ).hide();
                break;
            case "account":
                $("#debune-chart-account-meta-box").show();
                $("#debune-chart-asset-meta-box").hide();
                $( "#debuneAccountMetaboxSpinner" ).hide();
                break;
            default:
                //hide both metaboxes
                $("#debune-chart-account-meta-box").hide();
                $("#debune-chart-asset-meta-box").hide();
        }
    };

    /**
     * update metaboxes when one of the input fields changes
     * @returns {undefined}
     */
    var updateMetaBoxes = function() {
            var chart_obj_id = $("#debune_chart_obj_id").val(); // account or asset ID
            var chart_obj_type = $("#debune_chart_obj_type").val();// selected ojg type (account or asset)
            //validate the account or asset (only its existance)
            debuneGetNXTObjects(chart_obj_id, chart_obj_type, true, $( "#debune_chart_obj_type" ), 
            //success callback
            function(data){
                if (data.success == 'no') {
                    popupAlert(data.msg);
                    $("#debuneAccountMetaboxSpinner").hide();
                    //hide both metaboxes
                    $("#debune-chart-account-meta-box").hide();
                    $("#debune-chart-asset-meta-box").hide();                                
                } else {
                    //fill in the dropdown with resulting data
                    if (data.notexists) {
                        alert("this "+chart_obj_type+" doesn't exist in the blokchain..");
                        //hide both metaboxes
                        $("#debune-chart-account-meta-box").hide();
                        $("#debune-chart-asset-meta-box").hide();                                
                    } else {
                        toggleDebuneChartMetaboxes(chart_obj_id, chart_obj_type);
                    }
                }
            },
            //failed callback
            function(jqXHR, exception){
                if (!exception === 'abort') {
                    alert(exception);
                } else {
                    jsLog("AJAX request: "+exception);
                }
            });
    };
    
    /**
     * all functions for chart admin page
     * @returns {undefined}
     */
    var debuneAdminChartPage = function() {
        // init the metaboxes for debune-chart post type
        var chart_obj_type_box = $("#debune_chart_obj_type").parent('p');
        var obj_type = $("#debune_chart_obj_type").val();
            
        if (obj_type === "" || obj_type === null) {
            chart_obj_type_box.hide();
            $("#debune-chart-asset-meta-box").hide();
            $("#debune-chart-account-meta-box").hide();
        } else {
            if (obj_type === "account") {
                $("#debune-chart-asset-meta-box").hide();
            } else if (obj_type === "asset"){
                $("#debune-chart-account-meta-box").hide();
            }
        }

        // show metaboxes only if user put something into NXT ID field
        $("#debune_chart_obj_id").change(function(){
            if ($(this).val() !== "") {
                chart_obj_type_box.show();
                if ($("#debune_chart_obj_type").val() !== "" && $("#debune_chart_obj_type").val() !== null) {
                    updateMetaBoxes();
                }
            }
        });

        $("#debune_chart_obj_type").change(function(){
            updateMetaBoxes();
        });
        
        if ($(".transactionTypes-container")) {
            $(".transactionTypes-container").multiselectFromUl(true);
        }
    };
    
    

    /**
     * do the job when you are ready!
     */
    $(document).ready( function($) {
        /*
        if($('#debune_chart_obj_type').length !== 0) {
            debuneAdminChartPage();
        }
        */
    });    
})(jQuery);