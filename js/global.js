/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */


/*
 * GLOBAL VARIABLES HERE
 */

var DEBUNE_DEBUG = parseInt(localizedData.DEBUNE_DEBUG);

// absolute path to javascript files
var ngpth = localizedData.ngpath;

var wpNonce = localizedData.wp_nonce;

var globaljQXHR = [];

/* redeclare ajaxURL to include full URL (standard one includes only relative path)*/ 
var ajaxurl = localizedData.ajaxurl;

/* declare current post attributes for javascript functions to be able to get it effortlessly. eg. post.ID */
var wpPost = localizedData.wp_post;


/**
 * returns the client timezone offset in milliseconds
 * @note to be added to UTC formatted javascript timestamps
 * @returns {Number}
 */
function getTimeZoneOffsetMs (){
    return parseInt(- new Date().getTimezoneOffset() * 60 * 1000); // Note the reversed sign!
};

/**
 * returns human readable current client timezone offset in minutes (eg. +0800) 
 * @returns {String|Number|pad.str}
 */
function getTimeZoneOffsetHumanReadable (){
    var offset = new Date().getTimezoneOffset();
    offset = ((offset<0? '+':'-')+ // Note the reversed sign!
    pad(parseInt(Math.abs(offset/60)), 2)+
    pad(Math.abs(offset%60), 2));
    return offset;
};
function pad(number, length){
    var str = "" + number;
    while (str.length < length) {
        str = '0'+str;
    }
    return str;
}

/**
 * better formatted alert box
 * @param {type} msg
 * @returns {undefined}
 */
function popupAlert(msg) {
    setTimeout(function(){ alert(msg); }, 300);
};

/**
 * general purpose logging utility
 * @param {type} contents
 * @param {type} where (default 'console')
 * @returns {undefined}
 */
function jsLog(contents, where) {
    // never log anything when in production mode
    if (!DEBUNE_DEBUG) return;
    
    where = typeof where !== "undefined" ? where : 'console';
    switch(where) {
        case 'console':
            console.log(contents);
            break;
        default:
    }
};

function abortAllAjaxRequests() {
    globaljQXHR.forEach(function(element) {
      jsLog('element aborted:', element);
      element.abort();
    });    
};

/*
 * GLOBAL FUNCTIONS HERE
 */


/**
 * 
 * @param {type} $
 * @returns {undefined}
 */
(function($){
    "use strict";

    /* FIX TRIM FOR IE8 */
    if ( typeof String.prototype.trim !== 'function' ) {
            String.prototype.trim = function() {
                    return this.replace(/^\s+|\s+$/g, '');
            };
    }
    
    /**
     * Create a multiple select box from a (2 levels) unordered list (ul)
     * @param {type} divClass DIV class containing the ul
     * @param {type} ulWrapperClass class attached to first (root) ul element
     * @note the template must be in this format:
     * 
     *               <div class="mob-subpages">
     *               <div id='mob-subpages-name' style='display:none'>debune_chart_account_transactionTypes</div>
     *               <div id='mob-subpages-name-label' style='display:none'>selectbox label</div>
     *               <div id='mob-subpages-name-optionselected' style='display:none'>string containing an array of selected values (elements must be delimited by |)</div>
     *               <ul class="wrapper" style="display:none">
     *                   <li><a href="#">Section 1</a> 
     *                   <ul>
     *                           <li><a href="#">Section 1.1</a></li>
     *                       </ul></li>
     *                   <li><a href="#">Section 2</a>
     *                       <ul>
     *                           <li><a href="#">Section 2.1</a></li>
     *                           <li><a href="#">Section 2.2</a></li>
     *                           <li><a href="#">Section 2.3</a> </li>
     *                       </ul>
     *                   </li>
     *                   <li><a href="#">Section 3</a></li>
     *               </ul><br/><br/>
     *               </div>
     * 
     * 
     * @returns {undefined}
     */
    $.fn.multiselectFromUl = function() {
        // Create the dropdown base
        var divClass = $(this).attr('class');
        var ulWrapperClass = $(this).find('ul:first').attr('class');
        var selectName = $('#'+divClass+'-name').text();
        var selectLabel = $('#'+divClass+'-label').text();
        var optionSelected = $('#'+divClass+'-optionselected').text().split('|');
        
        $("<label style='display:block' for='"+selectName+"'>"+selectLabel+"</label>").appendTo("."+divClass);
        $("<select ng-model='objAttributes' multiple='multiple' name='"+selectName+"[]' id='"+selectName+"' />").appendTo("."+divClass);
        var opt = "";
        $("."+ulWrapperClass).children('li').each(function(){

            if($(this).find('ul').length !== 0){
                opt += "<optgroup label="+"'"+$(this).children().html()+"'"+">";
                $(this).find('li').each(function(){
                    var optVal = $(this).find('a').html();
                    var optSel = $.inArray(optVal, optionSelected) === -1 ? '' : 'selected';
                    opt += "<option "+optSel+" value='"+optVal+"'>"+optVal+"</option>";
                });
                opt += "</optgroup>";
            }else{
                opt += "<option value='"+$(this).children().html()+"'>"+$(this).children().html()+"</option>";
            }
            //jsLog($(this).find('ul').length !== 0);
        });
        //jsLog(opt);
        $("."+divClass+" select").append(opt);
    };
    
    /**
     * fill a dropdown menu with data coming from json array of single values pairs (key => value): key = option value; value = option text or a regular js array
     * @param {type} data
     * @param {type} keyIsValue (default false) if set to true, option values will be = option text
     * @returns {undefined}
     */
    $.fn.populateDropDown = function( data, keyIsValue ) {
        if (typeof keyIsValue === "undefined") {
            keyIsValue = false;
        }
        if (!data instanceof Array) {
            var data = $.parseJSON(data);
        }
        
        var thisDOM = $(this);
        // first remove all options from input select
        thisDOM
            .find('option')
            .remove()
            .end();
        $.each(data, function(key, value){
            var idx = key;
            if (keyIsValue) {
                idx = value;
            }
            thisDOM.append("<option value='"+idx+"'>" + value  + "</option>");
        });
    };
    

    $(document).ready(function(){
    });
})(jQuery);