/* 
 * AngularJs Services to make wp live with Angular
 */
var ngWpServices = angular.module('ngWpServices',[]);
ngWpServices.service("httpService",  [ '$http', '$q', function($http, $q) {
    var serviceFunc = {
        get_content: function(href)
        {
            var request = $http({
                method: "get",
                url: href
            });

            return request;
        },
        post_content: function(data)
        {
            var canceller = $q.defer();

            var cancel = function(reason){
                canceller.resolve(reason);
            };
            
            var promise = $http({
                timeout: canceller.promise,
                 method: "post",
                 url: window.ajaxurl,
                 data: jQuery.param(data),
                 headers: {
                      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
                 }
            }).then(function(response){
                   return response;
            });
            
            return {
                promise: promise,
                cancel: cancel
            };
        },
        // this helper function will check if an object exists in the blockchain
        obj_exist: function(objID, objType)
        {
            var data = {
                action: 'debuneVerifyObjExists_ajax_request',
                nonce: window.wpNonce,
                nxtObjType : objType,
                nxtObjID: objID
            };
             var request = $http({
                  method: "post",
                  url: window.ajaxurl,
                  data: jQuery.param(data),
                  headers: {
                       'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
                  }
             });
             return request;
        }            
    };
    return serviceFunc;
 
}]
);
