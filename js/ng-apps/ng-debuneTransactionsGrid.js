/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * angularjs application
 * this app implements the datagrid for account transactions in Admin Panel
 * @type @exp;angular@call;module
 */

var debuneAdminChartApp = angular.module( 'debuneAdminChart', [
  'ngLodash',
  'ngWpServices',
  'ngAnimate', 
  'ngTouch',
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.autoResize',
  'ui.grid.pagination'
]);

/**
 * convert UTC dates to local timezone dates
 * @param {type} param1
 * @param {type} param2
 */
debuneAdminChartApp.filter('localTimeFilter', function () { 
return function (value) {
    var date = new Date(value);
    date.setTime(date.getTime() + getTimeZoneOffsetMs());
    return date.toLocaleDateString(navigator.language) + " " + date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});
  };
});
debuneAdminChartApp.filter('currencyFilter', function () { 
return function (value, direction) {
    value = direction === 'out' ? -value : value;
    return value.toFixed(3).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  };
});

debuneAdminChartApp.controller('debuneAdminChartCtrl', ['$scope', '$http', '$log', 'httpService', '$interval', 'uiGridConstants', 'lodash', function($scope, $http, $log, httpService, $interval, uiGridConstants, lodash) {
    $scope.msg = {};
    
    /* model for transTypes dropdown */
    $scope.objTypes = {};

    $scope.objExists = false; // this is set true when an account or asset exist
    
    $scope.requests = []; //to be able to abord $http requests
    
    $scope.gridAjaxLoader = false;

    /*
     * initialize the app populating the model for dropdowns and setting initial values for form fields
     * @param {type} options
     * @returns {undefined}
     */
    $scope.init = function (options) {
        var options = angular.fromJson(options);
        // init model that feeds the dropdowns
        var postdata = {
                'action':'debuneGetTemplateModel_ajax_request',
                'filename' : 'chart-dropdowns.json',
                'nonce': window.wpNonce
        };
        $scope.cancelAllHttpRequests(); // we cancel all active $http requests before a new request fires
        var request = httpService.post_content(postdata);
        //$scope.requests.push(request); we don't want this request to be 'abortable'
        request.promise.then(function(res) {
            if (res.status == '200') {
                $scope.objTypes = res.data;
                // transaction attributes
                $scope.objTypes["account"].objAttributes = options.objType;
            } else {
                $scope.objTypes = {};
            }
            //clearRequest(request); // remove this request from requests array as it service has been fully consumed
        }, function(reason){
            console.log(reason);
        });

        $scope.objID = options.objIDSelected || '';
        $scope.objType = options.objTypeSelected || '';
        $scope.transactionAttributes = options.transAttrSelected || '';
        $scope.assetChartModel = options.assetChartSelected || '';
        if ($scope.objID !== '' && $scope.objType !='') {
            $scope.objExists = true;
        }
        
        $scope.buildShortCode();
        
    };
    
    /**
     * build the chart shortcode using parameters from chart admin page
     * TODO this doesn't work!
     * @returns {String}
     */
    $scope.buildShortCode = function() {
        $scope.chartshortcode = '[debune output="chart" objType="'+$scope.objType+'" id="'+$scope.objID+'" attributes="'+ $scope.formattedAttributes($scope.transactionAttributes)+'" formatted="No" title="'+$scope.escapeStr($scope.chartTitle)+'" chartoptions=\'{"model":"highstock-line", "width": "100%", "height": "600px"}\']';
        /*
        var chartTitle = $scope.chartTitle == "" || typeof $scope.chartTitle === "undefined" ? "" : $scope.escapeStr(+$scope.chartTitle);
        
        var sc='[debune output="chart" ';
        sc = sc + 'objType="'+$scope.objType+'" ';
        sc = sc + 'id="'+$scope.objID+'" ';
        sc = sc + 'attributes="'+$scope.formattedAttributes($scope.transactionAttributes)+'" ';
        sc = sc + 'formatted=No ';
        sc = sc + 'title="'+chartTitle+'" ';
        sc = sc + 'chartoptions="'+$scope.escapeStr('{"model":"highstock-line", "width": "100%", "height": "600px"}')+'" ';
        $scope.chartShortcode = sc;
        return sc;
        */
    };
    
    /**
     * escape a string (quotes and double quotes)
     * @param {type} str
     * @returns {unresolved}
     */
    $scope.escapeStr = function(str) {
        if (typeof str === "undefined") {
            return str;
        }
        
        if (typeof str === "string") {
            return str.replace(/(['"])/g, "\\$1");
        } else {
            return str;
        }
        
        //return JSON.stringify(str);
    };
        
    /**
     * function to update the dropdown menus when either object type od id are changed
     * @returns {undefined}
     */
    $scope.updateDropdowns = function(selectedAttributes) {
        if ($scope.objID != '' && $scope.objType != '') {
            var service = httpService.obj_exist($scope.objID, $scope.objType)
                .success(function(data, status, headers, config) {
                    if (status == '200') {
                        if (data.success === 'no') {
                            popupAlert(data.msg);
                            $scope.objExists = false;
                        } else {
                            // object (account or asset) exists, so make the attributes visible
                            $scope.objExists = true;
                            if (selectedAttributes !== "") {
                                $scope.updateTransactionsGrid(selectedAttributes);
                            }
                            // update shortcode
                            $scope.buildShortCode();
                        }
                    } else {
                        jsLog(status);
                    }
                })
                .error(function(data, status, headers, config) {
                  DEBUNE_DEBUG ? popupAlert(status) : console.log(status);
            });
            return service;
        } else {
            return false;
        }
    };

    /**
     * cancel (Abort) a single $http request
     * @param {type} request a $http request object (it must be present into $scope.requests array)
     * @returns {undefined}
     */    
    $scope.cancelHttpRequest = function(request){
        request.cancel("User cancelled");
        clearRequest(request);
    };
    var clearRequest = function(request){
        $scope.requests.splice($scope.requests.indexOf(request), 1);
    };

    /**
     * cancel (Abort) all active $http request
     * @returns {undefined}
     */
    $scope.cancelAllHttpRequests = function(){
        angular.forEach($scope.requests, function(request, idx){
            request.cancel("User cancelled");
            jsLog("http request "+request+" aborted");
        });
        $scope.requests = []; //reset the requests stack
    };
    
    $scope.formattedAttributes = function(attr) {
        if (!(typeof attr === "undefined" || attr === "")) {
            return attr.join("|");
        } else {
            return "";
        }
        
    };

    /**
     * THE GRID
     */
    
    $scope.gridOptions = {
        enableFiltering: true,
        paginationPageSizes: [20, 40, 100],
        paginationPageSize: 20,
        useExternalPagination: true,
        useExternalFiltering: true,
        useExternalSorting: true
    };
    
    // @note we use headerClass to define the type of attribute because I don't know how to pass extra params to the $http requests
    $scope.gridOptions.columnDefs = [
        //{field: 'transaction', displayName: 'Transaction ID', enableCellEdit: false},
        {field: 'timestamp_datetime', displayName: 'Date', width: 100, enableCellEdit: false, type: 'date', sort: {
          direction: uiGridConstants.DESC,
          priority: 1
        }, cellFilter: "localTimeFilter"},
        {field: 'senderrs', displayName: 'senderRS', width: 150, enableCellEdit: false, type: 'string'},
        {field: 'recipientrs', displayName: 'recipientRS', width: 150, enableCellEdit: false, type: 'string'},
        {field: 'direction', displayName: 'direction', width: 50, enableCellEdit: false, type: 'string'},
        {field: 'type', displayName: 'Type', width: 70, enableCellEdit: false, type: 'string'},
        {field: 'subtype', displayName: 'Sub Type', width: 100, enableCellEdit: false, type: 'string'},
        {field: 'amountnxt', displayName: 'amountNXT', width: 100, enableCellEdit: false, type: 'number', headerClass: 'number', cellTemplate: '<div style="padding-right: 10px; text-align: right">{{COL_FIELD | currencyFilter:row.entity.direction}}</div>'},
        {field: 'notes', displayName: 'Notes', width: '100%', type: 'string'}
    ];
    $scope.gridOptions.data = [];
    
    /**
     * declare all functions related to the grid inside this event so we are sure that it's been initialized
     * @param {type} gridApi
     * @returns {undefined}
     */
    $scope.gridOptions.onRegisterApi = function(gridApi){
        //set gridApi on scope
        $scope.gridApi = gridApi;
        
        // external sorting
        $scope.gridSort = {};
        $scope.gridApi.core.on.sortChanged( $scope, function( grid, sortColumns ) {
          if( sortColumns.length !== 0){
              $scope.gridSort.attribute = sortColumns[0].name;
              //TODO find a way to use an extra parameters from $scope.gridOptions.columnDefs
              $scope.gridSort.type = sortColumns[0].headerClass;
              $scope.gridSort.direction = sortColumns[0].sort.direction;
              $scope.gridSort.priority = sortColumns[0].sort.priority;
              //$scope.gridSort.sortingAlgorithm = sortColumns[0].sortingAlgorithm;
              //$scope.gridSort.suppressRemoveSort = sortColumns[0].suppressRemoveSort;
              $scope.updateTransactionsGrid();
          }
        });


        // external pagination
        $scope.gridApi.pagination.on.paginationChanged( $scope, function( currentPage, pageSize){
            $scope.updateTransactionsGrid($scope.transactionAttributes, currentPage, pageSize);
        });
        
        // external filtering (we throttle for 3 sec to reduce number of calls..)
        $scope.gridFilter = {};
        $scope.gridApi.core.on.filterChanged( $scope, lodash.throttle(function() {
            $scope.gridFilter = {}; // reset the filter object
            var grid = this.grid;
            angular.forEach(grid.columns, function(column, idx){
                var colName = column.name;
                var filterValue = column.filters[0].term;
                if(typeof filterValue !== "undefined" && filterValue !== '') {
                    $scope.gridFilter[colName] = filterValue;
                }
            });
            $scope.updateTransactionsGrid();
        }, 3000, { 'leading': false, 'trailing': true}));
        
        // save editable columns to db
        gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            if (newValue === null && oldValue === null) {
                return true;
            }
            if (newValue === null) {
                newValue = '';
            }
            $scope.gridAjaxLoader = true; //show the ajaxLoader
            /* HERE IT GOES THE AJAX CALL TO UPDATE DB MODEL */
            if (rowEntity.transaction !== '' && newValue !== oldValue) {
                var postdata = {
                    'action':'debuneUpdateAccountTransaction_ajax_request',
                    'transactionID' : rowEntity.transaction,
                    'attribute' : colDef.name,
                    'new_value' : newValue,
                    'nonce': window.wpNonce
                };
                $scope.cancelAllHttpRequests(); // we cancel all active $http requests before a new request fires
                var request = httpService.post_content(postdata);
                $scope.requests.push(request);                
                request.promise.then(function(res) {
                    $scope.gridAjaxLoader = false; //hide the ajaxLoader
                    if (res.status == '200') {
                        if (res.data.success === 'yes') {
                            jsLog(res.data.msg);
                        } else {
                            popupAlert(res.data.msg);
                        }
                        clearRequest(request);
                    }
                });
                
            }
            $scope.msg.lastCellEdited = 'Transaction:'+rowEntity.transaction+ ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue ;
            $scope.$apply();
        });

        // resize the grid when selection changes (maybe useless..)
        $scope.$watch(function(scope) { return scope.transactionAttributes; },
            function(newValue, oldValue) {
                $scope.resizeGrid();
            }
        );
        $scope.resizeGrid = function() {
            var container = angular.element("#accountTransactionsAppContainer").width();
            var selectBox = angular.element("#debune_chart_account_transactionTypes").width();
            var gridWidth = container - selectBox - 30;

            angular.element(".ng_adminGrid").width(gridWidth);
            $scope.gridApi.grid.gridWidth = gridWidth;
            $scope.gridApi.core.refresh();
        };
    };
    
    /**
     * update transaction's grid data and refresh the grid
     * @returns {undefined}
     */
    $scope.updateTransactionsGrid = function(selectedAttributes, pageNumber, pageSize, filter, sort){
        if ($scope.objID != '' && $scope.objType == "account" && $scope.objExists) {
            if (typeof selectedAttributes === "undefined" || selectedAttributes === null) {
                selectedAttributes = $scope.transactionAttributes;
            } else {
                $scope.transactionAttributes = selectedAttributes;
            }
            if (typeof pageNumber === "undefined" || pageNumber === null) {
                pageNumber = 1;
            }
            if (typeof pageSize === "undefined" || pageSize === null) {
                pageSize = $scope.gridOptions.paginationPageSize;
            }
            if (typeof filter === "undefined" || filter === null) {
                filter = $scope.gridFilter;
            }
            if (typeof sort === "undefined" || sort === null) {
                sort = $scope.gridSort;
            }
            $scope.gridAjaxLoader = true; //show the ajaxLoader
            var postdata = {
                    'action':'debuneGetAccountTransactions_ajax_request',
                    'accountID' : $scope.objID,
                    'optionsSelected' : $scope.transactionAttributes,
                    'pageNumber': pageNumber,
                    'pageSize': pageSize,
                    'filter' : filter,
                    'sort': sort,
                    'nonce': window.wpNonce
            };
            $scope.cancelAllHttpRequests(); // we cancel all active $http requests before a new request fires            
            var request = httpService.post_content(postdata);
            $scope.requests.push(request);                
            request.promise.then(function(res) {
                if (res.status == '200') {
                    if (res.data.success == 'no') {
                        popupAlert(res.data.msg);
                    } else {
                        //push data into the grid!
                        $scope.gridOptions.data = angular.fromJson(res.data.results);
                        $scope.gridOptions.totalItems = res.data.totItems;
                        $scope.gridAjaxLoader = false; //hide the ajaxLoader
                        // update shortcode
                        $scope.buildShortCode();
                    }
                } else {
                    $scope.gridOptions.data = {};
                }
                clearRequest(request);
            });
        }
    };    

}]);


/**
 * do the jQuery stuff for ui.grid
 * @param {type} $
 * @returns {undefined}
 */
(function($){
    "use strict";

    /* FIX TRIM FOR IE8 */
    if ( typeof String.prototype.trim !== 'function' ) {
            String.prototype.trim = function() {
                    return this.replace(/^\s+|\s+$/g, '');
            };
    }
    
    /**
     * resize the ng-grid when a jQuery window resize event occurs (angular seems doesn't have a easy way to control window resizing)
     * @note this can be used as an example on how to use angular scopes outside an angular application
     * @note to 'commit' any changes into the angular app we must call scope.$apply() method
     * @returns {undefined}
     */
    var resizeGrid = function() {
        var scope = angular.element($("#accountTransactionsGridCtrl")).scope();
        scope.$apply(function(){
            scope.resizeGrid();
        });
    };
    
    $(document).ready(function(){
        $(window).resize(_.throttle(function() {
            if ($("#transactionsGrid").length > 0) {
                resizeGrid();
            }
        }, 50));
    });
})(jQuery);