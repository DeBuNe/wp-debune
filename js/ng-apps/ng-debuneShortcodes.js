/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * angularjs application
 * this app implements the datagrid for account transactions in Admin Panel
 * @type @exp;angular@call;module
 */

var debuneShortcodesApp = angular.module( 'debuneShortcodes', [
  'ngWpServices',
  'ngAnimate', 
  'ngTouch'
]);

debuneShortcodesApp.controller('debuneShortcodesCtrl', ['$scope', '$http', '$log', 'httpService', function($scope, $http, $log, httpService) {
    //
    // scope variables    
    //
    $scope.msg = {};
    // variable holding the shortcode string
    $scope.shortCode = '';
    // objType (account/asset) / attributes (eg. account virtualattributes names) dropdowns' model
    $scope.objTypes = {};
    
    $scope.objID = '';
    $scope.objType = '';
    $scope.objExists = false; // this is set true when an account or asset exist
    
    $scope.init = function () {
        // init model that feeds the dropdowns
        var postdata = {
                'action':'debuneGetTemplateModel_ajax_request',
                'filename' : 'shortcode-dropdowns.json',
                'nonce': window.wpNonce
        };
        var request = httpService.post_content(postdata);
        //$scope.requests.push(request); we don't want this request to be 'abortable'
        request.promise.then(function(res) {
            if (res.status == '200') {
                $scope.objTypes = res.data;
            } else {
                $scope.objTypes = {};
            }
            //clearRequest(request); // remove this request from requests array as it service has been fully consumed
        }, function(reason){
            console.log(reason);
        });
    };
    
    /**
     * 
     * @param {type} objType 'account' or 'asset' (the first dropdown menu)
     * @param {type} objID
     * @param {type} apiaction
     * @returns {Boolean}
     */
    $scope.updateDataModel = function() {
        var postdata = {
                'action':'debuneUpdateObjAttributes_ajax_request',
                'objID' : $scope.objID,
                'armodel' : $scope.objTypes[$scope.objType].armodel,
                'nonce': window.wpNonce
        };
        
        var request = httpService.post_content(postdata);
        //$scope.requests.push(request); we don't want this request to be 'abortable'
        request.promise.then(function(res) {
            // data contains the response
            // status is the HTTP status
            // headers is the header getter function
            // config is the object that was used to create the HTTP request

            if (res.status == '200') {
                if (res.data.success === 'yes') {
                    $scope.objTypes[$scope.objType].objAttributes = res.data.results;
                    jsLog(res.data.msg);
                } else {
                    $scope.objTypes[$scope.objType].objAttributes = {};
                    popupAlert(res.data.msg);
                }
            } else {
                jsLog(res.status);
            }
        }, function(reason){
            jsLog(reason);
        });
            
        return request;
    };
    
    /**
     * function to update the dropdown menus when either object type od id are changed
     * @returns {undefined}
     */
    $scope.updateDropdowns = function() {
        if ($scope.objID != '' && $scope.objType != '') {
            var service = httpService.obj_exist($scope.objID, $scope.objType)
                .success(function(data, status, headers, config) {
                    if (status == '200') {
                        if (data.success === 'no') {
                            popupAlert(data.msg);
                        $scope.objExists = false;
                    } else {
                        // object (account or asset) exists, so make the attributes visible
                        $scope.objExists = true;
                        $scope.updateDataModel();
                    }
                } else {
                        jsLog(status);
                }
                })
                .error(function(data, status, headers, config) {
                  DEBUNE_DEBUG ? popupAlert(status) : console.log(status);
            });
            return service;
        } else {
            return false;
        }
    };
    
}]);

// http://docs.angularjs.org/guide/bootstrap Manual Initialization
//angular.bootstrap(document, ['debuneShortcodesApp']);


/**
 * do the jQuery stuff for ui.grid
 * @param {type} $
 * @returns {undefined}
 */
(function($){
    "use strict";

    /* FIX TRIM FOR IE8 */
    if ( typeof String.prototype.trim !== 'function' ) {
            String.prototype.trim = function() {
                    return this.replace(/^\s+|\s+$/g, '');
            };
    }
    
    $(document).ready(function(){
        
    });
})(jQuery);
