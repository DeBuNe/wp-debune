/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * JQuery plugin to implement the debune chart view
 * 
 * @note this plugin can be initialized with this html
 * @code
 * 
                    <div style="width: 600px; height: 600px;" id="NXT-9ZX2-YW4S-2YY5-ERV8E"></div>
                    <script type="text/javascript">
                        //parameters to be passed are:
                        // chartID (string - required): chart's ID (usually ID of the DOM object that will render the chart into)
                        // framework (string - required): name of the chart framework that this chart will use (eg. 'highstock')
                        // type (string - required): type of chart (eg. for 'highstock' could be 'candlestick')
                        // chartQueryParams (array value pairs - required): query parameters for the ajax call that popuates the chart
                        jQuery(document).ready(function(){
                            jQuery("#NXT-9ZX2-YW4S-2YY5-ERV8E").debunejQChart(parameters);
                        });
                    </script>
 * @endcode
 **/
(function($) {
    // guard to detect browser-specific issues early in development
    "use strict";
    
    $.debunejQChart = function(element, options) {
        
        //
        // private vars and methods
        //
        
        // plugin's default options
        // this is private property and is  accessible only from inside the plugin
        var defaults = {

            foo: 'bar',

            // if your plugin is event-driven, you may provide callback capabilities
            // for its events. execute these functions before or after events of your 
            // plugin, so that users may customize those particular events without 
            // changing the plugin's code
            onFoo: function() {}

        };
        
        /**
         * array holding all chart objects
         * @note every array element (holding the chart object) must have this structure:
         *              charts['obj ID (account or asset ID)'] = array(
         *                                                          'framework': 'highstock'
         *                                                          'type': 'candlestick'
         *                                                          'chartObj': *the chart object
         *                                                          );
         * @type Array
         */
        var charts = new Array();
        
        var ajaxCalls = new Array();

        // to avoid confusions, use "plugin" to reference the 
        // current instance of the object
        var plugin = this;

        // this will hold the merged default, and user-provided options
        // plugin's properties will be available through this object like:
        // plugin.settings.propertyName from inside the plugin or
        // element.data('pluginName').settings.propertyName from outside the plugin, 
        // where "element" is the element the plugin is attached to;
        plugin.settings = {};

        var $element = $(element), // reference to the jQuery version of DOM element
             element = element;    // reference to the actual DOM element
        
        // global vars and methods

        /**
         * plugin constructor method
         * inits plugin's variables passed from php view or controller action
         * @note these variables are global to the plugin functions
         * @param {array} settings json array ( created with CJavaScript::encode(array()) function)
         * @returns {undefined}
         */
        plugin.init = function() {
            // the plugin's final properties are the merged default and 
            // user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);
            
            /**
             * init the jquery variable to store all 'abortable' ajax calls (JXQR objects)
             */
            $.xhrPool = [];
            $.xhrPool.abortAll = function() {
                $(this).each(function(idx, jqXHR) {
                    jqXHR.abort();
                });
                $.xhrPool.length = 0;
            };

            // mandatory parameters: start
            if (typeof plugin.settings.chartID === 'undefined') {
                plugin.settings.chartID = null;
                popupAlert('required parameter "chartID" is missing!');
                return false;
            }
            if (typeof plugin.settings.chartQueryParams === 'undefined') {
                plugin.settings.chartQueryParams = null;
                popupAlert('required parameter "chartQueryParams" is missing!');
                return false;
            }

            if (typeof plugin.settings.formatAttributes === 'undefined') {
                plugin.settings.formatAttributes = false;
                popupAlert('required parameter "formatAttributes" is missing!');
                return false;
            }
            
            if (typeof plugin.settings.chartOptions === 'undefined') {
                plugin.settings.chartOptions = null;
                popupAlert('required parameter "chartOptions" is missing!');
                return false;
            }
            
            // mandatory parameters: end
            if (typeof plugin.settings.chartConfig === 'undefined')
                plugin.settings.chartOptions = null;
            if (typeof plugin.settings.debug === 'undefined')
                plugin.settings.debug = false;
            // view style
            if (typeof plugin.settings.minHeight === 'undefined') {
                plugin.settings.minHeight = 300;
            }
            
            /** init the chart widget */
            plugin.initChart(plugin.settings.chartID, plugin.settings.chartQueryParams, plugin.settings.chartOptions, plugin.settings.chartConfig, plugin.settings.formatAttributes);

        };

        /**
         * Call this into jQuery.ready function to perform actions after the last ajax call is performed
         * @note to access the results of all success calls:
         * @code
            for(var i=0;i<arguments.length;i++){
               dataobject=arguments[i][0]
               if(dataobject.variousattribute)
                   mySuccessHandlerFirstGuy(dataobject)
               else if(dataobject.anotherattribute)
                   mySuccessHandlerSecondGuy(dataobject)
               //etc ....
            }
         * @endcode
         */
        plugin.afterLastAjax = function(options) {
            options = typeof options === 'undefined'?new Array():options;
        };

        //
        // HELPERS and getters
        //


        //
        // CHARTS
        //

        /**
         * instanciate (if not already done) and update the chart
         * @param {type} options
         * @returns {undefined}
         */
        plugin.initChart = function(chartID, chartQueryParams, chartOptions, chartConfig, formatAttributes) {
            if (typeof options === "undefined") {
                options = new Object();
            }

            // extract the objID (account or asset) from the DOM ID that contains this chart
            var objID = chartID.split("_");
            objID = objID[0];

            var jqxhr = $.ajax({
                type: 'POST',
                url: window.ajaxurl,
                data: {
                    action:'debuneInitChart_ajax_request',
                    chartID: objID,
                    chartQueryParams: chartQueryParams,
                    chartOptions: chartOptions,
                    chartConfig: chartConfig,
                    formatAttributes: formatAttributes,
                    nonce: window.wpNonce
                },
                //dataType: 'json', removed for wordpress...
                beforeSend: function(jqXHR, settings) {
                    $.xhrPool.push(jqXHR);
                    $element.addClass("debune-ajaxLoader");
                }
            })
            .done(function(res, textStatus, jqXHR) {
                // render the chart!
                jsLog("debune initChart for element "+$element.attr('id')+". data retreived: ");
                if (res.success === "yes") {
                    plugin.renderChart(res.chartConfig);
                } else {
                    popupAlert(res.msg);
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                // catch errors generated by aborting the ajax call
                if (jqXHR.status === 0 && errorThrown === "abort") {
                    return;
                }
                if (plugin.settings.debug) {
                    alert(errorThrown + ': ' + jqXHR.responseText);
                } else {
                    popupAlert(errorThrown);
                }
            })
            .always(function(res, textStatus, jqXHR) {
                $element.removeClass("debune-ajaxLoader");
                // remove this ajax request from the pool of pending requests as it has been fully consumed
                var index = $.xhrPool.indexOf(jqXHR);
                if (index > -1) {
                    $.xhrPool.splice(index, 1);
                }
            });

            //TODO: not sure of this.. check it as it seems now we use $.xhrPool to hold all ajax request objects!
            ajaxCalls.push(jqxhr);
        };


        /**
         * init the chart object and attach the handler to its DOM object (div)
         * @param {object} chartOptions associative array containing extra configurations for the chart
         * @note for all highchart chartOptions see @link http://api.highcharts.com/highcharts#global
         * 
         * @param {type} data chart data object (usually retreived from an ajax call)
         * @param {type} chartOptions chart configuration options object
         * @param {type} chartID chart ID (usually DOM ID of chart's container)
         * @param {type} framework (chart's framework to be used: eg. 'highstock')
         * @param {type} type (chart framework's object type: eg. for highstock could be 'StockChart')
         * @param {type} chartType chart's type (eg. 'candlestick')
         * @returns {undefined}
         */
        plugin.renderChart = function (chartConfig) {
            var chartID = plugin.settings.chartID;
            var framework = chartConfig.framework;
            // unset the extra chart properties (not part of highchart framework)
            delete chartConfig._comment;
            delete chartConfig.framework;
            var type = plugin.settings.type;
            
            // if chart has already been drawn, we just update the series
            var chart = $element.highcharts();
            if (chart && chart.series && chart.series.length > 0) {
                /*
                chart.series[0].setData(_(data).map(function(e) { return e.revenuesTY; }),true);
                chart.series[1].setData(_(data).map(function(e) { return e.revenuesRef; }),true);
                chart.series[2].setData(_(data).map(function(e) { return e.targetTY; }),true);
                chart.xAxis[0].setCategories(_(data).map(function(e) { return e.formattedDate; }), false);
                */
            } else {
                chartConfig.chart.renderTo = $element.attr("id");
                
                // add the client timezone offset (in milliseconds) to timestamps (that are formatted in UTC/GMT)
                var offsetms = getTimeZoneOffsetMs();
                var i;
                for (i = 0; i < chartConfig.series.length; i++) { 
                    var j;
                    if (typeof chartConfig.series[i].data !== "undefined") {
                        for (j = 0; j < chartConfig.series[i].data.length; j++) {
                            //data[j][0] = x axes value
                            chartConfig.series[i].data[j][0] = parseInt(chartConfig.series[i].data[j][0]) + offsetms;
                        }
                    }
                    if (typeof chartConfig.series[i].tooltip !== "undefined" && typeof chartConfig.series[i].tooltip.pointFormatter !== "undefined") {
                        var func = new Function("return '" + chartConfig.series[i].tooltip.pointFormatter+"'");
                        chartConfig.series[i].tooltip.pointFormatter = func;
                    }
                }
                
                jsLog(chartConfig.series);
                
                // instantiate the chart object depending of its framework
                var chartObj;
                if (framework === 'highstock') {
                    if (typeof chartConfig.series !== "undefined") {
                        if (chartConfig.series.length > 1) {
                            chartConfig.rangeSelector.selected = chartConfig.series.length;
                            /*
                            chartConfig.xAxis.labels.formatter = function () {
                                    return (this.value > 0 ? ' + ' : '') + this.value + '%';
                            };
                            */
                        }
                    }
                    jsLog(chartConfig);                    
                    //chartConfig.series[1].data = _(data).map(function(e) { return e.revenuesRef; });
                    chartObj = new Highcharts.StockChart(chartConfig, function(chartObj){
                        plugin.afterChartRendered();
                    });
                } else {
                    chartObj = new Highcharts.Chart(chartConfig, function(chartObj){
                        plugin.afterChartRendered(chartObj);
                    });
                }

                charts[chartID] = new Object();
                charts[chartID].chartObj = chartObj;
                charts[chartID].framework = framework;
                charts[chartID].type = type;
            }
            
        };
        
        /**
         * after render callback
         * @param {type} chartObj
         */
        plugin.afterChartRendered = function (chartObj) {
            //CODE SOMETHING HERE IF USEFULL
        };

        /**
         * abort all currently running ajax calls
         * @returns {undefined}
         */
        plugin.abortAllAjaxCalls = function() {
            $.xhrPool.abortAll();
        };
        
        // fire up the plugin!
        // call the "constructor" method
        plugin.init();
    };
    
    // add the plugin to the jQuery.fn object
    $.fn.debunejQChart = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('debunejQChart')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.debunejQChart(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('pluginName').publicMethod(arg1, arg2, ... argn) or
                // element.data('pluginName').settings.propertyName
                $(this).data('debunejQChart', plugin);

            }

        });

    };
    
})(jQuery);
