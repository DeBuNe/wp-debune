<?php

/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * return a custom field value
 * @global type $post
 * @param type $value
 * @return boolean
 */
function debune_get_custom_field( $value ) {
    global $post;

    $custom_field = get_post_meta( $post->ID, $value, true );
    if ( !empty( $custom_field ) )
	    return is_array( $custom_field ) ? stripslashes_deep( $custom_field ) : stripslashes( wp_kses_decode_entities( $custom_field ) );

    return false;
}

function successRedirect() {
    return home_url('/');
}

/**
 * customize WP login adding nxt button
 */
function custom_login() {
    ?>

    <script type="text/javascript">
        function checkToken() {
            document.getElementById('user_login').value = '';
            document.getElementById('user_pass').value = '';
            var token = prompt("Enter valid NXT token, please:");
            document.getElementById('nxtToken').value = token;
            document.getElementById('wp-submit').click();
        }
    </script>

    <link rel='stylesheet' href='/wp-content/plugins/<?php echo plugin_basename(__DIR__); ?>/style.css' type='text/css' />
    <input type='button' class='button button-primary button-large nxtbtn' id='btnNXT' value='NXT' onClick='checkToken()' title='Login with NXT-Token' />
    <input type='hidden' name='nxtToken' id='nxtToken' value='' />
    <br><br>
    <p><strong><?php echo __("To log in using your NXT Token:",DEBUNE_WP_NXT_SLUG) ?></strong></p>
    <ul>
        <li><?php echo __("open your NXT client and generate a token (using java client provided by nxt.org, go to 'generate token' from top menu)",DEBUNE_WP_NXT_SLUG) ?></li>
        <li><?php echo __("generate a token using 'wp-nxt.com' as data (without quotes)",DEBUNE_WP_NXT_SLUG) ?></li>
        <li><?php echo __("click on 'NXT button and insert that token'",DEBUNE_WP_NXT_SLUG) ?></li>
    </ul>
    <br>
    <em><?php echo __("Note: tokens never expire and their validation, performed by any NXT peer, certifies that account effectively belongs to you",DEBUNE_WP_NXT_SLUG) ?></em>
    <?php
}

add_filter( 'login_form', 'custom_login' );
add_filter( 'login_redirect', 'successRedirect'); // Login page Redirect
add_filter( 'authenticate', 'checkNXTtoken', 30 , 3); // Auth with NXT token
/*
 * ADMIN MENU
 */
function DeBuNeNxtAdminPage() {
    add_options_page('DeBuNe-Wp-Nxt','DeBuNe-Wp-Nxt', 8, 'debune-wp-nxt-authoptions', 'deBuNeWpNxtAuthPageContent');
}

/**
 * DeBuNe Admin Page (WP Admin Settigs) with options
 */
function deBuNeWpNxtAuthPageContent() {
    /*
     * Options page
     */
    if ($_POST) {
        if ( isset($_POST['nxtHostname']) && isset($_POST['nxtPort']) ) {
            update_option('nxtHostname', preg_replace("/ /","",$_POST['nxtHostname']));
        }
        if (isset($_POST['nxtPort'])) {
            update_option('nxtPort', $_POST['nxtPort']);
        }
        if(isset($_POST['nxtUpdatePeers'])) {
            update_option('nxtUpdatePeers', $_POST['nxtUpdatePeers']);
        } else {
            update_option('nxtUpdatePeers', '0');
        }
    }

    $nxtH = get_option('nxtHostname');
    $nxtP = get_option('nxtPort');
    if (is_null($nxtP) || $nxtP == "") {
        $nxtP = '7876';
    }
    $nxtUpdatePeers = get_option('nxtUpdatePeers');


    echo "<div clas='wrap'>";
    echo "<h2>nxtAuth Settings page</h2>";
    echo "<form method=post>";
    settings_fields('nxtOptionsGroup');
    do_settings_sections('nxtOptionsGroup');
    echo __("Enter hostname of NXT nodes separated with comma",DEBUNE_WP_NXT_SLUG).": <br>";
    echo "<input type=text name='nxtHostname' value='".$nxtH."' size=60 /><br>";
    echo __("Enter default port of NXT nodes",DEBUNE_WP_NXT_SLUG).": <br>";
    echo "<input type=text name='nxtPort' value='".$nxtP."' size=5 /><br>";
    echo __("Check below if you want to have the list of available peers automatically updated for you (every 15 mintues) or leave it unchecked if you want to provide your own",DEBUNE_WP_NXT_SLUG).": <br>";
    ?>
    <input type="checkbox" name="nxtUpdatePeers" value="1" <?php checked( $nxtUpdatePeers, '1', true ); ?>/>
    <?php
    submit_button();
    echo "</form></div>";
    echo "<div>";
    echo "<p>".__("NXT API version",DEBUNE_WP_NXT_SLUG).": ".get_option('debune_nxt_api_version')."</p>";
    echo "<p>".__("DeBuNe db version",DEBUNE_WP_NXT_SLUG).": ".get_option('debune_db_version')."</p>";
    echo "</div>";

    // show using host
    $hosts = explode(',', $nxtH);

    echo "<pre>";
    echo __("Using nodes",DEBUNE_WP_NXT_SLUG).": <br>";
    if ( count($hosts) == 0 ) { echo __("No servers available, authorization will not work.",DEBUNE_WP_NXT_SLUG); }
    $hh = array();
    for ( $i=0; $i<count($hosts); $i++ ) {
        if ( strlen($hosts[$i]) < 7 ) continue;
        $hh[$i] = $hosts[$i].":".$nxtP;
    }
    $hh = json_encode($hh);
    echo $hh;
    echo "</pre>";
    // save json
    update_option('nxtHH', $hh);
    
    // check if log file is writable by the web server
    if (!is_writable ( plugin_dir_path(__FILE__) . "../log.txt" )) {
        echo "<div>";
        echo "<p><span style='color:red;'>".__("Attention! log file is not writable by the web server. please correct this giving ownership to webserver or chmod 777 (everybody) to the file:</br>",DEBUNE_WP_NXT_SLUG). " ". plugin_dir_path(__FILE__) . "/../log.txt" ."</span></p>";
        echo "</div>";
    }
    
}
add_action( 'admin_menu', 'DeBuNeNxtAdminPage' );

//
// update NXT peers functions: START
//STEF to debug cron jobs locally: http://wp-nxt.intranet.local/wp-cron.php?doing_cron
//

add_filter( 'cron_schedules', 'cron_add_quarterhour' );
/**
 * add new scheduler to run jobs every 15 minutes
 * @param type $schedules
 * @return type
 */
function cron_add_quarterhour( $schedules ) {
    // Adds once weekly to the existing schedules.
    $schedules['quarterhourly'] = array(
            'interval' => 900,
            'display' => __( 'Once every 15 minutes', DEBUNE_WP_NXT_SLUG )
    );
    return $schedules;
}    

add_action( 'wp', 'updateNxtPeers_activation' );
/**
 * add wp scheduled job to update peers every 15 minutes
 */
function updateNxtPeers_activation() {
    if ( !wp_next_scheduled( 'updateNxtPeers_quaterhourly_event' ) ) {
        wp_schedule_event( time(), 'quarterhourly', 'updateNxtPeers_quaterhourly_event' );
    }
}

add_action( 'updateNxtPeers_quaterhourly_event', 'updateNxtPeers' );
//
// update NXT peers functions: END
//

//
//
// AJAX ACTIONS
//
//

/**
 * ajax to get a json template file
 * @note used to populate dropdown lists
 */
function debuneGetTemplateModel_ajax_request() {
    //global $wpdb;
    // only allow POST requests (eg. to avoid CORS from external domains..)
    if ( isset($_POST) ) {
        // check to see if the submitted nonce matches with the
        // generated nonce we created earlier
        if (!isset($_POST['nonce'])) die ( 'Busted!');
        $nonce = sanitize_text_field($_POST['nonce']);
        if (!wp_verify_nonce( $nonce, 'debune-ajax-nonce' ) ) die ( 'Busted!');
        
        if (isset($_POST['filename'])) {
            $fileName = sanitize_text_field($_POST['filename']);
            $res = json_decode(file_get_contents(DEBUNE_WP_NXT_URL_MODELS . $fileName), true);
        } else {
            $res =array(
                'success' => 'no',
                'msg' => __("Wrong parameters (or no parameters) posted",DEBUNE_WP_NXT_SLUG)
            );
        }
    } else {
        $res =array(
            'success' => 'no',
            'msg' => __("sorry, only POST requests are allowed here!",DEBUNE_WP_NXT_SLUG)
        );
    }
    wp_send_json( $res );
}
add_action( 'wp_ajax_debuneGetTemplateModel_ajax_request', 'debuneGetTemplateModel_ajax_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
// add_action( 'wp_ajax_nopriv_debuneGetTemplateModel_ajax_request', 'debuneGetTemplateModel_ajax_request' );

/**
 * ajax to get all attributes from a model (model->virtual_attributes keys)
 * @note used to populate dropdown list in generate shordcode page
 */
function debuneUpdateObjAttributes_ajax_request() {
    //global $wpdb;
    // only allow POST requests (eg. to avoid CORS from external domains..)
    if ( isset($_POST) ) {
        // check to see if the submitted nonce matches with the
        // generated nonce we created earlier
        if (!isset($_POST['nonce'])) die ( 'Busted!');
        $nonce = sanitize_text_field($_POST['nonce']);
        if (!wp_verify_nonce( $nonce, 'debune-ajax-nonce' ) ) die ( 'Busted!');
        
        // for this to work we need: account ID (NXT- format)
        if (isset($_POST['objID']) && isset($_POST['armodel'])) {
            $objID = sanitize_text_field($_POST['objID']);
            $modelName = sanitize_text_field($_POST['armodel']);
            if ($model = $modelName::findbyIDWithBlockData($objID)) {
                // if model returns an array of models (eg. account_transactions), we only get the first element as we need it to extract its attribute names only
                if (is_array($model)) {
                    $model = $model[0];
                }
                $attributeNames = array_keys($model->getAllAttributes(1));
                $objAttributes = array();
                // clean the attribute array from attributes that we don't want to show
                foreach ($attributeNames as $key => $attribute) {
                    if (!($attribute == 'id' || $attribute == 'created_at' || $attribute == 'updated_at')) {
                        $objAttributes[$attribute] = array(
                            'label' => $attribute,
                            'value' => $attribute
                        );
                    }
                }
                 
                $res = array(
                    'success' => 'yes',
                    'msg' => __("Record retrieved",DEBUNE_WP_NXT_SLUG),
                    'results' => $objAttributes
                );
            } else {
                $res =array(
                    'success' => 'no',
                    'msg' => __("record not found",DEBUNE_WP_NXT_SLUG)
                );
            }
        } else {
            $res =array(
                'success' => 'no',
                'msg' => __("Wrong parameters (or no parameters) posted",DEBUNE_WP_NXT_SLUG)
            );
        }
    } else {
        $res =array(
            'success' => 'no',
            'msg' => __("sorry, only POST requests are allowed here!",DEBUNE_WP_NXT_SLUG)
        );
    }
    wp_send_json( $res );
}
add_action( 'wp_ajax_debuneUpdateObjAttributes_ajax_request', 'debuneUpdateObjAttributes_ajax_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
// add_action( 'wp_ajax_nopriv_debuneUpdateObjAttributes_ajax_request', 'debuneUpdateObjAttributes_ajax_request' );


//****************************************************************************
//
// include all other wp-function files
//
//

// helper functions specific for wordpress
require_once __DIR__ . '/helpers.php';

// functions to manage debune charts admin interface
require_once __DIR__ . '/wp-functions-admin-debuneCharts.php';

// functions to manage debune account shortcodes admin interface
require_once __DIR__ . '/wp-functions-admin-debuneShortcodes.php';

// functions to manage and render debune charts in post and pages using shortcodes
require_once __DIR__ . '/wp-functions-frontend-debuneCharts.php';
