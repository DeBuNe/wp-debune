<?php

/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

//
//
// functions implement and manage debune charts admin interface
//
//

//
// register DeBuNe chart post type: start
//

add_action( 'init', 'register_cpt_debune_chart', 20 );

function register_cpt_debune_chart() {

    $labels = array( 
        'name' => __( 'DeBuNe Charts',DEBUNE_WP_NXT_SLUG ),
        'singular_name' => __( 'DeBuNe Chart',DEBUNE_WP_NXT_SLUG ),
        'add_new' => __( 'Add New Chart',DEBUNE_WP_NXT_SLUG ),
        'add_new_item' => __( 'Add New DeBuNe Chart',DEBUNE_WP_NXT_SLUG ),
        'edit_item' => __( 'Edit DeBuNe Chart',DEBUNE_WP_NXT_SLUG ),
        'new_item' => __( 'New DeBuNe Chart',DEBUNE_WP_NXT_SLUG ),
        'view_item' => __( 'View DeBuNe Chart',DEBUNE_WP_NXT_SLUG ),
        'search_items' => __( 'Search DeBuNe Charts',DEBUNE_WP_NXT_SLUG ),
        'not_found' => __( 'No debune charts found',DEBUNE_WP_NXT_SLUG ),
        'not_found_in_trash' => __( 'No DeBuNe charts found in Trash',DEBUNE_WP_NXT_SLUG ),
        'parent_item_colon' => __( 'Parent DeBuNe Chart:',DEBUNE_WP_NXT_SLUG ),
        'menu_name' => __( 'DeBuNe Charts',DEBUNE_WP_NXT_SLUG ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => __('Custom post type to manage DeBuNe Charts',DEBUNE_WP_NXT_SLUG),
        //'supports' => array( 'title', 'custom-fields' ),
        'supports' => array( 'title'),

        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-chart-line',
        'show_in_nav_menus' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        //'rewrite' => array('slug' => DEBUNE_WP_NXT_SLUG),
        'capability_type' => 'post'
    );

    register_post_type( 'debune_chart', $args );
    
}    

// move the publish metabox 
function tc_screen_layout_columns( $columns ) {
    $columns['debune_chart'] = 999;
    return $columns;
}

add_filter( 'screen_layout_columns', 'tc_screen_layout_columns' );
function tc_screen_layout_debune_chart() {
    return 1;
}
add_filter( 'get_user_option_screen_layout_debune_chart', 'tc_screen_layout_debune_chart' );

//
// register DeBuNe chart post type: end
//


//
// custom fields for debune_chart post type: start
//

/**
 * Register the Metabox
 */
function debune_chart_add_custom_meta_boxes() {
    add_meta_box( 'debune-chart-config-meta-box', __( 'Chart Data Object', DEBUNE_WP_NXT_SLUG ), 'debune_chart_config_meta_box_output', 'debune_chart', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'debune_chart_add_custom_meta_boxes' );

/**
 * Output the Object Metabox (account or asset)
 * @param type $post
 */
function debune_chart_config_meta_box_output( $post ) {
    // create a nonce field
    wp_nonce_field( 'my_debune_chart_meta_box_nonce', 'debune_chart_meta_box_nonce' ); 
    
    // get the shortcode to render chart's preview
    $shortCode = html_entity_decode( debune_get_custom_field('_debune_chartshortcode') );
    
    // objID selected value (account ID or asset ID)
    $chart_obj_id = debune_get_custom_field( '_debune_chart_obj_id' );
    
    // objType dropdown selected value ('account' or 'asset')
    $option_objType_selected = debune_get_custom_field( '_debune_chart_obj_type' );
    
    // chart title
    $option_chartTitle = debune_get_custom_field( '_debune_chart_title' );
    
    
    // account transaction dropdown options and selected values
    global $nxtConstants;
    $option_transTypes = $nxtConstants->transactionSubTypesArray;
    $option_transTypes_selected = debune_get_custom_field( '_debune_chart_account_transactionTypes' );
    // asset dropdown selected value
    $option_asset_selected = debune_get_custom_field( '_debune_chart_asset_chartstype' );
    
    if ($option_transTypes_selected){
        $option_transTypes_selected = json_decode($option_transTypes_selected);
    }
    
    
    if ($post) {
        // edit, raw (new post)
        $action = $post->filter;
        
        $transactionsData = array();
        $objType = debune_get_custom_field( '_debune_chart_obj_type' );
        $objID = debune_get_custom_field( '_debune_chart_obj_id' );
        //asset_account
        if ($action == "edit") {
            if ( $objType == "account") {
                $account = debune_account::findbyIDWithBlockData($objID);
                //$account = debune_account::findWithBlockData(array($objID),array($objType=>$objID));
                
                // array holding all transactions data
                $transactionsData = array();
                foreach ($option_transTypes_selected as $optionName) {
                    $selectedType = $nxtConstants->getTransactionTypesFromDescription($optionName);
                    if ($transactions = debune_account_transactions::findWithBlockData(array('all', array('conditions' => "(senderRS = '".$objID."' OR recipientRS = '".$objID."') AND type = {$selectedType['type']} AND subtype = {$selectedType['subtype']}")),array($objType=>$objID, 'type'=>$selectedType['type'], 'subtype'=>$selectedType['subtype']),$cache = true, $cacheComponent = 'DBCache', $objID)) {
                        foreach ($transactions as $transaction) {
                            $transactionsData[] = $transaction->getAllAttributes();
                        }
                    }
                }
            }
        }
        $transactionsData = encodeArray($transactionsData);
    }

    $angularOptionsArray = encodeArray(array(
        'objIDSelected' => $chart_obj_id,
        'objType' => $option_transTypes,
        'objTypeSelected' => $option_objType_selected,
        'transAttrSelected' => $option_transTypes_selected,
        'assetChartSelected' => $option_asset_selected,
    ));
    
    ?>
    <div id="accountTransactionsAppContainer" class="selectbox-detail" ng-app="debuneAdminChart">
        <div id="accountTransactionsGridCtrl" ng-controller="debuneAdminChartCtrl" data-ng-init='init(<?php echo $angularOptionsArray ?>)'>
            <div>
                <label for="debune_chart_obj_id"><?php echo _e( 'Object ID (account or asset)', DEBUNE_WP_NXT_SLUG ); ?>:</label>
                <input type="text" name="debune_chart_obj_id" id="debune_chart_obj_id" size="24" value="<?php echo $chart_obj_id; ?>"
                    ng-model="objID"
                    ng-change="updateDropdowns(transactionAttributes);"
                    ng-model-options="{ updateOn: 'blur' }">
            </div>
            <div>
                <label for="debune_chart_obj_type"><?php echo _e( 'Object Type', DEBUNE_WP_NXT_SLUG ); ?>:</label>
                <!-- Using WP selected() to get the selected option -->
                <select name="debune_chart_obj_type" id="debune_chart_obj_type"
                    ng-model="objType" 
                    ng-options="objTypeId as objTypeDetails.label for (objTypeId , objTypeDetails) in objTypes"
                    ng-change="updateDropdowns(transactionAttributes);">
                    <option value="">-- choose object --</option>                
                </select>
            </div>
            <div ng-show="objExists">
                <div>
                    <label for="debune_chart_name"><?php echo _e( 'Chart Title', DEBUNE_WP_NXT_SLUG ); ?>:</label>
                    <input type="text" name="debune_chart_title" id="debune_chart_title" size="50"
                        ng-model="chartTitle"
                        ng-change="buildShortCode()"
                        ng-init='chartTitle="<?php echo esc_attr($option_chartTitle); ?>"'>
                </div>                
                
                <div class="transactionTypes-container" ng-if="objType == 'account'">
                    <div style="margin-top:20px">
                        <div style="display: inline-block;float: left;">
                            <label style="display: block;" for="objType"><?php echo _e( 'Transaction Type', DEBUNE_WP_NXT_SLUG ); ?></label>
                            <select id="debune_chart_account_transactionTypes" name="debune_chart_account_transactionTypes[]"  multiple="multiple" ng-multiple="true"
                                ng-model="transactionAttributes"
                                ng-options="objAttributesId as objAttributesDetails.label group by objAttributesDetails.parent for (objAttributesId, objAttributesDetails) in objTypes[objType]['objAttributes']"
                                ng-change="updateTransactionsGrid(transactionAttributes);"
                                ng-init="updateTransactionsGrid(transactionAttributes);"
                                ng-model-options="{ debounce: 500 }">
                            </select>
                        </div>
                        <div ng-show="gridAjaxLoader" class="debune-table-ajaxLoader"></div>
                        <div style="display: inline-block;float: right; margin-left: 5px;">
                            <div>
                                <div id="transactionsGridLabel" ng-if="gridOptions.data.length"><?php echo __( 'Records found', DEBUNE_WP_NXT_SLUG ); ?>: {{ gridOptions.data.length | number }}<br></div>
                                <!--<div ng-if="gridOptions.data.length"><strong>Last Cell Edited:</strong> {{msg.lastCellEdited}}</div>-->
                                <!-- STEF delete <div id="transactionsGrid" class="ng_adminGrid" ui-grid="gridOptions" ng-if="gridOptions.data.length" ui-grid-edit ui-grid-pagination></div> -->
                                <div id="transactionsGrid" class="ng_adminGrid" ui-grid="gridOptions" ui-grid-edit ui-grid-pagination></div>
                            </div>                            
                        </div>
                        

                    </div>
                </div>
                <div class="assetTypes-container" ng-if="objType == 'asset'">
                    <label for="debune_chart_asset_chartstype"><?php echo _e( 'Asset Chart Types', DEBUNE_WP_NXT_SLUG ); ?>:</label>
                    <!-- Using WP selected() to get the selected option -->
                    <select name="debune_chart_asset_chartstype" id="debune_chart_asset_chartstype"
                        ng-model="assetChartModel" 
                        ng-options="objAttributesId as objAttributesDetails.label  for (objAttributesId, objAttributesDetails) in objTypes[objType]['objAttributes']"
                        ng-change="">
                        <option value="">-- choose asset chart --</option>
                    </select>   
                </div>
                <h3 ng-show="transactionAttributes">Shortcode: </h3>
                <!--<div id="chartshortcode" class="shortcodebox" ng-if="transactionAttributes">[debune output="chart" objType="{{objType}}" id="{{objID}}" attributes="{{formattedAttributes(transactionAttributes)}}" formatted="No" title="{{escapeStr(chartTitle)}}" chartoptions='{"model":"highstock-line", "width": "100%", "height": "600px"}']</div>-->
                <!--<div id="chartshortcodetxt" class="shortcodebox" ng-if="transactionAttributes">{{chartshortcode}}</div>-->
                <div id="chartshortcodetxt" class="shortcodebox" ng-if="transactionAttributes">
                    <div>{{chartshortcode}}</div></br></br>
                    <i><?php echo "*" . __( 'Cut&paste this code into any post or page to render this chart!', DEBUNE_WP_NXT_SLUG ) ?></i>
                </div>
                <input type="hidden" id="debune_chartshortcode" name="debune_chartshortcode" value="{{chartshortcode}}" ng-if="transactionAttributes" />
                <div class="shortcodebox" ng-if="transactionAttributes">
                    <h2 style="text-align: center"><?php echo __( 'Chart Preview', DEBUNE_WP_NXT_SLUG ) ?></h2>
                    <?php
                    // render the chart (after this page has been saved/updated)
                    if ($shortCode) {
                        //echo do_shortcode('[debune output="chart" objType="account" id="NXT-9ZX2-YW4S-2YY5-ERV8E" attributes="Arbitrary message|Ordinary payment" formatted="No" title="cazzofritto" chartoptions=\'{"model":"highstock-line", "width": "100%", "height": "600px"}\']');
                        echo do_shortcode($shortCode);
                    }
                    
                    ?>
                    <i><?php echo "*" . __( 'if you made any changes in the selection (transaction types or any other parameter), please update this page to update the chart with current shortcode', DEBUNE_WP_NXT_SLUG ) ?></div>
                </div>
                
            </div>
            
        </div> <!-- end of controller -->
    </div> <!-- end of app -->
    <?php
}


/**
 * Save the Metabox values
 * @param type $post_id
 * @return type
 */
function debune_chart_meta_boxes_save( $post_id ) {
    // Stop the script when doing autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { 
        return;
    }

    // Verify the nonce. If insn't there, stop the script
    if( !isset( $_POST['debune_chart_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['debune_chart_meta_box_nonce'], 'my_debune_chart_meta_box_nonce' ) ) {
        return;
    }

    // Stop the script if the user does not have edit permissions
    if( !current_user_can( 'edit_post' ) ) {
        return;
    }

    // Save the dropdown value (asset or account)
    if( isset( $_POST['debune_chart_obj_type'] ) ) {
        update_post_meta( $post_id, '_debune_chart_obj_id', esc_attr( sanitize_text_field($_POST['debune_chart_obj_id'])) );
        update_post_meta( $post_id, '_debune_chart_obj_type', esc_attr( sanitize_text_field($_POST['debune_chart_obj_type'])) );
            
        // Save the right metaboxes forms
        switch ($_POST['debune_chart_obj_type']) {
            case "account":
                // save all fields from account metabox
                $transTypes = sanitize_str_array($_POST['debune_chart_account_transactionTypes']);
                update_post_meta( $post_id, '_debune_chart_account_transactionTypes', encodeArray($transTypes) );
                break;
            case "asset":
                // save all fields from asset metabox
                $assetChart = sanitize_text_field($_POST['debune_chart_asset_chartstype']);
                update_post_meta( $post_id, '_debune_chart_asset_chartstype', $assetChart );
                break;

            default:
                break;
        }
        
        if( isset( $_POST['debune_chart_title'] ) ) {
            $chartTitle = sanitize_text_field(stripcslashes($_POST['debune_chart_title']));
            update_post_meta( $post_id, '_debune_chart_title', esc_attr( $chartTitle ) );
        }

        if( isset( $_POST['debune_chartshortcode'] ) ) {
            $chartShortcode = sanitize_text_field(htmlentities(stripslashes($_REQUEST['debune_chartshortcode'])));
            update_post_meta( $post_id, '_debune_chartshortcode', $chartShortcode );
        }
            
    }
    
}
add_action( 'save_post', 'debune_chart_meta_boxes_save' );

/**
 * ajax to populate the account metabox with all account's object types directly from the blockchain
 */
function debuneGetTypes_ajax_request() {
    // only allow POST requests (eg. to avoid CORS from external domains..)
    if ( isset($_POST) ) {
        if (isset($_POST['nxtObjID']) && isset($_POST['nxtObjType'])) {
            $nxt = new deBuNe_NXT_API();
            $nxt->ObjID = $_POST['nxtObjID'];
            $nxt->ObjType = $_POST['nxtObjType'];
            
            if(isset($_POST['verifyOnly']) && (int) $_POST['verifyOnly'] == true) {
                if( $nxt->currentObjectExists() ) {
                    $res =array(
                        'success' => 'yes',
                        'msg' => $_POST['nxtObjType'] . __( ' succesfully verified.',DEBUNE_WP_NXT_SLUG )
                    );
                } else {
                    $res =array(
                        'success' => 'no',
                        'msg' => $_POST['nxtObjType'] . __( ' does not exist in the blockchain.',DEBUNE_WP_NXT_SLUG )
                    );
                }
            } else {
                if (!$data = $nxt->getObjData($nxt->ObjID, $nxt->ObjType, $options = array("includeLessors","includeAssets","includeCurrencies","includeCounts"))) {
                    $res = array(
                        'success' => 'no',
                        'error' => $nxt->error['errorCode'],
                        'msg' => $nxt->error['msg']
                    );
                } else {
                    //SUCCESS! WE CAN PROCESS RETURNING DATA
                    $objTypes = array_keys($data);
                    $res = array(
                        'success' => 'yes',
                        'msg' => __("Data successfully retrieved from RNS",DEBUNE_WP_NXT_SLUG),
                        'results' => $objTypes
                    );
                }
            }
        } else {
            $res =array(
                'success' => 'no',
                'msg' => __("ID or object type not defined",DEBUNE_WP_NXT_SLUG)
            );
        }
    } else {
        $res =array(
            'success' => 'no',
            'msg' => __("sorry, only POST requests are allowed here!",DEBUNE_WP_NXT_SLUG)
        );
    }
    wp_send_json( $res );
}
add_action( 'wp_ajax_debuneGetTypes_ajax_request', 'debuneGetTypes_ajax_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
// add_action( 'wp_ajax_nopriv_debuneGetTypes_ajax_request', 'debuneGetTypes_ajax_request' );


/**
 * ajax to populate the #transactionsGrid admin grid (in Admin/debuneCharts)
 */
function debuneGetAccountTransactions_ajax_request() {
    //global $wpdb;
    // only allow POST requests (eg. to avoid CORS from external domains..)
    if ( isset($_POST) ) {
        // check to see if the submitted nonce matches with the
        // generated nonce we created earlier
        if (!isset($_POST['nonce'])) die ( 'Busted!');
        $nonce = sanitize_text_field($_POST['nonce']);
        if (!wp_verify_nonce( $nonce, 'debune-ajax-nonce' ) ) die ( 'Busted!');
        
        // for this to work we need: account ID (NXT- format)
        if (isset($_POST['accountID'])) {
            $option_selected = isset($_POST['optionsSelected']) ? sanitize_str_array($_POST['optionsSelected']) : array();
            $objID = sanitize_text_field($_POST['accountID']);
            $objType = 'account';
            if (empty($option_selected)) {
                $res = array(
                    'success' => 'yes',
                    'msg' => __("Data successfully retrieved: no transactions for this account",DEBUNE_WP_NXT_SLUG),
                    'results' => array()
                );                
            } else {
                $nxtConstants = new nxt_constants();

                //$account = debune_account::findWithBlockData(array($objID),array($objType=>$objID));
                
                // get all transactions, of all selected options (type/subtype) for this account
                $mergedTransactions = array();
                $transactions = debune_account_transactions::getAccountTransactionsData($objID, $option_selected, true);
                
                // implementing grid external filter
                $filteredTransactions = array();
                if (isset($_POST['filter']) && $_POST['filter'] != "" && !empty($_POST['filter'])) {
                    $filter = array_map( 'sanitize_text_field', $_POST['filter'] );
                    $filter = array_filter($filter);
                    foreach ($transactions as $transaction) {
                        $keepIt = true;
                        foreach ($filter as $attrName => $filterValue) {
                            if (isset($transaction[$attrName]) && strpos($transaction[$attrName],$filterValue) !== false) {
                                $keepIt = true;
                            } else {
                                $keepIt = false;
                                break;
                            }
                        }
                        if ($keepIt) {
                            $filteredTransactions[] = $transaction;
                        }
                    }
                    $transactions = $filteredTransactions;
                }
                
                /*
                 * $_POST['sort'] is a hash array with this structure:
                                array(
                                    'attribute' => attribute to use for sorting
                                    'type' => ""
                                    'direction' => "asc" or "desc"
                                    'priority' => 1 (means it will be the first attr to be sorted in a multi-attribute sort)
                                )
                 */
                if (isset($_POST['sort']) && $_POST['sort'] != "" && !empty($_POST['sort'])) {
                    $sort = array_map( 'sanitize_text_field', $_POST['sort'] );
                    // if we are sorting by date, use the timestamp instead, so if date is formatted in a way that will mess the sort algorithm up, we fix this
                    if ($sort['attribute'] == "timestamp_date" || $sort['attribute'] == "timestamp_datetime") {
                        $sort['attribute'] = "timestamp";
                    }
                } else {
                    // default sort (descending) by timestamp
                    $sort = array(
                        'attribute' => "timestamp",
                        'type' => "",
                        'direction' => "desc",
                        'priority' => 1
                    );
                }
                $order = $sort['direction'];
                //$order = $sort['direction'] == 'asc' ? SORT_ASC : SORT_DESC;
                $attrName = $sort['attribute'];
                $attrType = $sort['type'];
                $sortingAlgorithm = $attrType !== "" && !empty($attrType) ? $attrType : 'natural';
                multiArraySort($transactions, $attrName, $order, $sortingAlgorithm);
                //array_multisort($transactions, $order, SORT_NATURAL);
                    
                //array_multisort ( array &$array1 [, mixed $array1_sort_order = SORT_ASC [, mixed $array1_sort_flags = SORT_REGULAR [, mixed $... ]]] )
                
                // implementing grid external pagination
                $totItems = count($transactions); // counter for grid's total items and pagination
                $transactionsData = array();
                if (isset($_POST['pageNumber']) && isset($_POST['pageSize'])) {
                    $pageNumber = (int)sanitize_text_field($_POST['pageNumber'])-1;
                    $pageSize = (int)sanitize_text_field($_POST['pageSize']);
                    
                    $start = $pageNumber * $pageSize;
                    $end = ($pageNumber+1) * $pageSize;
                    $pagedTransactions = array();
                    for ($index = $start; $index < $end; $index++) {
                        if (!isset($transactions[$index])) break; // if this happens it means we have reached the last transaction

                        $transactionsData[] = $transactions[$index];
                    }
                } else {
                    $transactionsData = $transactions;
                }
                
                $res = array(
                    'success' => 'yes',
                    'msg' => __("Data successfully retrieved",DEBUNE_WP_NXT_SLUG),
                    'results' => $transactionsData,
                    'totItems' => $totItems
                );
            }
        } else {
            $res =array(
                'success' => 'no',
                'msg' => __("Wrong parameters (or no parameters) posted",DEBUNE_WP_NXT_SLUG)
            );
        }
    } else {
        $res =array(
            'success' => 'no',
            'msg' => __("sorry, only POST requests are allowed here!",DEBUNE_WP_NXT_SLUG)
        );
    }
    
    wp_send_json( $res );
}
add_action( 'wp_ajax_debuneGetAccountTransactions_ajax_request', 'debuneGetAccountTransactions_ajax_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
// add_action( 'wp_ajax_nopriv_debuneGetAccountTransactions_ajax_request', 'debuneGetAccountTransactions_ajax_request' );

/**
 * ajax to update a transaction with values coming from the grid
 */
function debuneUpdateAccountTransaction_ajax_request() {
    //global $wpdb;
    // only allow POST requests (eg. to avoid CORS from external domains..)
    if ( isset($_POST) ) {
        // check to see if the submitted nonce matches with the
        // generated nonce we created earlier
        if (!isset($_POST['nonce'])) die ( 'Busted!');
        $nonce = sanitize_text_field($_POST['nonce']);
        if (!wp_verify_nonce( $nonce, 'debune-ajax-nonce' ) ) die ( 'Busted!');
        
        // for this to work we need: account ID (NXT- format)
        if (isset($_POST['transactionID']) && isset($_POST['attribute']) && isset($_POST['new_value'])) {
            $objID = sanitize_text_field($_POST['transactionID']);
            $objAttrName = sanitize_text_field($_POST['attribute']);
            $objAttrValue = sanitize_text_field($_POST['new_value']);
            
            if ($transaction = debune_transaction::find($objID)) {
                $transaction->$objAttrName = $objAttrValue;
                if ($transaction->save()) {
                    $res = array(
                        'success' => 'yes',
                        'msg' => __("Record successfully updated",DEBUNE_WP_NXT_SLUG),
                    );
                } else {
                    $transaction = new debune_ActiveRecord();
                    $res =array(
                        'success' => 'no',
                        'msg' => __("Something went wrong.. record not saved",DEBUNE_WP_NXT_SLUG)
                    );
                }
            } else {
                $res =array(
                    'success' => 'no',
                    'msg' => __("Something went wrong.. transaction not found into the db. probably has been deleted",DEBUNE_WP_NXT_SLUG)
                );
            }
        } else {
            $res =array(
                'success' => 'no',
                'msg' => __("Wrong parameters (or no parameters) posted",DEBUNE_WP_NXT_SLUG)
            );
        }
    } else {
        $res =array(
            'success' => 'no',
            'msg' => __("sorry, only POST requests are allowed here!",DEBUNE_WP_NXT_SLUG)
        );
    }
    wp_send_json( $res );
}
add_action( 'wp_ajax_debuneUpdateAccountTransaction_ajax_request', 'debuneUpdateAccountTransaction_ajax_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
// add_action( 'wp_ajax_nopriv_debuneUpdateAccountTransaction_ajax_request', 'debuneUpdateAccountTransaction_ajax_request' );

