<?php

/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * Validate NXT Token for user's login
 * @param type $user
 * @param type $username
 * @param type $password
 * @return \WP_Error
 */
function checkNXTtoken($user, $username, $password) {
   if ( is_a($user, 'WP_User') ) { return $user; }

   if ( !isset($_POST['nxtToken'] ) || (isset($_POST['nxtToken']) && $_POST['nxtToken']=="") ) { // If User Don't user NXT Token String - exit
        return;
   }

    $nxt = new deBuNe_NXT_API();

    if (is_null($nxt->lasthost)) {
        return new WP_Error('valid_username', '<strong>ERROR:</strong>no active peers found for NXT authentication.' );
    }

    $check =  $nxt->checkToken($_POST['nxtToken']); 

    switch ( $check ) {
        case false:
            return new WP_Error('valid_username', '<strong>ERROR:</strong>NOT valid NXT token.' );
            break;
        // CASE 1
        default:
            if ( username_exists($check) ) {
                return get_user_by('login', $check);
            } else {
                wp_create_user( $check, $password, '' );
                return get_user_by('login', $check);
            }
        // CASE 0
   }
} 

/**
 * get the list of peers from peerexchange and update the admin option
 */
function updateNxtPeers() {
    $nxtUpdatePeers = get_option('nxtUpdatePeers');
    if ($nxtUpdatePeers == '1') {
        $nxt = new deBuNe_NXT_API();
        $nxt->updatePeers($notifyAdmin = true);
    }
}

/**
 * ajax to verify if an object (account or asset) exists in the blockchain
 */
function debuneVerifyObjExists_ajax_request() {
    // only allow POST requests (eg. to avoid CORS from external domains..)
    if ( isset($_POST) ) {
        // check to see if the submitted nonce matches with the
        // generated nonce we created earlier
        if (!isset($_POST['nonce'])) die ( 'Busted!');
        $nonce = sanitize_text_field($_POST['nonce']);
        if (!wp_verify_nonce( $nonce, 'debune-ajax-nonce' ) ) die ( 'Busted!');
        
        if (isset($_POST['nxtObjID']) && isset($_POST['nxtObjType'])) {
            $nxt = new deBuNe_NXT_API();
            $nxt->ObjID = sanitize_text_field($_POST['nxtObjID']);
            $nxt->ObjType = sanitize_text_field($_POST['nxtObjType']);
            
            if( $nxt->currentObjectExists() ) {
                $res =array(
                    'success' => 'yes',
                    'msg' => $_POST['nxtObjType'] . __( ' succesfully verified.',DEBUNE_WP_NXT_SLUG )
                );
            } else {
                $res =array(
                    'success' => 'no',
                    'msg' => $_POST['nxtObjType'] . __( ' does not exist in the blockchain.',DEBUNE_WP_NXT_SLUG )
                );
            }
        } else {
            $res =array(
                'success' => 'no',
                'msg' => __("ID or object type not defined",DEBUNE_WP_NXT_SLUG)
            );
        }
    } else {
        $res =array(
            'success' => 'no',
            'msg' => __("sorry, only POST requests are allowed here!",DEBUNE_WP_NXT_SLUG)
        );
    }
    wp_send_json( $res );
}
add_action( 'wp_ajax_debuneVerifyObjExists_ajax_request', 'debuneVerifyObjExists_ajax_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
// add_action( 'wp_ajax_nopriv_debuneVerifyObjExists_ajax_request', 'debuneVerifyObjExists_ajax_request' );
