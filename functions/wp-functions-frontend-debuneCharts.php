<?php

/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

//
//
// functions implement and render debune charts in worpress posts and pages
//
//

/**
 * ajax to populate the chart with data
 * @note this function returns the series and basic configuration object for every kind of chart
 * @note every serie must be retured as:
                array(
                        type: type of chart representing this serie (eg. 'candlestick' or 'column'),
                        name: 'name of the serie',
                        data: array containing the data 
                )
 * 
 * TODO (data array is to be mapped according to a transcodification array ("serie's name" => sequence of attributes to be mapped into the serie)),
 */
function debuneInitChart_ajax_request() {
    //global $wpdb;
    // only allow POST requests (eg. to avoid CORS from external domains..)
    if ( isset($_POST) ) {
        // check to see if the submitted nonce matches with the
        // generated nonce we created earlier
        if (!isset($_POST['nonce'])) die ( 'Busted!');
        $nonce = sanitize_text_field($_POST['nonce']);
        if (!wp_verify_nonce( $nonce, 'debune-ajax-nonce' ) ) die ( 'Busted!');
        
        /**
         * for this to work we need:
            'chartID' => account or asset ID,
            'formatAttributes' => "Yes" or any other value will be considered "No",
            'chartQueryParams' => array containing:
                    'className' => the AR model's name,
                    'attributes' => attributes to be queried (eg. subtypes for accountTransactions),
                    'filter' => filters to be applied
            'chartConfig' => array containing a custom chart configuration object (it will be merged with the one taken from the json model in js/models/charts/),
            'chartOptions' => hash array containing all options to select and customize the chart
                // standard options are:
                // - 'model' the chart's json model name (eg. 'highstock-candle-volume')
                // - 'width' the chart's container width
                // - 'height' the chart's container height
         */
        if (isset($_POST['chartID']) && isset($_POST['formatAttributes']) && isset($_POST['chartQueryParams']) && isset($_POST['chartOptions'])) {
            
            // non mandatory parameters
            $chartConfig = isset($_POST['chartConfig']) ? sanitize_str_array($_POST['chartConfig']) : array();

            $chartID = sanitize_text_field($_POST['chartID']);
            $formatAttributes = sanitize_text_field($_POST['formatAttributes']);
            $chartQueryParams = sanitize_str_array($_POST['chartQueryParams']);
            $chartOptions = sanitize_str_array($_POST['chartOptions']);

            // get the chart's structure from corresponding json file
            $chartModel = json_decode(file_get_contents(DEBUNE_WP_NXT_URL_CHART_MODELS . $chartOptions['model']. ".json"), true);
            $chartConfig = array_merge_recursive($chartModel, $chartConfig);
            // override chart template's title
            $chartConfig['title']['text'] = stripslashes($chartOptions['title']);
            
            // get chart's credits
            $chartConfig['credits'] = json_decode(file_get_contents(DEBUNE_WP_NXT_URL_CHART_MODELS . "credits.json"), true);
            
            // get the mappings between transaction subtypes and values to display on charts
            $chartMappings = json_decode(file_get_contents(DEBUNE_WP_NXT_URL_MODELS . "map-transactions-chart-values.json"), true);
            $chartTransactionMappings = $chartMappings['transactionTypes'];
            
            $arModelName = $chartQueryParams['className'];
            $attributes = $chartQueryParams['attributes'];
            
            // we want an array of transactions separated by attribute (subtype)
            $getSeries = true;
            if ($arModelName == "debune_account_transactions") {
                // get all transactions, of all selected options (type/subtype) for this account
                $transactions = debune_account_transactions::getAccountTransactionsData($chartID, $attributes, true, true);
            }

            // implementing filter
            $filteredTransactions = array();
            if (isset($chartQueryParams['filter']) && $chartQueryParams['filter'] != "" && !empty($chartQueryParams['filter'])) {
                $filter = sanitize_str_array($chartQueryParams['filter']);
                $filter = array_filter($filter);
                
                if ($getSeries) {
                    foreach ($attributes as $attribute) {
                        if (isset($transactions[$attribute])) {
                            foreach ($transactions[$attribute] as $transaction) {
                                $keepIt = true;
                                foreach ($filter as $attrName => $filterValue) {
                                    if (isset($transaction[$attrName]) && strpos($transaction[$attrName],$filterValue) !== false) {
                                        $keepIt = true;
                                    } else {
                                        $keepIt = false;
                                        break;
                                    }
                                }
                                if ($keepIt) {
                                    $filteredTransactions[$attribute] = $transaction;
                                }
                            }
                        }
                    }
                } else {
                    foreach ($transactions as $transaction) {
                        $keepIt = true;
                        foreach ($filter as $attrName => $filterValue) {
                            if (isset($transaction[$attrName]) && strpos($transaction[$attrName],$filterValue) !== false) {
                                $keepIt = true;
                            } else {
                                $keepIt = false;
                                break;
                            }
                        }
                        if ($keepIt) {
                            $filteredTransactions[] = $transaction;
                        }
                    }
                }
                $transactions = $filteredTransactions;
            }

            /*
             * $chartQueryParams['sort'] is a hash array with this structure:
                            array(
                                'attribute' => attribute to use for sorting
                                'type' => ""
                                'direction' => "asc" or "desc"
                                'priority' => 1 (means it will be the first attr to be sorted in a multi-attribute sort)
                            )
             */
            
            // always sort data for charts by timestamp ascending
            $chartQueryParams['sort'] = array(
                'attribute' => "timestamp",
                'type' => "",
                'direction' => "asc",
                'priority' => 1
            );
            if (isset($chartQueryParams['sort']) && $chartQueryParams['sort'] != "" && !empty($chartQueryParams['sort'])) {
                $sort = sanitize_str_array($chartQueryParams['sort']);
                $order = $sort['direction'];
                //$order = $sort['direction'] == 'asc' ? SORT_ASC : SORT_DESC;
                $attrName = $sort['attribute'];
                $attrType = $sort['type'];
                $sortingAlgorithm = $attrType !== "" && !empty($attrType) ? $attrType : 'natural';
                if ($getSeries) {
                    foreach ($attributes as $attribute) {
                        if(is_null($transactions[$attribute])) {
                            $a=1;
                        }
                        multiArraySort($transactions[$attribute], $attrName, $order, $sortingAlgorithm);
                        $totItems[$attribute] = count($transactions[$attribute]); // counter for grid's total items and pagination
                    }
                } else {
                    multiArraySort($transactions, $attrName, $order, $sortingAlgorithm);
                    $totItems = count($transactions); // counter for grid's total items and pagination
                }
                
            }
            
            // preparing data for charts (data chosen for the series depend on every chart)
            switch ($chartOptions['model']) {
                case "highstock-candle-volume":
                    $series[0] = array(
                        "type" => "candlestick",
                        "name" => "'ID ASSET or (asset name)'",
                        "data" => "", //TODO! implement a function to extract OHLC data from the model,
                    );
                    $series[1] = array(
                        "type" => "candlestick",
                        "name" => "'ID ASSET or (asset name)'",
                        "data" => "", //TODO! implement a function to extract OHLC data from the model,
                    );
                    // injecting the new data into chart structure
                    $newseries = array_merge_recursive($chartConfig['series'], $series);
                    $chartConfig['series'] = $newseries;

                    break;
                case "highcharts_timeline" || "highstock_line":
                    $series = array();
                    $i = 0;
                    foreach ($transactions as $subtype => $subTransaction) {
                        //TODO! change $transaction['amountnxt'] with the mapped attribute for that element (the one that corresponds to the y value for that subtype)
                        $series[$i] = array(
                            'name' => $subtype,
                        );
                        
                        foreach ($subTransaction as $transaction) {
                            // value defaults to amountnxt if not specified by some mappings
                            $value = $transaction['amountnxt'];
                            
                            // extract the right value for every transaction's subtype
                            // TODO! mappings will be done directly by users using a table instead of using a json model
                            $chartType=false;
                            $chartColor=false;
                            $dataMagnitude=1;
                            $dataGrouping=array(); // data grouping is a sub array of the series
                            $dataGroupingFunc=false;
                            //$dataGroupingMinWidth=50; // minimum distance between data where grouping is performed
                            foreach ($chartTransactionMappings as $transType) {
                                foreach ($transType['subtypes'] as $transSubtype) {
                                    if ($transSubtype['description'] == $subtype) {
                                        $attribute = $transSubtype['chart_value_fields'];
                                        // if we pass an array of attributes (comma separated string), pass to the function an array with corresponding values
                                        $attributes = explode(',',$attribute);
                                        if (count($attributes) > 1) {
                                            $attrValue = array();
                                            foreach ($attributes as $attr) {
                                                $attrValue[] = recursiveFindByKey($transaction, $attr);
                                            }
                                        } else {
                                            $attrValue = recursiveFindByKey($transaction, $attribute);
                                        }
                                        // call the user function defined in chart's template
                                        if ($transSubtype['function'] !== "") {
                                            $chartFuncLibrary = new deBuNe_chart_functions();
                                            // only allow execution of functions defined as methods of this class, to avoid user been able to execute arbitrary php functions
                                            if (method_exists($chartFuncLibrary,$transSubtype['function'])) {
                                                $func = $transSubtype['function'];
                                                $value = $chartFuncLibrary->$func($attrValue);
                                            } else {
                                                throw new Exception("Attention! function {$transSubtype['function']} is not allowed");
                                            }
                                            
                                        }
                                        if (isset($transSubtype['chart_type']) && $transSubtype['chart_type'] !== "") {
                                            $chartType = $transSubtype['chart_type'];
                                        }
                                        if (isset($transSubtype['chart_color']) && $transSubtype['chart_color'] !== "") {
                                            $chartColor = $transSubtype['chart_color'];
                                        }
                                        if (isset($transSubtype['data_magnitude']) && $transSubtype['data_magnitude'] !== "") {
                                            $dataMagnitude = $transSubtype['data_magnitude'];
                                        }
                                        if (isset($transSubtype['data_grouping_func']) && $transSubtype['data_grouping_func'] !== "") {
                                            $dataGroupingFunc = $transSubtype['data_grouping_func'];
                                        }
                                        if (isset($transSubtype['data_grouping_minwidth']) && $transSubtype['data_grouping_minwidth'] !== "") {
                                            $dataGroupingMinWidth = $transSubtype['data_grouping_minwidth'];
                                        }
                                        
                                        
                                        break;
                                    }
                                }
                            }
                            // configuring series (data, type of chart and style)
                            // @note $dataMagnitude is the value used to normalize this series (default to 1)
                            // @note $dataGrouping is the function to be applied to the series when data are grouped (eg. by month or year): see http://api.highcharts.com/highstock#plotOptions.series.dataGrouping for details on available functions
                            
                            // change sign for y axes data if it refers to a transaction
                            if ($transaction['direction'] === 'out') {
                                $value = -$value;
                            }
                            $series[$i]['data'][] = array($transaction['timestamp_unix_js'], (float)$value*$dataMagnitude);
                            if ($chartType) {
                                $series[$i]['type'] = $chartType;
                            }
                            if ($chartColor) {
                                $series[$i]['color'] = $chartColor;
                            }
                            $series[$i]['dataGrouping']['groupPixelWidth'] = 50;
                            if ($dataGroupingFunc) {
                                $series[$i]['dataGrouping']['approximation'] = $dataGroupingFunc;
                            }
                            if ($dataGroupingMinWidth) {
                                $series[$i]['dataGrouping']['groupPixelWidth'] = $dataGroupingMinWidth;
                            }
                            // de-normalize tooltip values otherwise normalized data will look wrong
                            if ($dataMagnitude != 0) {
                                $series[$i]['tooltip']['pointFormatter'] = "<span style=\"color:'+ this.series.color +'\">'+ this.series.name +'</span>: <b>'+ this.y / ".$dataMagnitude." +'</b><br/>";
                            }
                            
                        }
                        $i +=1;
                    }
                    // injecting the new data into chart structure
                    $newseries = array_merge_recursive($chartConfig['series'], $series);
                    $chartConfig['series'] = $newseries;

                    break;

                default:
                    break;
            }
            $res = array(
                'success' => 'yes',
                'msg' => __("Data successfully retrieved",DEBUNE_WP_NXT_SLUG),
                //'data' => $transactions,
                'chartConfig' => $chartConfig,
                'totItems' => $totItems
            );
            
        } else {
            $res =array(
                'success' => 'no',
                'msg' => __("Wrong parameters (or no parameters) posted",DEBUNE_WP_NXT_SLUG)
            );
        }
    } else {
        $res =array(
            'success' => 'no',
            'msg' => __("sorry, only POST requests are allowed here!",DEBUNE_WP_NXT_SLUG)
        );
    }
    
    wp_send_json( $res );
}
add_action( 'wp_ajax_debuneInitChart_ajax_request', 'debuneInitChart_ajax_request' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_debuneInitChart_ajax_request', 'debuneInitChart_ajax_request' );
