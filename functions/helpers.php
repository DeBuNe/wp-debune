<?php

/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * missing function in wordpress
 * @param type $array
 * @return type
 */
function sanitize_str_array($array) {
    return array_map_recursive('sanitize_text_field', $array);
}

/**
 * implements array map recursively
 * @param callable $func
 * @param array $arr
 * @return array
 */
function array_map_recursive(callable $func, array $arr) {
    array_walk_recursive($arr, function(&$v) use ($func) {
        $v = $func($v);
    });
    return $arr;
}

/**
 * encode an array into various formats
 * @note this has been developed to normalize the way we encode attributes to be passed to javascript (eg if we don't specify JSON_HEX_APOS in json_encode, single quotes won't be escaped, giving an error in angularjs functions)
 * @param type $format (default json)
 * @param type $depth (default 512) the depth of attributes array to consider
 * @return string encoded variable
 */
function encodeArray($array, $format='json', $depth=512) {
    switch ($format) {
        case 'json':
            $encoded = json_encode($array, JSON_HEX_APOS);

            break;

        default:
            break;
    }

    return $encoded;
}

/**
 * unset a global variable
 * @param type $varName
 */
function unsetGlobal($varName) {
    unset($GLOBALS[$varName]);
}

/**
 * this function is used as a part of an multidimensional array sorting algorithm
 * @note this function has to be called by usort in this way:
 *              usort($array, build_sorter('key name', 'order asc or desc', 'sortingAlgorithm: natural, number'));
 * @param type $key
 * @param type $order
 * @param type $sortingAlgorithm
 * @return type
 */
function build_sorter($key, $order, $sortingAlgorithm='natural') {
    return function ($a, $b) use ($key, $order, $sortingAlgorithm) {
        switch ($sortingAlgorithm) {
            case 'natural':
                $cmp = strnatcmp($a[$key], $b[$key]);
                break;
            case 'number':
                $cmp = (float)$a[$key] - (float)$b[$key];
                $cmp = $cmp === 0 ? 0 : ($cmp < 0 ? -1 : 1);
                break;

            default:
                $cmp = strnatcmp($a[$key], $b[$key]);
                break;
        }
        return $order === "asc" ? $cmp : -$cmp;
    };
}

/**
 * sort a multidimensional hash array by key => value pairs
 * @param type $array
 * @param type $attrName
 * @param type $order
 * @param type $sortingAlgorithm
 * @return type
 */
function multiArraySort(&$array, $attrName, $order, $sortingAlgorithm){
    if (!is_null($array)) {
        //usort doesn't get executed if array contains only one element.... (suckers!)
        if(count($array) == 1) {
            return $array;
        } else {
            return usort($array, build_sorter($attrName, $order, $sortingAlgorithm));
        }
    } else {
        return null;
    }
    
}

/**
 * finds an array key, recursively into complex array structures
 * @param array $array
 * @param type $needle
 * @return type
 */
function recursiveFindByKey(array $array, $needle)
{
    $iterator  = new RecursiveArrayIterator($array);
    $recursive = new RecursiveIteratorIterator($iterator,
                         RecursiveIteratorIterator::SELF_FIRST);
    foreach ($recursive as $key => $value) {
        if ($key === $needle) {
            return $value;
        }
    }
}

function array_merge_recursive_unique($array1, $array2) {
  if (empty($array1)) return $array2; //optimize the base case

  foreach ($array2 as $key => $value) {
    if (is_array($value) && is_array(@$array1[$key])) {
      $value = array_merge_recursive_unique($array1[$key], $value);
    }
    $array1[$key] = $value;
  }
  return $array1;
}

function array_merge_recursive_simple() {

    if (func_num_args() < 2) {
        trigger_error(__FUNCTION__ .' needs two or more array arguments', E_USER_WARNING);
        return;
    }
    $arrays = func_get_args();
    $merged = array();
    while ($arrays) {
        $array = array_shift($arrays);
        if (!is_array($array)) {
            trigger_error(__FUNCTION__ .' encountered a non array argument', E_USER_WARNING);
            return;
        }
        if (!$array)
            continue;
        foreach ($array as $key => $value)
            if (is_string($key))
                if (is_array($value) && array_key_exists($key, $merged) && is_array($merged[$key]))
                    $merged[$key] = call_user_func(__FUNCTION__, $merged[$key], $value);
                else
                    $merged[$key] = $value;
            else
                $merged[] = $value;
    }
    return $merged;
}
