<?php

/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

//
//
// functions implement and manage single attributes shortcodes
//
//

//
// register a new menu item for DeBuNe Charts account shortcodes post type: start
//

add_action('admin_menu', 'debune_shortcodegenerator_menu'); 
function debune_shortcodegenerator_menu() { 
    //add_menu_page( 'Shortcodes', 'DeBuNe Shortcode', 'manage_options', 'debune-shortcode-generator','debune_shortcode_page_view', 'dashicons-editor-rtl',7);
    
    //add_submenu_page('edit.php?post_type=debune_chart', 'Shortcodes', 'Shortcode generator', 'manage_options', 'debune-shortcode-generator','debune_shortcode_page_view'); 
    //add_submenu_page('tools.php?', 'Shortcodes', 'Shortcode generator', 'manage_options', 'debune-shortcode-generator','debune_shortcode_page_view'); 
    $page = add_management_page( 'Shortcodes', 'DeBuNe Shortcode generator', 'manage_options', 'debune-shortcode-generator','debune_shortcode_page_view' );
}

function debune_shortcode_html_template() {
    ?>
        <div id="debuneShortcodes" ng-app="debuneShortcodes">
            <div id="debuneShortcodesCtrl" ng-controller="debuneShortcodesCtrl" data-ng-init='init()'>
                <form>
                    <div>
                        <label style="display: block;" for="objID"><?php echo _e( 'Object ID (account or asset)', DEBUNE_WP_NXT_SLUG ); ?>:</label>
                        <input type="text" name="objID" id="objID" size="24" value="<?php echo $chart_obj_id; ?>"
                            ng-model="objID"
                            ng-change="updateDropdowns();"
                            ng-model-options="{ updateOn: 'blur' }">
                    </div>
                    <div>
                        <label style="display: block;" for="objType"><?php echo _e( 'Object Type', DEBUNE_WP_NXT_SLUG ); ?></label>
                        <select id="objType" name="objType"
                            ng-model="objType" 
                            ng-options="objTypeId as objTypeDetails.label for (objTypeId, objTypeDetails) in objTypes"
                            ng-change="updateDropdowns();">
                            <option value="">-- choose object --</option>
                        </select>
                    </div>
                    <div ng-show="objExists">
                        <label style="display: block;" for="objAttributes"><?php echo _e( 'Object attributes', DEBUNE_WP_NXT_SLUG ); ?></label>
                        <select name="objAttributes"
                            ng-show="objType"
                            ng-model="objAttributes"
                            ng-options="objAttributesId as objAttributesDetails.label  for (objAttributesId, objAttributesDetails) in objTypes[objType]['objAttributes']"
                            ng-change="updateDropdowns();">
                            <option value="">-- choose attribute --</option>
                        </select>
                        <br>
                        <h3 ng-show="objAttributes">Shortcode: </h3>
                        <div class="shortcodebox" ng-if="objAttributes">[debune output="attribute" objType="{{objType}}" id="{{objID}}" attributes="{{objAttributes}}" formatted="Yes"]</div>
                    </div>
                </form>                
            </div>
        </div>
    <?php
}

function debune_shortcode_page_view() {
    ?>
    <div class="wrap"><div id="icon-tools" class="icon32"></div>
        <h2><?php echo __( 'DeBuNe Shortcode generator',DEBUNE_WP_NXT_SLUG ); ?></h2>
        <h3>Within this page is possible to generate shortcodes for variuos &#39;single tag&#39; elements, such as an account balance or an account Description, to be placed directly into a regular WordPress page or post.</h3>
        <br>
        <?php 
            debune_shortcode_html_template();
        ?>
    </div>
    <?php
}


//
// add a metabox to post and pages to the shortcode generator
//

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function debune_shortcode_add_meta_box() {

	$screens = array( 'post', 'page' );

	foreach ( $screens as $screen ) {
                add_meta_box(
                        'debune_shortcode_sectionid',
                        __( 'DeBuNe Shortcode Generator', DEBUNE_WP_NXT_SLUG ), 
                        'debune_shortcode_meta_box_callback',
                        $screen,
                        'side', 
                        'high'
                );
	}
}
add_action( 'add_meta_boxes', 'debune_shortcode_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function debune_shortcode_meta_box_callback( $post ) {

	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'debune_shortcode_meta_box', 'debune_shortcode_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$value = get_post_meta( $post->ID, '_my_meta_value_key', true );
        
        debune_shortcode_html_template();
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function debune_shortcode_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['debune_shortcode_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['debune_shortcode_meta_box_nonce'], 'debune_shortcode_meta_box' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	// Make sure that it is set.
	if ( ! isset( $_POST['debune_shortcode_new_field'] ) ) {
		return;
	}

	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['debune_shortcode_new_field'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_my_meta_value_key', $my_data );
}
add_action( 'save_post', 'debune_shortcode_save_meta_box_data' );
