��          �   %   �      @     A     I  �   ^  (        *     7     E     W  0   w     �     �     �  2   �  |   #     �     �  Q   �          2     R     ^  +   p  <   �  w   �  �  Q     D     S  �   q  +   .	     Z	     i	     x	  @   �	  A   �	     
  )   (
     R
  m   p
  �   �
     u     �  ^   �       -        F     V  0   p  O   �  �   �                          
              	                                                                                      Add New Add New DeBuNe Chart Check below if you want to have the list of available peers automatically updated for you (every 15 mintues) or leave it unchecked if you want to provide your own Custom post type to manage DeBuNe Charts DeBuNe Chart DeBuNe Charts Edit DeBuNe Chart Enter default port of NXT nodes Enter hostname of NXT nodes separated with comma New DeBuNe Chart No DeBuNe charts found in Trash No debune charts found No servers available, authorization will not work. Note: tokens never expire and their validation, performed by any NXT peer, certifies that account effectively belongs to you Once every 15 minutes Parent DeBuNe Chart: RNS list cannot be updated due to a connection timeout with PeerExplorer.com at:  Search DeBuNe Charts To log in using your NXT Token: Using nodes View DeBuNe Chart click on 'NXT button and insert that token' generate a token using 'wp-nxt.com' as data (without quotes) open your NXT client and generate a token (using java client provided by nxt.org, go to 'generate token' from top menu) Project-Id-Version: debune-wp-nxt
POT-Creation-Date: 2015-03-04 12:31+0800
PO-Revision-Date: 2015-03-04 12:31+0800
Last-Translator: Stefano Galassi <galaxy73.it@gmail.com>
Language-Team: DeBuNe <capodieci@gmail.com>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.4
X-Poedit-Basepath: ./
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_x
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 Aggiungi Nuovo Aggiungi nuovo Grafico DeBuNe Seleziona la check box sotto se vuoi che la lista dei peers venga aggiornata automaticamente da WordPress ogni 15 minuti o lascia deselezionata se vuoi utilizzare una lista dei peers fissa Custom Post Type per gestire Grafici DeBuNe Grafico DeBuNe Grafici DeBuNe Aggiorna Grafico DeBuNe Inserisci la porta di Default per i peers NXT (normalmente 7876) Inserisci gli host names dei peers (nodi) NXT separati da virgola Nuovo Grafico DeBuNe Nessun Grafico DeBuNe trovato nel Cestino Nessun Grafico DeBuNe trovato Nessun Server disponibile trovato! Autenticazione via NXT Token e aggiornamento grafici NXT non funzioneranno Nota: i token NXT non hanno scadenza e la loro validazione, ad opera di un peer NXT online, certifica che quell'account effettivamente appartiene a te Ogni 15 minuti Grafico DeBuNe 'Padre' (Parent) la lista RNS non puo' essere aggiornata per problemi di connessione con PeerExplorer.com alle: Cerca Grafico DeBuNe Per fare Log In utilizzanto il tuo Token NXT: Nodi Utilizzati Visualizza Grafico DeBuNe click su sul pulsante NXT e inserisci quel token genera il token inserendo 'wp-ntx.com' nel campo 'data' del token (senza apici) apri il tuo NXT client e genera un token (utilizzando il client java da nxt.org, per generare un token, click su 'generate token' dal menu in alto a destra) 