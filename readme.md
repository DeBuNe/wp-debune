=== Plugin Name ===

DeBuNe wp-nxt plugin (wp-nxt.com)
Contributors: iltoga
Donate link: http://wp-nxt.com
Tags: DeBuNe, NXT, Token, Data Analytics, Finance, Cryptocurrency, Crypto Currency, Crypto Platform
Requires at least: 4.0.0
Tested up to: 4.1.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

DeBuNe wp-nxt plugin (wp-nxt.com) is a WordPress plugin that allow users to:
* log into wordpress using an NXT token (thanks to scor2k, developer of nxtAuth plugin)
* create data analytics for any NXT account or asset using an assisted composer that will generates a WordPress shortcode, that can be placed into any post of page
* create tags with data coming from an account or asset, using another assisted composer that will generates a WordPress shortcode, that can be placed into any post of page


Note: if you don't know about NXT crypto platform, have a look to these links

* What is NXT: http://NxtCrypto.org/  [EN]
* NXT WiKi: http://wiki.nxtcrypto.org/ [EN][RU]

== Installation ==

## SERVER CONFIGURATION

###Php Server:

* Php Server 5.5.x or above is recommended (to allow use of functions such as array_column that perform faster on array calculations needed to reconcile blockchain data with db models)
* use of Xcache Component (xcache.lighttpd.net) is strongly recommended (or, if you don't like it, at least use any OPCode caching component)

###MySql Server:

**IMPORTANT** mySql Server must be configured to accept large data, to be able to use dbcache component (that drastically reduces number of API calls to the blockchain).
To do so:

* run this query (from a mySql client or phpMyAdmin): show variables like 'max_allowed_packet'
* if this value is lower than 10M (that value will be shown in bytes, so just divided it by 1000..), open my.cnf (usually /etc/mysql/my.cnf) and change:

[mysqld]
change: max_allowed_packet = "whatever"
with: max_allowed_packet = 10M


P.S. restart mysql server to commit configuration's changes

###Web Server (here we cover only optimization of Nginx):

In the "http" directive (server wide) add this line:

fastcgi_read_timeout 600s;

**should fix "upstream timed out (110: Connection timed out) while reading" when dealing with large data requests** 


In the "Server" directive for the website holding the wordpress application with wp-nxt plugin, add this line:

nginx proxy_max_temp_file_size 0

**This disables buffering of files on tmp files (recommended as some queries return large json data that is better to send directly to the php server)**


### Wordpress Configuration files

To activate debug mode for the plugin add, to wp-config.php:

```
#!php

define('DEBUNE_USE_LOCALHOST', true);

```

* connects only to localhost instead of connecting to live peers

```
#!php

define('DEBUNE_DEBUG', true);

```

* loads non minified version of js libraries and logs various messages to browser console


== Changelog ==

= 1.0 =


## PLUG-IN SETTINGS AND DOCS


## HOW TO UPDATE PLUGINS


## THEME


### Custom functions


### OPEN ISSUES


### WHISHLIST




### external links

how to create a wordpress readme.txt file: http://generatewp.com/plugin-readme/