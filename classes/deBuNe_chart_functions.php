<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of deBuNe_chart_public static functions
 *
 * @author demo
 */
class deBuNe_chart_functions {
    
    public static function debune_count($values) {
        return count($values);
    }
    
    /**
     * just a dummy public static function to return the same value as the one given..
     * @param type $value
     * @return type
     */
    public static function dummyCalc($value) {
        return $value;
    }
    public static function debune_mul($values) {
        $values = array_filter($values);
        if (!empty($values)) {
            return array_product($values);
        } else {
            return null;
        }
    }
    public static function debune_add($values) {
        $values = array_filter($values);
        if (!empty($values)) {
            return array_sum($values);
        } else {
            return null;
        }
    }

}
