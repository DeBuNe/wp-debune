<?php

/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * This class extends ActiveRecord Model Class to add specific functionalities to models
 */
class ActiveRecordExtended extends ActiveRecord\Model {
    
        /**
         * cache or not cache data for this model
         * @var type 
         */
        public $cacheData = true;
        
        /**
         * time to live for cached variables
         * @var type 
         */
        public $cacheTTL = 0; // cache results forever
        
        /**
         * attributes that are part of the blockchain but not of the model
         * @note they are either retrieved directly from the blockchain or from a cache component (Xcache or others)
         * @var array 
         */
        public $virtualAttributes = array();
        
        /**
         * array with value pairs 'attribute name' => 'format'  that specifies the custom format to apply to some attributes
         * eg. 
         *              array(
         *                  'balancenqt' => 'nxtcurrency'
         *                  'timestamp' => 'date'
         *                  )
         * @var Array
         */
        public $attributesFormat = array();
        
        /**
         * override magic __get method to include 'virtual attributes' coming from the blockchain data
         * @note with this method, AFTER THE MODEL HAS BEEN FILLED WITH DATA COMING FROM THE BLOCKCHAIN, it is possible to get any (existing) virtual attribute the same way as we would get a model's attribute, eg.: 
         *                  $model = new debune_account();
         *                  // $name will contain the account's name
         *                  $name = $model->name
         * @param type $name
         * @return type
         */
        public function &__get($name) {
            if (isset($this->virtualAttributes[$name])) {
                return $this->virtualAttributes[$name];
            } else {
                return parent::__get($name);
            }
        }
        
        /**
         * extract values from an array recursively
         * @param type $array
         * @param type $maxDepth maximum depth to extract
         * @param type $curDepth starts with 1 and increments during recurisve callbacks
         * @return type
         */
        private function extractArrayRecursive($array, $maxDepth, $curDepth = 1) {
            $res=array();
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    if ($curDepth < $maxDepth) {
                        $res[$key] = $this->extractArrayRecursive($value, $maxDepth, $curDepth+1);
                    }
                } else {
                    $res[$key] = $value;
                }
            }
            return $res;
        }
        
        /**
         * returns an array with all attributes and virtual attributes (full record)
         * @param type $depth is the maximum depth of data (virtualattributes) we want to extract
         * @return Array
         */
        public function getAllAttributes( $depth = 512 ) {
            $res = $this->extractArrayRecursive($this->virtualAttributes, $depth);
            return array_merge($this->to_array(), $res);
        }
        
        /**
         * encode attributes array (including the one from the blockchain if loaded into the model) into various formats
         * @note this has been developed to normalize the way we encode attributes to be passed to javascript (eg if we don't specify JSON_HEX_APOS in json_encode, single quotes won't be escaped, giving an error in angularjs functions)
         * @param type $format (default json)
         * @param type $depth (default 512) the depth of attributes array to consider
         * @return string encoded variable
         */
        public function encodeAttributes($format='json', $depth=512) {
            $attr = $this->getAllAttributes($depth);
            switch ($format) {
                case 'json':
                    $encoded = json_encode($attr, JSON_HEX_APOS);

                    break;

                default:
                    break;
            }
            
            return $encoded;
        }

        /**
        * Mass update the model with data from an attributes hash.
        * @note this method set only attributes that exist in the model's attribute array, all the rest is skipped
        *
        * Unlike update_attributes() this method only updates the model's data
        * but DOES NOT save it to the database.
        *
        * @see update_attributes
        * @param array $attributes An array containing data to update in the form array(name => value, ...)
        * @param boolean $guard_attributes Set to true to guard protected/non-accessible attributes
        */
	public function set_available_attributes(array $attributes, $guard_attributes=true) {
		//access uninflected columns since that is what we would have in result set
		$table = static::table();
		$exceptions = array();
		$use_attr_accessible = !empty(static::$attr_accessible);
		$use_attr_protected = !empty(static::$attr_protected);
		$connection = static::connection();

		foreach ($attributes as $name => $value) {
                    // is a normal field on the table
                    if (array_key_exists($name,$table->columns)) {
                        $value = $table->columns[$name]->cast($value,$connection);
                        $name = $table->columns[$name]->inflected_name;
                        if ($guard_attributes) {
                                if ($use_attr_accessible && !in_array($name,static::$attr_accessible))
                                        continue;

                                if ($use_attr_protected && in_array($name,static::$attr_protected))
                                        continue;

                                // set valid table data
                                try {
                                        $this->$name = $value;
                                } catch (UndefinedPropertyException $e) {
                                        $exceptions[] = $e->getMessage();
                                }
                        }
                        $this->assign_attribute($name,$value);
                    } else {
                        // if attribute doens't exist in the table definition, try to set it as 'virtual attribute'
                        $this->virtualAttributes[$name] = $value;
                    }
		}
                
                //turn all virtual attribute array keys to lower case
                $this->virtualAttributes = $this->array_change_key_case_recursive($this->virtualAttributes);

                if (!empty($exceptions))
                        throw new UndefinedPropertyException(get_called_class(),$exceptions);
	}

        	/**
	 * Find records in the database (in a "Safer mode").
	 * @note this adds the possibility to manage the RecordNotFound exception when calling as "find('pk','safe')": adding 'safe' as last argument, all known exceptions (such as RecordNotFound) will return false (instead of an exception) when triggered, then application won't stop running
	 *
	 * Finding by the primary key:
	 *
	 * <code>
	 * # queries for the model with id=123
	 * YourModel::find(123);
	 *
	 * # queries for model with id in(1,2,3)
	 * YourModel::find(1,2,3);
	 *
	 * # finding by pk accepts an options array
	 * YourModel::find(123,array('order' => 'name desc'));
	 * </code>
	 *
	 * Finding by using a conditions array:
	 *
	 * <code>
	 * YourModel::find('first', array('conditions' => array('name=?','Tito'),
	 *   'order' => 'name asc'))
	 * YourModel::find('all', array('conditions' => 'amount > 3.14159265'));
	 * YourModel::find('all', array('conditions' => array('id in(?)', array(1,2,3))));
	 * </code>
	 *
	 * Finding by using a hash:
	 *
	 * <code>
	 * YourModel::find(array('name' => 'Tito', 'id' => 1));
	 * YourModel::find('first',array('name' => 'Tito', 'id' => 1));
	 * YourModel::find('all',array('name' => 'Tito', 'id' => 1));
	 * </code>
	 *
	 * An options array can take the following parameters:
	 *
	 * <ul>
	 * <li><b>select:</b> A SQL fragment for what fields to return such as: '*', 'people.*', 'first_name, last_name, id'</li>
	 * <li><b>joins:</b> A SQL join fragment such as: 'JOIN roles ON(roles.user_id=user.id)' or a named association on the model</li>
	 * <li><b>include:</b> TODO not implemented yet</li>
	 * <li><b>conditions:</b> A SQL fragment such as: 'id=1', array('id=1'), array('name=? and id=?','Tito',1), array('name IN(?)', array('Tito','Bob')),
	 * array('name' => 'Tito', 'id' => 1)</li>
	 * <li><b>limit:</b> Number of records to limit the query to</li>
	 * <li><b>offset:</b> The row offset to return results from for the query</li>
	 * <li><b>order:</b> A SQL fragment for order such as: 'name asc', 'name asc, id desc'</li>
	 * <li><b>readonly:</b> Return all the models in readonly mode</li>
	 * <li><b>group:</b> A SQL group by fragment</li>
	 * </ul>
	 *
	 * @throws {@link RecordNotFound} if no options are passed or finding by pk and no records matched
	 * @return mixed An array of records found if doing a find_all otherwise a
	 *   single Model object or null if it wasn't found. NULL is only return when
	 *   doing a first/last find. If doing an all find and no records matched this
	 *   will return an empty array.
	 */
	public static function findSafe(/* $type, $options */) {
            $args = func_get_args();
            // if arguments are passed as array and we only have one argument (such as: 'all' or the primary key), convert the argument into a string
            /*
            if (is_array($args) && count($args) == 1) {
                $args = $args[0];
            }
             * 
             */
            $found = true;
            try {
                if (is_array($args)) {
                    $record = call_user_func_array('parent::find', $args);
                } else {
                    $record = parent::find($args);
                }
                
            } catch (Exception $exc) {
                if ($exc instanceof \ActiveRecord\RecordNotFound) {
                    $found = false;
                } else {
                    // if the exception is not coded, go on the 'old' way
                    throw new Exception($exc);
                }
            }

            if ($found) {
                return $record;
            } else {
                return false;
            }
	}
        
        /**
         * delete a model and all given related models
         * @note this only deletes relative models if relation is ActiveRecord\HasMany (one-to-many), to avoid deletion of parent records
         * @param array $relations
         */
        public function deleteWithRelations(array $relations) {
            // check if attribute is a relationships
            if (empty($relations)) {
                $this->delete();
            } else {
                $this->transaction(function($this) use ($relations) {
                    $table = static::table();
                    foreach ($relations as $name) {
                        if (($relationship = $table->get_relationship($name))) {
                            // prudencially we delete only related records if the relation is one-to-many, to avoid deleting parent models
                            if ($relationship instanceof ActiveRecord\HasMany) {
                                $relatedModels = $this->$name;
                                foreach ($relatedModels as $relatedModel) {
                                    $relatedModel->delete();
                                }
                            }
                        }
                    }
                    $this->delete();
                    return true; //commit!
                });
            }
        }
        
        /**
         * drop a line to the log
         * @param type $data
         */
        public static function debune_log($data) {
            file_put_contents(__DIR__ . '/../../log.txt', $data, FILE_APPEND);
        }
        
        /**
         * log an error message and throw an exception
         * @param type $exceptionType (default "Exception")
         * @param type $msg (see php Exception class)
         * @param type $code (see php Exception class)
         * @param type $previous (see php Exception class)
         * @throws Exception
         */
        public static function log_exception($exceptionType="Exception", $msg='', $code='', $previous='') {
            static::debune_log($msg, $code, $previous);
            throw new $exceptionType($msg, $code, $previous);
        }
        
        /**
         * cache a variable
         * @param type $key
         * @param type $value
         * @param type $TTL
         * @param string $cacheComponent (default 'DBCache') set the cacher that will store cached content
         */
        public function setCache( $key, $value, $TTL=0, $cacheComponent = 'DBCache' ) {
            // if we have xcache use it, otherwise use file cache (TODO)
            if (class_exists($cacheComponent)) {
                $cache = $cacheComponent::getInstance();
                $cache->$key = array(
                    'value' => $value,
                    'TTL' => $TTL
                );
            }
        }
        
        /**
         * get a cached variable
         * @param type $key
         * @param string $cacheComponent (default 'DBCache') set the cacher that will store cached content
         * @return type
         */
        public function getCache($key, $cacheComponent = 'DBCache') {
            // if we have xcache use it, otherwise use file cache (TODO)
            if (class_exists($cacheComponent)) {
                $cache = $cacheComponent::getInstance();
                return $cache->$key;
            }
        }
        
       /**
        * unset a cached variable
        * @param type $key
        * @param string $cacheComponent (default 'DBCache') set the cacher that will store cached content
        */
        public function clearCachedKey($key, $cacheComponent = 'DBCache') {
            // if we have xcache use it, otherwise use file cache (TODO)
            if (class_exists($cacheComponent)) {
                $cache = $cacheComponent::getInstance();
                unset($cache->$key);
            }            
        }
        
        /**
         * algorithm to generate a valid cached key
         * @note to generate a key we use an encoded version of the parameters used to request data to the blockchain
         * @note eg.
         * 
                                $queryParams = array(
                                    'requestType' => 'getAccount',
                                    'account' => 'NXT-HNQU-HTEQ-5QDF-BZLSQ',
                                    'includeLessors' => 'false',
                                    'includeAssets' => 'false',
                                    'includeCurrencies' => 'false',
                                );
         *
         * @note this array will be encoded into a json string first and into a 20 bytes (characters) sha1 hash
         * 
         * @param array $queryParams parameters used to build the query against the blockchain
         * @param array $extraParams (optional) extra parameters used to identify a record/object/model
         * @return type
         */
        protected function buildCacheKey($queryParams, $extraParams=array()) {
            $params = array_merge($queryParams, $extraParams);
            return sha1(json_encode($params));
        }
        
        /**
         * recursively change all keys in an array to lower or upper case
         * @note useful to turn all keys of virtual attributes into lowercase to sync with real model's attributes (activerecord attributes), that are automatically converted to lowercase..
         * @param type $input
         * @param type $case
         */
        protected function array_change_key_case_recursive($input, $case = CASE_LOWER) {
            $input = array_change_key_case($input, $case);
            foreach ($input as $key => $val) {
                if (is_array($val))
                    $input[$key] = $this->array_change_key_case_recursive($val, $case);
            }
            return $input;
        }

        /**
         * format an attribute using a mask
         * @param string $mask something like "%s%s.%s%s%s.%s%s%s/%s%s%s%s-%s%s"
         * @param string $attr
         * @return string formatted attribute
         */
        public function formattedAttribute($attrName) {
            $attr = $this->$attrName;
            if (array_key_exists($attrName, $this->attributesFormat)) {
                switch ($this->attributesFormat[$attrName]) {
                    case 'nxtcurrency':
                        $attr = number_format($attr, $decimals = 2, $floatSeparator = ".", $thousandsSeparator = "'");
                        break;

                    default:
                        break;
                }
            }
            return  $attr;
        }

}
