<?php
/**
 * class for nxt accounts
 */
class debune_account extends debune_ActiveRecord
{
	// explicit table name since our table is not "arTest"
        // FIXME some versions of php server don't play well with dynamically assigned static variables...
	//static $table_name = DEBUNE_TABLE_PREFIX.'debune_accounts';
	static $table_name = 'wp_debune_accounts';

	// explicit pk since our pk is not "id"
	static $primary_key = 'id';
        
        /**
         * id of the blockchain hash array that maps to model's id
         * @note if is left empty (=''), it means that this model doesn't have an id that maps an id in the blockchain (eg. this API call to the blockchain always returns a single record)
         * @var type 
         */
        static $blockChainID = 'accountRS';
        
        /**
         * define what key, in the resulting hash array from the blockchain, corresponds to the root of subArray (sub set) containing all records to be attached to the models
         * @note defaults to false that means no subset (this API call returns a single record). for account transactions, for instance, should be: "transactions"
         * @var string
         */
        static $blockChainSubSetIndex = false;
        
        /**
         * model relations
         * @var array
         */
        static $has_many = array(
            array(
                'transactions_out',
                'class_name' => 'debune_account_transactions',
                'foreign_key' => 'senderRS',
                'primary_key' => 'id',
            ),
            array(
                'transactions_in',
                'class_name' => 'debune_account_transactions',
                'foreign_key' => 'recipientRS',
                'primary_key' => 'id',
            ),
            array(
                'account_assets',
                'class_name' => 'debune_asset',
                'foreign_key' => 'accountRS',
                'primary_key' => 'id',
            )
        );        
        
        /**
         * blockchain relationships (mapping between model and 'related' blockchain objects)
         * @note this is used to be able to get related models already filled with data from the blockchain
         * @note source is the name of current model's field that maps into the (foreign) name of the related blockchain object (described in the related model)
         * @var array
         */
        static $blockChainRelationShips = array(
            'transactions_out' => array(
                'keymapping' => array(
                    'source' => 'id',
                    'foreign' => 'account',
                )
            ),
            'transactions_in' => array(
                'keymapping' => array(
                    'source' => 'id',
                    'foreign' => 'account',
                )
            )
        );
        
        /**
         * parameters specific for this model to retrieve data from the blockchain
         * @var array
         */
        public $blockChainCallParams = array(
            'requestType' => 'getAccount',
            'includeLessors' => 'true',
            'includeAssets' => 'true',
            'includeCurrencies' => 'true',
        );
        
        /**
         * name of the parameter considered to be the ID to be used to retreive data from the blockchain
         * @note commonly, every API call has an attribute used as an ID (required to perform the API call)
         * @var type 
         */
        static $blockChainCallRequiredParam = 'account';
        
        /**
         * if set to true, forces retrieveFromBolckChain to always update the cache with results from the blockchain (eg. for transactions)
         * @var type 
         */
        static $alwaysUpdate = false;
        
        /**
         * time to live for cached variables
         * @var type 
         */
        public $cacheTTL = 86400; // cache results for 1 day
        
        /**
         * array with value pairs 'attribute name' => 'format'  that specifies the custom format to apply to some attributes
         * eg. 
         *              array(
         *                  'balancenqt' => 'nxtcurrency'
         *                  'timestamp' => 'date'
         *                  )
         * @var Array
         */
        public $attributesFormat = array(
            'balancenqt' => 'nxtcurrency',
            'balancenxt' => 'nxtcurrency',
            'guaranteedbalancenqt' => 'nxtcurrency',
            'guaranteedbalancenxt' => 'nxtcurrency'
        );
        
        /**
         * getter method to retrieve current balanceNQT in NXT coutervalue
         * @note NXT = NQT * 10^-8
         * @return type
         */
        public function get_balancenxt() {
            if (isset($this->balancenqt)) {
                return $this->balancenqt / 100000000;
            } else {
                return '';
            }            
        }
        
        /**
         * getter method to retrieve current guaranteedBalanceNQT in NXT coutervalue
         * @note NXT = NQT * 10^-8
         * @return type
         */
        public function get_guaranteedbalancenxt() {
            if (isset($this->guaranteedbalancenqt)) {
                return $this->guaranteedbalancenqt / 100000000;
            } else {
                return '';
            }
        }
        
        /**
         * returns an array with all attributes and virtual attributes (full record)
         * @note this function overrides the one in activerecord_extended display the extra (computed) attributes set in this model via getter methods
         * @return Array
         */
        public function getAllAttributes($depth=512) {
            $attributes = parent::getAllAttributes($depth);
            // setting extra virtual attributes (based on previuosly defined model's getters)
            $attributes['balancenxt'] = $this->balancenxt;
            $attributes['guaranteedbalancenxt'] = $this->guaranteedbalancenxt;
            return $attributes;
        }
}
