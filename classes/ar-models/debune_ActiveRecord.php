<?php

/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * This class extends ActiveRecord Model Class to add Blockchain specific functionalities to models
 */
class debune_ActiveRecord extends ActiveRecordExtended {
    
	// explicit connection name since we always want production with this model
	static $connection = 'production';
        
        /**
         * the blockchain hash array key that maps to model's id
         * @note if is left empty (=''), it means that this model doesn't have an id that maps an id in the blockchain and the id is an autoincrement field
         * @var type 
         */
        static $blockChainID = '';
        
        /**
         * defines what key, in the resulting hash array from the blockchain, corresponds to the root of subArray (sub set) containing all records to be attached to the models
         * @note defaults to false that means no subset (this API call returns a single record). for account transactions, for instance, should be: "transactions"
         * @var string
         */
        static $blockChainSubSetIndex = false;
        
        /**
         * if $blockChainSubSetIndex is set, we expect that this model is an extension of another model (eg. account_transactions extends transaction) and all queries to return an array of models (eg. an array of all transactions for this account)
         * to be able to transparently perform queries on the sub-model we need to define all attributes that map for the primary key of this model
         * eg. for account_transaction, the primary key ($model->id) contains the account number that maps to 'senderRS' and 'recipientRS' into the sub-model (transaction)
         * setting this mapping allow us to perform a query like this:
         * debune_account_transactions::findbyIDWithBlockData('NXT-L6FM-89WK-VK8P-FCRBB')
         * @note this will produce this active record query to the sub-model that will match the getAccountTransactions API call results: 
         *              debune_account_transactions::findWithBlockData(array('all', array('conditions' => "senderRS = 'NXT-L6FM-89WK-VK8P-FCRBB' OR recipientRS = 'NXT-L6FM-89WK-VK8P-FCRBB'")),array($objType=>$objID));
         * @note not setting this parameter will throw an error when checking if this model should return an array of models instead of a single model
         * @var array
         */
        static $subModelPKMappings = false;
        
        /**
         * in case we set up model's relationships, using this static variable allow to map corresponding API call for related models, allowing to do something like:
         * $this->relatedmodel  //this will return all related models, already feeded with their relative blockchain data
         * exampre of configuration: 
         * - blockchain relations
         * 
         *                   static $blockChainRelationShips = array(
         *                       'transactions_out' => array(
         *                           // this is the relevant part. The mapping bewtween two models into the blockchain
         *                           'accountRS' => 'account'
         *                   )
         * 
         * - model relations
         * 
         *                   static $has_many = array(
         *                       'transactions_out' => array(
         *                           'keymapping' => array(
         *                               'source' => 'id',
         *                               'foreign' => 'account',
         *                           )
         *                       )
         *                   );        
         *
         * 
         *
         * @var array
         */
        static $blockChainRelationShips = array();
           
        /**
         * if set to true, forces retrieveFromBolckChain to always update the cache with results from the blockchain (eg. for transactions)
         * @var type 
         */
        static $alwaysUpdate = true;
        
        /**
         * maps the key name of the earliest timestamp to search for (to perform an API request that includes a timestamp parameter)
         * @note usually should be 'timestamp' (eg. for getAccountTransactions)
         * @var string 
         */
        static $blockChainTimestampIndex = 'timestamp'; // in the final model (class), it should be overridden either with 'height' or 'timestamp' or 'blockchainTimestamp' (see wiki at https://wiki.nxtcrypto.org/wiki/Nxt_API to set the correct index for this API call)
        
        /**
         * parameters specific for this model to retrieve data from the blockchain
         * @var array
         */
        public $blockChainCallParams;
        
        /**
         * name of the parameter considered to be the ID to be used to retreive data from the blockchain
         * @note commonly, every API call has an attribute used as an ID (required to perform the API call). eg. for getAccount, this param is 'account'
         * @var type 
         */
        static $blockChainCallRequiredParam;
        
        /**
         * the referring timestamp to compute actual timestamps
         * @note in case we are using nxt, this is initialized to the genesis block's timestamp
         * @var type  
         */
        protected $genesis;

        /**
         * always have the genesis block in mind!
         */
        public function after_construct() {
            $this->genesis = strtotime('24.11.2013 12:00:00 UTC');
        }
        
        
        //
        // OVERRIDES: start
        //
        
        /*
         * if we ever want to override the "find" function adding some spices ;)
        public static function find() {
            $args = func_get_args();
            $result = call_user_func_array('parent::find', $args);
            return $result;
        }
         */
        
	/**
          * Override this function to be able to fill the relations with data coming from the blockchain
	 * Retrieves an attribute's value or a relationship object based on the name passed. If the attribute
	 * accessed is 'id' then it will return the model's primary key no matter what the actual attribute name is
	 * for the primary key.
	 *
	 * @param string $name Name of an attribute
	 * @return mixed The value of the attribute
	 * @throws {@link UndefinedPropertyException} if name could not be resolved to an attribute, relationship, ...
	 */
	public function &read_attribute($name)
	{
            // if attribute is not set (eg. relation returns null), gracely return the same
            if (!$attr = parent::read_attribute($name)) {
                return $attr;
            }
            
            // check if attribute is a relationships
            $table = static::table();
            $blockChainRelationships = static::$blockChainRelationShips;
            if (($relationship = $table->get_relationship($name)) && isset($blockChainRelationships[$name]['keymapping'])) {
                /** 
                 * as we cannot pass extra parameters for this API call, we always suppose that the API call will perform a full search using the id record if defined in the model:
                 * eg. if model is transactions, params will contain:
                 *              $params = array(
                 *                  'requestType' => 'getTransaction'
                 *                  static::$blockChainID => $this->id
                 *              );
                 */
                $tableClass = $relationship->class_name;
                $model = new $tableClass();
                $id = strtolower($blockChainRelationships[$name]['keymapping']['source']);
                $foreign = $blockChainRelationships[$name]['keymapping']['foreign'];
                if (isset($this->$id)) {
                    $params = array_merge($model->blockChainCallParams, array($foreign => $this->$id));
                    if ($blockData = $model->retrieveFromBolckChain($params, $model->cacheData)) {
                        $attr = $model->fillWithBlockData($relationship->load($this), $blockData);
                    }
                }
            }
            
            return $attr;
	}
        
        //  
        // OVERRIDES: end
        //
        
        //
        // GETTERS/SETTERS: start
        //
        
        /**
         * getter method to retrieve the date and time from a blockchain timestamp
         * @return string the date formatted for 'db' ('Y-m-d H:i:s')
         */
        public function get_timestamp_datetime() {
            $time = $this->timestamp_unix;
            if (!is_null($time)) {
                $date = new ActiveRecord\DateTime("@$time");
                return $date->format($date::$FORMATS['db']);
            }
            return null;
        }
        
        /**
         * getter method to retrieve the date from a blockchain timestamp
         * @return string the date formatted for 'db' ('Y-m-d H:i:s')
         */
        public function get_timestamp_date() {
            $time = $this->timestamp_unix;
            if (!is_null($time)) {
                $date = new ActiveRecord\DateTime("@$time");
                return $date->format('Y-m-d');
            }
            return null;
        }
        
        /**
         * getter method to retrieve the unix timestamp from a blockchain timestamp
         * @return int
         */
        public function get_timestamp_unix() {
            if (isset($this->timestamp)) {
                return $this->timestamp + $this->genesis;
            }
            return null;
        }

        /**
         * getter method to retrieve the unix timestamp from a blockchain timestamp and format for javascript
         * @return int
         */
        public function get_timestamp_unix_js() {
            if (isset($this->timestamp)) {
                return $this->timestamp_unix * 1000;
            }
            return null;
        }

        /**
         * returns the equivalent of Javascript Date.UTC function
         * @note useful with charts for x axis values
         * @return type
         */
        public function get_timestamp_dateutc() {
            $time = $this->timestamp_unix;
            if (!is_null($time)) {
                $date = new ActiveRecord\DateTime("@$time", new DateTimeZone('UTC'));
                return $date->getTimestamp();
            }
            return null;
        }
        
        
        // 
        // GETTERS/SETTERS: end
        // 
        
        /**
         * tells if current model is supposed to return an array of models or a single model when queried with retrieveFromBolckChain
         * @note eg. debune_account_transactions, that fires the getAccountTransactions API call, returns an array of transactions
         * @return type
         */
        public static function isArrayModel() {
            $isArrayModel =  static::$blockChainSubSetIndex ? true : false;
            if ($isArrayModel) {
                // check if $subModelPKMappings has been set, otherwise throw an exeption!
                if (!static::$subModelPKMappings) {
                    throw new Exception('static::$subModelPKMappings must be set for this model, as it is supposed to return records from a sub-model',500);
                }
            }
            return $isArrayModel;
        }
        

        /**
         * a generic method to request data to the blockChain
         * @param array $params
         * @param bool $cache (default true) if we want to cache blockchain results or not
         * @note if $model->cacheData is false NO DATA will be cached even if $cache is set to true
         * @param string $cacheComponent (default 'DBCache') set the cacher that will store cached content
         * @param bool $doNotDecode (default fals) if true, returns the json string instead of an array
         * @return mixed (array with the cached results or json string)
         */
        public static function retrieveFromBolckChain($params = array(), $cache = true, $cacheComponent = 'DBCache') {
            $class_name = get_called_class();
            $staticModel = new $class_name();
            $blockChainSubSetIndex = static::$blockChainSubSetIndex;
            
            $params = array_merge($staticModel->blockChainCallParams, $params);
            if ($cache && $staticModel->cacheData) {
                $key = $staticModel->buildCacheKey($params);
                if($cached = $staticModel->getCache($key, $cacheComponent)) {
                    $blockChaintimeStamp = false;
                    // some cache component returns a single value, some other an array containing the value and a timestamp corresponding to the time where this value has been chached
                    if (is_array($cached)) {
                        $blockChaintimeStamp = isset($cached['timestamp']) ? $cached['timestamp'] : false;
                        // cachedVal always contain a json string
                        $cachedVal = $cached['value'];
                    } else {
                        // cachedVal always contain a json string
                        $cachedVal = $cached;
                    }

                    /**
                     * TO BE TESTED!!!!!!!!!!!!!!!!!!!
                     * if static::$alwaysUpdate is set to true
                     * 1. add here a recursive call to retrieves all increments starting from a 'master' query to the blockchain (if possible, make an api call to "lastBlockchainFeeder", retrieved using getBlockchainStatus)
                     * 2. merge new data with the cached value and update the cache
                     */
                    if (static::$alwaysUpdate && $blockChaintimeStamp) {
                        
                        $params = array_merge($params, array(static::$blockChainTimestampIndex => $blockChaintimeStamp));
                        // don't use the cache for recursive calls
                        $remainingBlocks = $staticModel->retrieveFromBolckChain($params, false);
                        if (!empty($remainingBlocks) && $remainingBlocks) {
                            $cachedVal = json_encode(array_merge(json_decode($cachedVal, true), $remainingBlocks));
                            //$cachedVal = array_merge_recursive($cachedVal, $remainingBlocks);
                            //
                            // as we are treating two strings, just concatenate them together
                            //$cachedVal = $cachedVal . $remainingBlocks;
                            $staticModel->setCache($key, $cachedVal, $staticModel->cacheTTL, $cacheComponent);
                        }
                    }
                    
                    $res = json_decode($cachedVal,true);
                    // check if there are 'missing records' in db (records that have been deleted, for instance), to always keep the db in sync with the blockchain (cache)
                    $staticModel->updateFromBlockChain(array('blockchain_array'=>$res), $updateMode='append', $cache = true, $cacheComponent = 'DBCache');
                    return $res;
                }
            }
            
            // retrieve live data from the blockchain and put them in cache (if $cache is true)
            $nxt = new deBuNe_NXT_API();
            $res = $nxt->requestData($params); // as we want to put results from API call into the cache, we have to return a json formatted string instead than an array (which is the default behavior of requestData function)
            // if this API call returns multiple records (subset), as for getAccountTransactions, process the subset only
            $res = $blockChainSubSetIndex ? $res[$blockChainSubSetIndex] : $res;
            
            if ($res) {
                // always update (create) models when we have uncached live data (THIS WILL SYNCRONIZE MODELS WITH BLOCKCHAIN DATA)
                $staticModel->updateFromBlockChain(array('blockchain_array'=>$res), $updateMode='append', $cache = true, $cacheComponent = 'DBCache');
                
                if (!empty($res) && $cache && $staticModel->cacheData) {
                    $staticModel->setCache($key, json_encode($res), $staticModel->cacheTTL, $cacheComponent);
                }
            }
            return $res;
        }
        
        /**
         * retrieve data from the blockchain and creates/updates the model/s
         * @param string $updateMode insertion mode of data coming from the blockchain:
         * @note possible modes are
         * @note        update (default): overwrites all data already into the db
         * @note        append: append only new data
         * @note        new: deletes data that don't match with the one into the blockchain
         * @param array $params either params to fire an API call to the RNS or array('blockchain_array'=>"the json string or array containing data to be put into models"), in case we already have these data
         * @param bool $cache (default true) if we want to cache blockchain results or not
         * @param string $cacheComponent (default 'DBCache') set the cacher that will store cached content
         * @return type
         */
        public static function updateFromBlockChain($params = array(), $updateMode='append', $cache = true, $cacheComponent = 'DBCache') {
            // if blockchain_array is passed as parameter, we get data from that instead of doing an API call to RNS
            if (isset($params['blockchain_array'])) {
                $blockChainRecords = $params['blockchain_array'];
            } else {
                if (!$blockChainRecords = static::retrieveFromBolckChain($params, $cache, $cacheComponent)) {
                    return false;
                }
            }
            
            $priKeyBlockChainAttr = static::$blockChainID;
            $blockChainSubSetIndex = static::$blockChainSubSetIndex;
            $modelName = get_called_class();
            
            // cannot update a model if there is no mapping between data coming from the blockchain and the model itself
            if ($priKeyBlockChainAttr == '') {
                static::log_exception("Exception in debune_ActiveRecord::updateFromBlockChain: no mapping found between blockchain and model");
            }
            
            switch ($updateMode) {
                case "append":
                    // append (create) a single model
                    if (!$blockChainSubSetIndex) {
                        static::createFromBlockChain($blockChainRecords);
                    } else {
                        // if subset is empty, return false, so we never cache an empty dataset
                        // @note subset can be in a form of an array of models or a model with a subset (sub-Array of models)
                        // @note in case of model with a subset, we suppose that the key for the sub-Array is = $blockChainSubSetIndex
                        if (isset($blockChainRecords[$blockChainSubSetIndex])) {
                            $blockChainSubSet = $blockChainRecords[$blockChainSubSetIndex];
                        } else {
                            $blockChainSubSet = $blockChainRecords;
                        }
                        if (is_array($blockChainSubSet)) {
                            foreach ($blockChainSubSet as $blockChainRecord) {
                                // if something went wrong during the transaction, rollback!
                                if (!$modelName::createFromBlockChain($blockChainRecord)) {
                                    throw new Exception("createFromBlockChain rollback!");
                                }
                            }
                            /*
                             * TODO for somer reasons transactions doesn't work..
                            // append (create) multiple models at once
                            $modelName::transaction(function($modelName, $blockChainSubSet) {
                                foreach ($blockChainSubSet as $blockChainRecord) {
                                    // if something went wrong during the transaction, rollback!
                                    if (!$modelName::createFromBlockChain($blockChainRecord)) {
                                        throw new Exception("createFromBlockChain rollback!");
                                    }
                                }
                            });
                             * 
                             */
                        }
                        
                    }

                    break;
                case "overwrite":


                    break;

                default:
                    static::log_exception("Exception in debune_ActiveRecord::updateFromBlockChain: updateMode not valid!");
                    //break;
            }
            
            return true;
        }
        
        /**
         * create a new model from a set of (blockchain) attributes
         * @param type $blockChainAttributes
         * @return boolean
         */
        public static function createFromBlockChain($blockChainAttributes) {
            $class_name = get_called_class();
            
            if (!is_array($blockChainAttributes)) {
                $blockChainAttributes = json_decode($blockChainAttributes, true);
            }

            $priKeyAttr = static::$primary_key;
            $priKeyBlockChainAttr = static::$blockChainID;
            $priKey = $blockChainAttributes[$priKeyBlockChainAttr];

            // first check if this model already exists (to avoid duplicate keys)
            if (!$model = static::findSafe($priKey)) {
                $model = new $class_name();
                $model->$priKeyAttr = $priKey;
                $model->set_available_attributes($blockChainAttributes);
                return $model->save();
            } else {
                // if model already exists, don't update it!
                return true;
            }
        }

        /**
         * find one or more models and fill them with their relative data from the blockchain (or cache)
         * @param type $findArgs argument array for find method see ActiveRecord\Model::find()
         * @param type $params extra parameters to include in the API call to the RNS
         * @param array $filter array to filter results
         * @param type $cache (default true) if false, don't cache API call results
         * @param type $cacheComponent (default 'DBCache')
         * @return mixed (model or collection of models if a dataset is retrieved)
         */
        public static function findWithBlockData($findArgs, $params = array(), $cache = true, $cacheComponent = 'DBCache') {
            // first we fire the API call, to be sure that the model is up to date..
            $class_name = get_called_class();
            $staticModel = new $class_name();
            $params = array_merge($staticModel->blockChainCallParams, $params);
            $blockData = $staticModel->retrieveFromBolckChain($params, $cache, $cacheComponent);
            if (is_array($blockData)) {
                if(empty($blockData)) {
                    // no transactions
                    return $blockData;
                }
            } else {
                //sorry no transdata from the RNS
                return false;
            }
            if (is_array($findArgs)) {
                $model = call_user_func_array('static::findSafe',$findArgs);
            } else {
                $model = $staticModel->findSafe($findArgs);
            }
            if ($model) {
                $staticModel->fillWithBlockData($model, $blockData);
                return $model;
            } else {
                // this it shouldn't happen because all models are created when their data are retreived from the blockchain
                return null;
            }
        }

        /**
         * find one or more models and fill them with their relative data from the blockchain (or cache)
         * @param type $ID model's primary key
         * @note in case this model is supposed to retreive an array of sub-models, a key mapping between this model and the sub-model will be used to produce a custo query on the 'sub-model'
         * @param type $cache (default true) if false, don't cache API call results
         * @param type $cacheComponent (default 'DBCache')
         * @return mixed (model or collection of models if a dataset is retrieved)
         */
        public static function findbyIDWithBlockData($ID, $cache = true, $cacheComponent = 'DBCache') {
            $blockchainPK = static::$blockChainCallRequiredParam;
            if (!is_null($blockchainPK)) {
                if (static::isArrayModel()) {
                    // create the condition string to query the sub-model
                    $conditions = array();
                    foreach (static::$subModelPKMappings as $foreignKey) {
                        $conditions[] = "{$foreignKey} = '{$ID}'";
                    }
                    $conditions = implode(' OR ', $conditions);
                    $model = static::findWithBlockData(array('all', array('conditions' => $conditions)), $params = array($blockchainPK => $ID), $cache, $cacheComponent);
                } else {
                    $model = static::findWithBlockData($ID, $params = array($blockchainPK => $ID), $cache, $cacheComponent);
                }
                
                return $model;
            }
            return false;
        }
        
        /**
         * fill a model (or an array of models) with data coming from the blockchain
         * @param type $model
         * @param type $blockData
         * @return type
         */
        public function fillWithBlockData(&$model, $blockData) {
            $priKeyAttr = static::$primary_key;
            $priKeyBlockChainAttr = static::$blockChainID;
            
            // every block data could be:
            // 1. an array containing a sub-array with data (eg. getAccountTransactions)
            // 2. an array containing the block data
            // so we have to extract the data corresponding to this model's virtual attributes
            // this sub-array key must be defined in static::$blockChainSubSetIndex
            $blockChainSubSetIndex = static::$blockChainSubSetIndex;

            // are model and data from blockchain both arrays (it means model contains multiple records..)
            if (is_array($model) && !static::isassoc($blockData)) {
                // extract data corresponding to this model's virtual attributes
                if ($blockChainSubSetIndex) {
                    // if subIndex is set and we are using the parent structure, reduce the array to the child structure 
                    // (eg. if we have the accountTransactions, for every transaction, just take the sub-array accountTransaction[transaction])
                    foreach ($blockData as &$block) {
                        if (isset($block[$blockChainSubSetIndex]) && is_array($block[$blockChainSubSetIndex])) {
                            $block = $block[$blockChainSubSetIndex];
                        }
                    }
                    //unset($block); // I don't know why but this has to be done when modifying arrays by reference...
                }
                
                // multiple models and dataset returned from RNS
                foreach ($model as &$subModel) {
                    $idx = $this->blockchain_search_key($blockData, $priKeyBlockChainAttr, $subModel->$priKeyAttr);
                    if(!is_null($idx) && $idx !== false) {
                        $subModel->set_available_attributes($blockData[$idx]);
                        // reducing data into the blockdata array to speedup a little bit finding remaining records
                        unset($blockData[$idx]);
                    }
                }
                //unset($subModel); // I don't know why but this has to be done when modifying arrays by reference...
            } elseif (is_object ($model)) {
                if (static::isassoc($blockData)) {
                    // extract data corresponding to this model's virtual attributes
                    $this->blockChainMerge($model, $blockData);
                } else {
                    // single model and dataset from blockchain
                    foreach ($blockData as &$block) {
                        if (isset($block[$blockChainSubSetIndex]) && is_array($block[$blockChainSubSetIndex])) {
                            $block = $block[$blockChainSubSetIndex];
                        }
                    }
                    unset($block); // I don't know why but this has to be done when modifying arrays by reference...

                    $idx = $this->blockchain_search_key($blockData, $priKeyBlockChainAttr, $model->$priKeyAttr);
                    if(!is_null($idx)) {
                        $model->set_available_attributes($blockData[$idx]);
                    }
                }
            }
            //return $model;
        }
        
        /**
         * merge blockchain data into model considering that, if subIndex is set and we retreive the parent structure, data should be taken from the sub-array with subIndex key
         * @param type $model
         * @param type $blockData
         */
        private function blockChainMerge(&$model, $blockData) {
            $blockChainSubSetIndex = static::$blockChainSubSetIndex;
            if ($blockChainSubSetIndex) {
                // extract data corresponding to this model's virtual attributes
                if (isset($blockData[$blockChainSubSetIndex]) && is_array($blockData[$blockChainSubSetIndex])) {
                    $blockData = $blockData[$blockChainSubSetIndex];
                }
            }

            // single model and associative array returned from RNS (single record)
            $model->set_available_attributes($blockData);
        }
        
        /**
         * convert a standard date into a NXT timestamp
         * @note "UTC" is set to keep timestamps in sync with the function used to set timestamps in ActiveRecord\Table::set_timestamps() function
         * @return type
         */
        public static function dateToNXTTimestamp($date) {
            $time = date_create_from_format("Y-m-d H:i:s", $date);
            $dateTime = $time . " UTC";
            $timeStamp = strtotime($dateTime);

            return $timeStamp - $this->genesis;
        }       
        
        /**
         * evaluates if an array is associative or numeric indexed
         * @note this is useful when we want to know if results from an API call refers to a single record (=associative array) or a dataset (=numeric indexed)
         * @param type $array
         * @return boolean
         */
        protected static function isassoc($array) {
            $ret = false;
            foreach ($array as $key => $value) {
                if (!is_numeric($key)) {
                    $ret=true;
                    break;
                }
            }
            return $ret;
        }
        
        /**
         * search an array for a given key=>value pairs
         * @note this function performs an object like search on hash arrays
         * @note this function performs much faster with php >= 5.5.x
         * @param type $dataset the whole json array
         * @param type $keyName the key (column) of array to be searched
         * @param type $value value to search
         * @return mixed the index/key (numeric or string) of the array element (if found)
         */
        protected static function blockchain_search_key($dataset, $keyName, $value) {
            $key = null;
            if (static::isassoc($dataset)) {
                $key = isset($dataset[$keyName]) && $dataset[$keyName] == $value ? $keyName : null;
            } else {
                foreach ($dataset as $idx => $data) {
                    if (isset($data[$keyName]) && $data[$keyName] == $value) {
                        $key = $idx;
                        break;
                    }
                }
                /* 
                 * NOT WORKING!
                if((int)DEBUNE_PHPVERSION<540) {
                    // first time we get the array column containing key to be searched, push it into a global array so if we are doing a iterative search trhough many models of the same tipe (eg. account transactions), we don't have to call this function again
                    global $searchedColumns;
                    if (!isset($searchedColumns[$keyName])) {
                        // as this column, theoretically contains unique ids, use a flipped array (keys <-> values), so later we just check if this key (which is the value we wish to find) exists and return the value (which is the key)
                        $searchedColumns[$keyName] = array_flip(array_column($dataset, $keyName));
                    }
                    
                    $key = isset($searchedColumns[$keyName][$value]) ? $searchedColumns[$keyName][$value] : null;
                    // the above method should run much faster!
                    //$key = array_search($value, $searchedColumns[$keyName]);
                } else {
                }
                 * 
                 */
            }
            return $key;
            //return array_search($value, array_column($dataset, $keyName));
        }
        
        /**
         * standard way to get a timestamp
         * @note it must be the same function used to set timestamps in ActiveRecord\Table::set_timestamps() function
         * @return type
         */
        public function createTimestamp() {
            $now = date("Y-m-d H:i:s") . " UTC";
            $timeStamp = strtotime($now);

            return $timeStamp - $this->genesis;
        }
        
}
