<?php
/**
 * This class it is used to specifically retrieve NXT constants (nxt constants are stored into db using the getConstants API call)
 * @note this model is updated to the NXT API ver 1.4.16
 * 
 * @note don't use updateFromBlockChain to update this model as it contains specific attributes that won't make it work!
 * @note instead, use retrieveFromBolckChain and update the model manually
 */
class nxt_constants extends debune_ActiveRecord {
	// explicit table name since our table is not "arTest"
        // FIXME some versions of php server don't play well with dynamically assigned static variables...
        //static $table_name = DEBUNE_TABLE_PREFIX.'debune_constants';
        static $table_name = 'wp_debune_constants';

        // explicit pk since our pk is not "id"
        static $primary_key = 'id';

        static $constantID = 'nxtconstants';
        
        // NXT constants
        private $_constants;
        
        // getters/setters: start
        public function get_genesisAccountId() {
            return $this->_constants['genesisAccountId'];
        }
        public function get_genesisBlockId() {
            return $this->_constants->genesisBlockId;
        }
        public function get_transactionTypesArray() {
            return $this->_constants['transactionTypes'];
        }
        /**
         * get the transaction subtypes, formatted for dropdown models
         * @return array an array containing all transaction subtypes with relative parent types and values (type/subtype values are separated by |)
         * eg. array('Ordinary Payment' => array(
         *                                          'parent' => 'Payment'
         *                                          'label' => 'Ordinary Payment'
         *                                          'value' => '0|0'
         *                                      ))
         */
        public function get_transactionSubTypesArray() {
            $transAry = $this->transactionTypesArray;
            foreach ($transAry as $transType) {
                foreach ($transType['subtypes'] as $subType) {
                    $res[$subType['description']] = array(
                        'parent' => $transType['description'],
                        'label' => $subType['description'],
                        'value' => $transType['value']."|".$subType['value']
                    );
                }
            }
            return $res;
        }
        public function get_transactionTypes() {
            $transAry = $this->transactionTypesArray;
            $res = array();
            foreach ($transAry as $value) {
                $res[] = $value['description'];
            }
            return $res;
        }
        public function get_peerStates() {
            return $this->_constants->peerStates;
        }
        public function get_maxBlockPayloadLength() {
            return $this->_constants->maxBlockPayloadLength;
        }
        public function get_currencyTypes() {
            return $this->_constants->currencyTypes;
        }
        public function get_hashAlgorithms() {
            return $this->_constants->hashAlgorithms;
        }
        
        /**
         * get an array representing current record's constants
         * @return boolean
         */
        public function get_constants() {
            if ($this->constant_data) {
                return json_decode($this->constant_data, true);
            } else {
                try {
                    $nxtConstants = $this->find(static::$constantID);
                } catch (Exception $exc) {
                    // possibly record not found.. return false
                    return "";
                }

                return json_decode($nxtConstants->constant_data, true);
            }
            
        }
        
        // getters/setters: end
        
        /**
         * update constants from the blockchain
         */
        public function updateConstants() {
            $nxt = new deBuNe_NXT_API();
            $params = array(
                'requestType' => 'getConstants',
            );            
            if ($res = $nxt->requestData($params, false)){
                try {
                    $nxtConstants = $this->find(static::$constantID);
                } catch (Exception $exc) {
                    $nxtConstants = false;
                    // let go..
                    //echo $exc->getTraceAsString();
                }

                if($nxtConstants){
                    $nxtConstants->update_attribute('constant_data', $res);
                } else {
                    // first time
                    $this->id = 'nxtconstants';
                    $this->constant_data = $res;
                    $this->save();
                }
                
            } else {
                //throw new Exception("error ". $nxt->error['errorCode'] . ": " . $nxt->error['msg']);
            }
        }
        
        /**
         * this function is called right after model initialization
         */
        public function after_construct() {
            parent::after_construct();
            // populate virtual attributes for this model (above) and if nxt constants are not imported yet, import themo into db from the blockchain
            $this->_constants = $this->constants;
        }
        
        /**
         * get type and subtype (numeric values) of a transaction, given the description (eg. "Ordinary Payment")
         * @param type $description
         * @return type
         */
        public function getTransactionTypesFromDescription($description) {
            $ret = false;
            foreach ($this->transactionTypesArray as $types) {
                if (isset($types['subtypes'])) {
                    foreach ($types['subtypes'] as $subtype) {
                        if ($subtype['description'] == $description) {
                            $ret = array(
                                'type'=>$types['value'],
                                'subtype'=>$subtype['value']
                            );
                            break 2;
                        }
                    }
                }
            }
            return $ret;
        }
        
}
