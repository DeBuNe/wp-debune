<?php
/**
 * class for account transactions
 * @note THIS MODEL, AS ALL "ARRAY AR MODELS" (DERIVED FROM OTHER AR MODELS), CANNOT BE INSTANCIATED WITH new debune_account_transactions() BECAUSE
 *        THE CORRESPONDING DB TABLE DOES NOT EXISTS. THUS ALL METHODS HERE ARE STATICALLY CALLED (eg. debune_account_transactions::findWithBlockData
 * @note this class extends the single transaction class model and its API call retrieves all transactions that belongs to a specific account
 * @note these parameters are allowed to restrict results (remember that every different parameter array generates a different cached value):
 * account is the account ID
 * timestamp is the earliest block (in seconds since the genesis block) to retrieve (optional)
 * type is the type of transactions to retrieve (optional)
 * subtype is the subtype of transactions to retrieve (optional)
 * firstIndex is a zero-based index to the first transaction to retrieve (optional)
 * lastIndex is a zero-based index to the last transaction to retrieve (optional)
 * numberOfConfirmations is the required number of confirmations per transaction (optional)
 * withMessage is true to retrieve only only transactions having a message attachment, either non-encrypted or decryptable by the account (optional)
 */
class debune_account_transactions extends debune_transaction
{
	// explicit table name since our table is not "arTest"
        // FIXME some versions of php server don't play well with dynamically assigned static variables...
	//static $table_name = DEBUNE_TABLE_PREFIX.'debune_transactions';
	static $table_name = 'wp_debune_transactions';
    
        /**
         * if set to true, forces retrieveFromBolckChain to always update the cache with results from the blockchain (eg. for transactions)
         * @var type 
         */
        static $alwaysUpdate = true;
        
        /**
         * define what key, in the resulting hash array from the blockchain, corresponds to the root of subArray (sub set) containing all records to be attached to the models
         * @note defaults to false that means no subset (this API call returns a single record). for account transactions, for instance, should be: "transactions"
         * @var string
         */
        static $blockChainSubSetIndex = 'transactions';
        
        /**
         * if $blockChainSubSetIndex is set, we expect that this model is an extension of another model (eg. account_transactions extends transaction) and all queries to return an array of models (eg. an array of all transactions for this account)
         * to be able to transparently perform queries on the sub-model we need to define all attributes that map for the primary key of this model
         * eg. for account_transaction, the primary key ($model->id) contains the account number that maps to 'senderRS' and 'recipientRS' into the sub-model (transaction)
         * setting this mapping allow us to perform a query like this:
         * debune_account_transactions::findbyIDWithBlockData('NXT-L6FM-89WK-VK8P-FCRBB')
         * @note this will produce this active record query to the sub-model that will match the getAccountTransactions API call results: 
         *              debune_account_transactions::findWithBlockData(array('all', array('conditions' => "senderRS = 'NXT-L6FM-89WK-VK8P-FCRBB' OR recipientRS = 'NXT-L6FM-89WK-VK8P-FCRBB'")),array($objType=>$accountID));
         * @note not setting this parameter will throw an error when checking if this model should return an array of models instead of a single model
         * @var array
         */
        static $subModelPKMappings = array('senderRS','recipientRS');
        
        /**
         * parameters specific for this model to retrieve data from the blockchain
         * @var array
         */
        public $blockChainCallParams = array(
            'requestType' => 'getAccountTransactions'
        );
        
        /**
         * name of the parameter considered to be the ID to be used to retreive data from the blockchain
         * @note commonly, every API call has an attribute used as an ID (required to perform the API call)
         * @var type 
         */
        static $blockChainCallRequiredParam = 'account';
        
        /**
         * cache or not cache data for this model
         * @var type 
         */
        public $cacheData = true;
        
        /**
         * time to live for cached variables
         * @var type 
         */
        public $cacheTTL = 0; // cache forever!
        
        /**
         * store account of who made this getAccountTransactions API call
         * @note this is used to be able to know, if a transaction is in ($account = recipientRS) or out ($account = senderRS)
         * @var type 
         */
        public $requestedBy;
        
        
        /**
         * get all transactions, of all selected options (type/subtype) for this account
         *
         * @param type $accountID
         * @param type $subtypes
         * @param boolean $formatAttributes if = "Yes" it returns attributes formatted according to their specifications
         * @param type $getSeries if true, it returns an associative array of transactions where every key is a transaction subtype
         * @return type
         */
        public static function getAccountTransactionsData($accountID, $subtypes, $formatAttributes=false, $getSeries=false, $cache = true, $cacheComponent = 'DBCache'){
            $nxtConstants = new nxt_constants();
            $mergedTransactions = array();
            if ($formatAttributes==="Yes") $formatAttributes = true;
            
            foreach ($subtypes as $optionName) {
                if(isset($_SESSION['accountTransactions'][$accountID][$optionName])) {
                    if ($getSeries) {
                        $mergedTransactions[$optionName] = $_SESSION['accountTransactions'][$accountID][$optionName];
                    } else {
                        $mergedTransactions = array_merge($mergedTransactions, $_SESSION['accountTransactions'][$accountID][$optionName]);
                    }

                } else {
                    $selectedType = $nxtConstants->getTransactionTypesFromDescription($optionName);
                    if ($transactions = debune_account_transactions::findWithBlockData(array('all', array('conditions' => "(senderRS = '".$accountID."' OR recipientRS = '".$accountID."') AND type = {$selectedType['type']} AND subtype = {$selectedType['subtype']}")),array("account"=>$accountID, 'type'=>$selectedType['type'], 'subtype'=>$selectedType['subtype']),$cache, $cacheComponent, $accountID)) {
                        $transactionsData = array();
                        foreach ($transactions as $transaction) {
                            $transactionsData[] = $transaction->getAllAttributes(512, $formatAttributes);
                        }
                        if ($getSeries) {
                            $mergedTransactions[$optionName] = $transactionsData;
                        } else {
                            $mergedTransactions = array_merge($mergedTransactions, $transactionsData);
                        }
                        
                    }
                    $_SESSION['accountTransactions'][$accountID][$optionName] = $transactionsData;
                }
            }
            return $mergedTransactions;
        }
        
        /**
         * set transactions ownership (account that requested these transaction data)
         * @param type $model
         * @param type $account
         */
        private function setTransactionOwnership(&$model, $account) {
            if (!is_null($account) && $model) {
                if (is_array($model)) {
                    foreach ($model as &$rec) {
                        $rec->requestedBy = $account;
                    }
                } else {
                        $model->requestedBy = $account;
                }
            }
        }
        
        
        //
        //
        // FUNCTION OVERRIDES
        //
        //
        
        
        /**
         * find one or more models and fill them with their relative data from the blockchain (or cache)
         * @note this function is overridden because we need to set the account of user who request the transactions (transaction's owner), for every account transaction request, to be able to get the transactions' direction (in or out from transaction's owner account)
         * @param type $findArgs argument array for find method see ActiveRecord\Model::find()
         * @param type $params extra parameters to include in the API call to the RNS
         * @param array $filter array to filter results
         * @param type $cache (default true) if false, don't cache API call results
         * @param type $cacheComponent (default 'DBCache')
         * @param type $account (default NULL) in cate of getAccountTransactions, this is the NXT account we use to make the API call. it is used to get the transactions direction
         * @return mixed (model or collection of models if a dataset is retrieved)
         */
        public static function findWithBlockData($findArgs, $params = array(), $cache = true, $cacheComponent = 'DBCache', $account=NULL) {
            // first we fire the API call, to be sure that the model is up to date..
            $class_name = get_called_class();
            $staticModel = new $class_name();
            $params = array_merge($staticModel->blockChainCallParams, $params);
            $blockData = $staticModel->retrieveFromBolckChain($params, $cache, $cacheComponent);
            if (is_array($blockData)) {
                if(empty($blockData)) {
                    // no transactions
                    return $blockData;
                }
            } else {
                //sorry no data from the RNS
                return false;
            }
            if (is_array($findArgs)) {
                $model = call_user_func_array('static::findSafe',$findArgs);
            } else {
                $model = $staticModel->findSafe($findArgs);
            }
            
            // set account owner in case this request comes from a getAccountTransactions
            $staticModel->setTransactionOwnership($model, $account);

            // push data from blockchain into db model(s)
            if ($model) {
                $staticModel->fillWithBlockData($model, $blockData);
                return $model;
            } else {
                // this it shouldn't happen because all models are created when their data are retreived from the blockchain
                return null;
            }
        }
        
        
}
