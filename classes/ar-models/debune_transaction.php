<?php
/**
 * class for transaction
 * @note by default the API call fired from this class will retrieve one transaction, specifying:
 * transaction is the transaction ID (optional)
 * fullHash is the full hash of the transaction (optional if transaction ID is provided)
 */
class debune_transaction extends debune_ActiveRecord
{
	// explicit table name since our table is not "arTest"
        // FIXME some versions of php server don't play well with dynamically assigned static variables...
	//static $table_name = DEBUNE_TABLE_PREFIX.'debune_transactions';
	static $table_name = 'wp_debune_transactions';

	// explicit pk since our pk is not "id"
	static $primary_key = 'id';
        
        /**
         * id of the blockchain hash array that maps to model's id
         * @note if is left empty (=''), it means that this model doesn't have an id that maps an id in the blockchain (eg. this API call to the blockchain always returns a single record)
         * @var type 
         */
        static $blockChainID = 'transaction';
        
        /**
         * define what key, in the resulting hash array from the blockchain, corresponds to the root of subArray (sub set) containing all records to be attached to the models
         * @note defaults to false that means no subset (this API call returns a single record). for account transactions, for instance, should be: "transactions"
         * @var string
         */
        static $blockChainSubSetIndex = false;
        
        /**
         * model relations
         * @var type 
         */
        static $belongs_to = array(
            array(
                'sender_account',
                'class_name' => 'debune_account',
                'foreign_key' => 'senderRS',
                'primary_key' => 'id'
            ),
            array(
                'recipient_account',
                'class_name' => 'debune_account',
                'foreign_key' => 'recipientRS',
                'primary_key' => 'id'
            ),
        );
        
        /**
         * blockchain relationships (mapping between model and 'related' blockchain objects)
         * @note this is used to be able to get related models already filled with data from the blockchain
         * @note source is the name of current model's field that maps into the (foreign) name of the related blockchain object (described in the related model)
         * @var array
         */
        static $blockChainRelationShips = array(
            'sender_account' => array(
                'keymapping' => array(
                    'source' => 'senderRS',
                    'foreign' => 'account',
                )
            ),
            'recipient_account' => array(
                'keymapping' => array(
                    'source' => 'recipientRS',
                    'foreign' => 'account',
                )
            ),
        );
        
        /**
         * parameters specific for this model to retrieve data from the blockchain
         * @var array
         */
        public $blockChainCallParams = array(
            'requestType' => 'transaction'
        );
        
        /**
         * name of the parameter considered to be the ID to be used to retreive data from the blockchain
         * @note commonly, every API call has an attribute used as an ID (required to perform the API call)
         * @var type 
         */
        static $blockChainCallRequiredParam = 'account';
        
        /**
         * cache or not cache data for this model
         * @note we don't want to cache every single transaction if we query the blockchain with getTransaction (just to not flood the dbcache table with data)
         * @var type 
         */
        public $cacheData = false;
        
        
        /**
         * getter method to retrieve current feeNQT in NXT coutervalue
         * @note NXT = NQT * 10^-8
         * @return type
         */
        public function get_feenxt() {
            if (isset($this->feenqt)) {
                return $this->feenqt / 100000000;
            } elseif (isset($this->virtualAttributes['feenqt'])) {
                    return $this->virtualAttributes['feenqt'] / 100000000;
            } else {
                return '';
            }
        }
        
        /**
         * getter method to retrieve current feeNQT in NXT coutervalue
         * @note NXT = NQT * 10^-8
         * @return type
         */
        public function get_amountnxt() {
            if (isset($this->amountnqt)) {
                return $this->amountnqt / 100000000;
            } elseif (isset($this->virtualAttributes['amountnqt'])) {
                    return $this->virtualAttributes['amountnqt'] / 100000000;
            } else {
                return '';
            }
        }
        
        /**
         * get the direction of this transaction (out if from this to another account, in if not)
         * @return type
         */
        public function get_direction() {
            return $this->requestedBy === $this->senderrs ? 'out' : ($this->requestedBy === $this->recipientrs ? 'in' : 'unknown');
        }
        
        public function get_sign() {
            return $this->requestedBy === 'out' ? '-' : '';
        }
        
        
        //
        //
        // FUNCTION OVERRIDES
        //
        //
        
        
        /**
         * returns an array with all attributes and virtual attributes (full record)
         * @note this function overrides the one in activerecord_extended to correctly set type/subtype attributes
         * @param type $depth is the maximum depth of data (virtualattributes) we want to extract
         * @param type $formatted return array formatted to be displayed (eg. convert numeric type and subtype to relative names)
         * @return Array
         */
        public function getAllAttributes($depth=512, $formatted=true) {
            $attributes = parent::getAllAttributes($depth);
            if ($formatted) {
                global $nxtConstants;
                $transTypes = $nxtConstants->transactionTypesArray;
                // format type and subtype attributes
                if (isset($attributes['type'])) {
                        $type = $nxtConstants->blockchain_search_key($transTypes, 'value', $attributes['type']);
                        $attributes['type'] = $transTypes[$type]['description'];
                        if (isset($attributes['subtype']) && isset($transTypes[$type]['subtypes'])) {
                            $subType = $nxtConstants->blockchain_search_key($transTypes[$type]['subtypes'], 'value', $attributes['subtype']);
                            $attributes['subtype'] = $transTypes[$type]['subtypes'][$subType]['description'];
                        }
                }
            }
            // setting extra virtual attributes (based on previuosly defined model's getters)
            $attributes['feenxt'] = $this->feenxt;
            $attributes['amountnxt'] = $this->amountnxt;
            $attributes['timestamp_date'] = $this->timestamp_date;
            $attributes['timestamp_datetime'] = $this->timestamp_datetime;
            $attributes['timestamp_dateutc'] = $this->timestamp_dateutc;
            $attributes['timestamp_unix_js'] = $this->timestamp_unix_js;
            $attributes['direction'] = $this->direction;
            $attributes['sign'] = $this->sign;
            return $attributes;
        }
        
}
