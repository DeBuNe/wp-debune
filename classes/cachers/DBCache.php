<?php

/**
 * ActiveRecord class database caching
 */
class DBCacheModel extends ActiveRecord\Model
{
    // explicit table name since our table is not "arTest"
    // FIXME some versions of php server don't play well with dynamically assigned static variables...
    //static $table_name = DEBUNE_TABLE_PREFIX.'debune_cache';
    static $table_name = 'wp_debune_cache';

    // explicit pk since our pk is not "id"
    static $primary_key = 'id';

    // explicit connection name since we always want production with this model
    static $connection = 'dbcache';
    
    /**
     * the referring timestamp to compute actual timestamps
     * @note in case we are using nxt, this is initialized to the genesis block's timestamp
     * @var type  
     */
    protected $genesis;

    public function after_construct() {
        $this->genesis = strtotime('24.11.2013 12:00:00 UTC');
    }
    
    /**
     * standard way to get a timestamp
     * @note it must be the same function used to set timestamps in ActiveRecord\Table::set_timestamps() function
     * @return type
     */
    public function createTimestamp() {
        $now = date("Y-m-d H:i:s") . " UTC";
        $timeStamp = strtotime($now);

        return $timeStamp - $this->genesis;
    }
}

/**
 * DBCache
 * Implements a 'permanent' cache component to store arbitrary data into the db
 * @note data are retrieved using an arbitrary key that MUST be unique
 * @note IMPORTANT the table attribute where to store cached values must be named 'cached_value'
 * @package DBCache
 * @copyright 2015
 * @author DeBuNe <capodieci@gmail.com>
 * @license BSD {@link http://www.opensource.org/licenses/bsd-license.php}
 */
class DBCache {
    
    private static $xcobj;
    

    /**
     * time to live for cached variables
     * @var type 
     */
    public $defaultTTL = 0; // cache results forever
    
    private function __construct()
    {
    }

    public final function __clone()
    {
        throw new BadMethodCallException("Clone is not allowed");
    } 
    
    /**
     * getInstance 
     * 
     * @static
     * @access public
     * @return object DBCache instance
     */
    public static function getInstance() 
    {
        if (!(self::$xcobj instanceof DBCache)) {
            self::$xcobj = new DBCache;
        }
        return self::$xcobj; 
    }

    /**
     * __set 
     * 
     * @param mixed $name 
     * @param mixed $value 
     * @access public
     * @return void
     */
    public function __set($name, $value)
    {
        if (!is_array($value) && !isset($value['value'])) {
            return false;
        }
        if (!isset($value['TTL'])) {
            $value['TTL'] = $this->defaultTTL;
        }
        
        // update existing key or create a new one
        $cache = new DBCacheModel();
        try {
            $model = $cache->find($name);
        } catch (Exception $exc) {
            $model = false;
            //echo $exc->getTraceAsString();
        }
        if (!$model) {
            $model = new DBCacheModel();
            $model->id = $name;
        }
        $model->cached_value = $value['value'];
        $model->ttl = $value['TTL'];
        // always update the timestamp when creating/updating the cached value
        $model->blockchain_timestamp = $model->createTimestamp();
        $model->save();
    }

    /**
     * __get 
     * 
     * @param mixed $name 
     * @access public
     * @return void
     */
    public function __get($name)
    {
        $cache = new DBCacheModel();
        try {
            $model = $cache->find($name);
        } catch (Exception $exc) {
            $model = false;
            //echo $exc->getTraceAsString();
        }
        
        // check if this value is still valid
        if ($model instanceof DBCacheModel && isset($model->ttl) && isset($model->updated_at)) {
            $lastUpdate = $model->blockchain_timestamp;
            $now = $model->createTimestamp();
            if((($now-$lastUpdate) <= $TTL) || ($TTL == 0)) {
                return array(
                    'value'=>$model->cached_value,
                    'timestamp'=>$model->blockchain_timestamp
                );
            } else {
                //invalidate expired cached value and return false (commented because is actually better to let _set cache update this record)
                //unset($this->$name);
            }
        }
        return false;
    }

    /**
     * __isset 
     * 
     * @param mixed $name 
     * @access public
     * @return bool
     */
    public function __isset($name)
    {
        $model = new DBCacheModel();
        try {
            $found = $model->find($name) ? true : false;
        } catch (Exception $exc) {
            $found = false;
            //echo $exc->getTraceAsString();
        }
        
        return $found;
    }

    /**
     * __unset 
     * 
     * @param mixed $name 
     * @access public
     * @return void
     */
    public function __unset($name)
    {
        $model = new DBCacheModel();
        try {
            $cachedValue = $model->find($name);
        } catch (Exception $exc) {
            $cachedValue = false;
            //echo $exc->getTraceAsString();
        }
        
        if ($cachedValue) {
            $cachedValue->delete();
        }
    }

}
