<?php
/* 
  Copyright 2014 DeBuNe (capodieci@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */

/**
 * Description of deBuNe_NXT_API
 *
 * @author DeBuNe
 */
class deBuNe_NXT_API {
    const nxt_API_version = '1.4.16'; // current nxt API version
    
    protected $hosts = array(); // ip:port
    protected $genesis;
    protected $postfix = '/nxt';
    protected $requestsCount = 0;
    public $lasthost;
    public $ObjID;
    public $ObjType;
    public $error;
    public $debug = false;

    /**
     * Class constructor
     * @param type $keepAlive (default TRUE) if false, skip peer availability and uses the first one
     */
    public function __construct($keepAlive = false) {
        $this->genesis = strtotime('24.11.2013 12:00:00 UTC');
        $this->init($keepAlive);
    }

    public function init($keepAlive) {
        // put nxtConstants into a global variable so we don't have to create/destroy a new object all the time
        global $nxtConstants; 
        $nxtConstants = new nxt_constants();
        
        if ( !defined( 'DEBUNE_DEBUG' ) )
                define( 'DEBUNE_DEBUG', false );
        
        if ( !defined( 'DEBUNE_USE_LOCALHOST' ) )
                define( 'DEBUNE_USE_LOCALHOST', false );
        
        $this->debug = DEBUNE_DEBUG;
        
        $this->setHosts();
        
        if ($this->debug || DEBUNE_USE_LOCALHOST) {
            $this->lasthost = 'http://localhost:7876'.$this->postfix;
        } else {
            $found = false;
            for ( $i=0; $i<count($this->hosts); $i++ ){
                $t = $this->hosts[$i];
                if ($keepAlive){
                    if ( $this->isAlive($t) ) {
                        $this->lasthost = 'http://'.$t.$this->postfix;
                        $found = true;
                    }
                } else {
                        $this->lasthost = 'http://'.$t.$this->postfix;
                        $found = true;
                }
                if ($found) { break; }
            }
        }
    }
    
    //
    // GETTERS/SETTERS: start
    //

        /**
         * retrieve a timestamp of the last block created in the blockchain
         * @return boolean
         */
        public function get_blockchainTimestamp() {
            if ($res = $this->requestData(array('requestType' => 'getBlockchainStatus'))) {
                return $res['time'];
            }
            return false;
        }
        
        /**
         * get current (php Server) timestamp in seconds since the genesis block
         * @note is the difference, between Server timestamp and the genesis timestamp and can be used to compare a blockchain timestamp
         * @return type
         */
        public function get_currentTimestamp() {
            $now = date("Y-m-d H:i:s") . " UTC";
            $timeStamp = strtotime($now);
            
            return $timeStamp - $this->genesis;
        }

    //
    // GETTERS/SETTERS: end
    //
    
    

    /**
     * Updates the list of available peers downloading it from PeerExplorer.com Top peers
     * @param type $notifyAdmin
     * @return boolean
     */
    public function updatePeers($notifyAdmin = true) {
        $url = "http://peerexplorer.com/api_openapi_hallmark_version";
        $res = $this->sendRequest($url, array(), 55);
        if ( ! $res ) { 
            if ($notifyAdmin){
                wp_mail( get_bloginfo( 'admin_email' ), __("RNS list cannot be updated due to a connection timeout with PeerExplorer.com at: ", DEBUNE_WP_NXT_SLUG) . current_time( 'mysql' ) );
            }
            return false; 
        } else { 
            $res = json_decode($res, true);
            if (isset($res['peers']) && !empty($res['peers'])){
                // option containing a list of hosts IPs (without port)
                update_option('nxtHostname', implode(',', $res['peers']));
                $this->saveHosts($res['peers']);
            } else {
                if ($notifyAdmin){
                    wp_mail( get_bloginfo( 'admin_email' ), __("Empty answer from PeerExplorer.com at: ", DEBUNE_WP_NXT_SLUG) . current_time( 'mysql' ) );
                }
            }
        }
        return $res['peers'];
    }
    
    /**
     * save an array of IP addresses into wordpress db (option) as a list of hosts (IP:port)
     * @param type $peers
     */
    public function saveHosts($peers) {
        // get port for NXT NRS network (default 7876)
        $nxtP = get_option('nxtPort');
        if (is_null($nxtP) || $nxtP == "") {
            $nxtP = '7876';
        }
        $hh = array();
        for ( $i=0; $i<count($peers); $i++ ) {
            if ( strlen($peers[$i]) < 7 ) continue;
            $hh[$i] = $peers[$i].":".$nxtP;
        }
        $hh = implode(',',$hh);
        update_option('nxtHH', $hh);
    }

    /**
     * get available peers
     * @param type $getListFromOptions (default TRUE) if false get the list form an online source
     * @param type $notifyAdmin (default TRUE) if false don't notify admin via email if something went wrong
     */
    public function setHosts($getListFromOptions = true, $notifyAdmin = true) {
        if ($getListFromOptions) {
            // Get hosts from options
            $hh = get_option('nxtHH');
        } else {
            // Get hosts from online provider
            $hh = implode(',',$this->updatePeers($notifyAdmin));
        }
        $hosts = array();
        if ( strlen($hh) > 5 && explode(',', $hh) && !$this->debug ) {
            // randomize hosts order so that every API call will be randomly done (maximize network performance and security)
            $hosts = explode(',', $hh);
            shuffle($hosts);
        } else {
            $hosts[0] = 'localhost:7876';
        }
        // check is_array 
        if ( ! is_array($hosts) ) {
            $tmpHosts = array();
            $tmpHosts[0] = $hosts;
            $hosts = $tmpHosts;
        }

        $this->hosts = $hosts;
    }
    
    /**
     * generate the request to the peer and get results either using php file_get_contents or cUrl
     * @note if first peer fails, the function will try with others as many times as we have declared in $tryOuts param or if there aren't more peers in the local list
     * @param string $url
     * @param array $params
     * @param int $timeout default 5 (sec)
     * @param string $funcType 'filegetcontents'|'cUrl' (default 'filegetcontents')
     * @param int $tryOuts how many times try to contact peers before giveing up
     * @return string response (json encoded)
     */
    private function sendRequest($url, $params, $timeout = 5, $funcType = 'filegetcontents', $tryOuts = 5) {
        // TODO FIXME! check why we have double quotes in host ip address... (for now we just clean the string)
        $url = str_replace('"', '', $url);
        
        if ($this->requestsCount >= $tryOuts) {
            return false;
        }
        if ($funcType == "filegetcontents") {
            $res = @file_get_contents($url , false, stream_context_create( array(
                'http' => array( 
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params),
                    'timeout' => $timeout
                )
            )));
        } else {
            $data = http_build_query($params);
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,$timeout);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $res = curl_exec($ch);
            curl_close($ch);
        }
        
        // do recursive calls to try with another peer if current one timeouts
        if (!$res) {
            $this->requestsCount += 1;
            // strip down last 4 chars from current host url ("/nxt")
            $postfixLen = strlen($this->postfix);
            $curHost = substr($this->lasthost, 0, -$postfixLen);
            $curHost = substr($curHost, 7); // remove http:// too..
            // remove timed out peer from hosts
            if(($key = array_search($curHost, $this->hosts)) !== false) {
                unset($this->hosts[$key]);
            }
            // if list of hosts is finished and option to autoupdate the list is checked, refresh the list of available hosts from a service provider (eg. peerexplorer.com)
            if (count($this->hosts) == 0) {
                // use the external function to update peers as it could take care of other parameters (eg. in wordpress plugin it update peers from internet only is a checkbox in admin interface is checked)
                updateNxtPeers();
                $this->setHosts();
            }
            
            foreach ($this->hosts as $host) {
                if ($this->isAlive($host)) {
                    $this->lasthost = 'http://'.$host.$this->postfix;
                    break;
                } else {
                    if(($key = array_search($host, $this->hosts)) !== false) {
                        unset($this->hosts[$key]);
                    }                        
                }
            }
            $res = $this->sendRequest($this->lasthost, $params, $timeout, $funcType, $tryOuts);
        }
        // reset number of requests done
        $this->requestsCount = 0;
        return $res;
    }
    
    /**
     * Check if a peer is alive asking for its status
     * @note first request gets the server status and last available block and second request gets the last block timestamp and compares it with current timestamp. if > 30 minutes means that this peer is not updated
     * @param type $host
     * @return boolean
     */
    private function isAlive($host) {
        $url = 'http://'.$host.$this->postfix;

        $params = array(
            'requestType' => 'getState'
        );

        // cannot use sendRequest because it screws the function (this one is also called inside send request)
        $res = @file_get_contents($url , false, stream_context_create( array(
            'http' => array( 
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params),
                'timeout' => 5
            )
        )));
        if ( ! $res ) { return false; } else { $res = json_decode($res, true); }

        $lastblock = isset($res['lastBlock']) ? $res['lastBlock'] : 0; // last block ID
        $time = isset($res['time']) ? $res['time'] : 0; // node time

        if ( $time == 0 || $lastblock == 0 ) { return false; }

        // get block info

        $params = array(
            'requestType' => 'getBlock',
            'block' => $lastblock,
        );

        // cannot use sendRequest because it screws the function (this one is also called inside send request)
        $res = @file_get_contents($url , false, stream_context_create( array(
            'http' => array( 
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params),
                'timeout' => 5
            )
        )));
        if ( ! $res ) { return false; } else { $res = json_decode($res, true); }
        $blocktime = isset($res['timestamp']) ? $res['timestamp'] : 0;  //block time

        // check diff : block time & node time

        $diff = $time - $blocktime; 
        if ( $diff > 1800 ) { return false; } // 30 min from last block!

        return true;
    }

    /**
     * Validate a NXT token
     * @param type $token
     * @param type $website
     * @return boolean
     */
    public function checkToken($token, $website = '') {
        $website = strlen($website) == 0 ? $_SERVER['SERVER_NAME'] : $website;

         $params = array(
            'requestType' => 'decodeToken',
            'website' => $website,
            'token' => $token
        );
        //STEF to test with localhost: $this->lasthost = "http://127.0.0.1:7876/nxt";
        $res = $this->sendRequest($this->lasthost, $params, 20); // decode token
        if ( ! $res ) { return false; } else { $res = json_decode($res, true); }

        if ( isset($res['accountRS']) && isset($res['valid']) && $res['valid'] == '1' ) {
            $acc = $res['accountRS'];
            return $acc; //return account in Reed Solomon 
        } else { return false; }
    }
    
    /**
     * get data from the RNS
     * @param array $params the configuration array
     * @param bool $decode (default TRUE) if leave response in json format or decode it into a php array
     * @note see: https://wiki.nxtcrypto.org/wiki/Nxt_API for all type of requests
     * @return array the json decoded array (php associative array) with data returned from RNS or false if something went wrong
     * @note check $this->error array for a description of error in case function returns false
     */
    public function requestData($params, $decode = true){
        $res = $this->sendRequest($this->lasthost, $params, 55); 
        if ( ! $res ) {
            $this->error = array(
                'errorCode' => '10000',
                'msg' => "no answer from RNS",
            );
            return false; 
        } else {
            $res1 = json_decode($res, true); 
            if (isset($res1['errorCode'])) {
                $this->error = array(
                    'errorCode' => $res1['errorCode'],
                    'msg' => $res1['errorDescription'],
                );
                return false;
            }
            $res = $decode ? $res1 : $res;
        }
        unset($this->error);
        return $res;
    }
    
    /**
     * retrieve data from an account
     * @param array $options options to restrict results eg:
     * 
     *              $this->getAccountData($ID,$options = array("includeLessors","includeAssets","includeCurrencies"))
     * 
     * @param bool $decode (default TRUE) if leave response in json format or decode it into a php array
     * @note see https://wiki.nxtcrypto.org/wiki/Nxt_API#Get_Account for all parameters
     * @return array the json decoded array (php associative array) with data returned from RNS or false if something went wrong
     * @note check $this->error array for a description of error in case function returns false
     */
    public function getAccountData($ID,$options = array(), $decode = true) {
        $params = array(
            'requestType' => 'getAccount',
            'account' => $ID,
            'includeLessors' => in_array("includeLessors", $options) ? 1 : 'false',
            'includeAssets' => in_array("includeAssets", $options) ? 1 : 'false',
            'includeCurrencies' => in_array("includeCurrencies", $options) ? 1 : 'false',
        );
        return $this->requestData($params, $decode);
    }

    /**
     * retrieve data from an asset
     * @param array $options options to restrict results eg:
     * 
     *              $this->getAssetData($ID,$options = array("includeCounts"))
     * 
     * @param bool $decode (default TRUE) if leave response in json format or decode it into a php array
     * @note see https://wiki.nxtcrypto.org/wiki/Nxt_API#Get_Asset for all parameters
     * @return array the json decoded array (php associative array) with data returned from RNS or false if something went wrong
     * @note check $this->error array for a description of error in case function returns false
     */
    public function getAssetData($ID, $options = array(), $decode = true) {
        $params = array(
            'requestType' => 'getAsset',
            'asset' => $ID,
            'includeCounts' => in_array("includeCounts", $options) ? 1 : 'false'
        );
        return $this->requestData($params, $decode);
    }
    
    /**
     * retrieve data from a transcoded Object (for now: account, asset)
     * @param string $ID Object ID
     * @param string $objType Object Type ("account", "asset")
     * @param array $options options to restrict results eg:
     * 
     *              for accounts: $this->getObjData($ID,"account",$options = array("includeLessors","includeAssets","includeCurrencies"))
     *              for assets: $this->getObjData($ID,"asset",$options = array("includeCounts"))
     * 
     * @param bool $decode (default TRUE) if leave response in json format or decode it into a php array
     * @note see https://wiki.nxtcrypto.org/wiki/Nxt_API#Get_Asset for all parameters
     * @return array the json decoded array (php associative array) with data returned from RNS or false if something went wrong
     * @note check $this->error array for a description of error in case function returns false
     */
    public function getObjData($ID, $objType, $options = array(), $decode = true) {
        switch ($objType) {
            case "account":
                // do minimal query for accounts to make response faster
                $res = $this->getAccountData($ID, $options, $decode);
                break;
            case "asset":
                $res = $this->getAssetData($ID, $options, $decode);
                break;

            default:
                $this->error = array(
                    'errorCode' => '10001',
                    'msg' => "wrong object type (or object type not transcoded by DeBuNe",
                );
                return false;
        }
        return $res;
    }
    
    /**
     * check if the current object (account or asset) exists
     * @note initialize $this->ObjID and $this->ObjType before calling this function
     * @return boolean
     */
    public function currentObjectExists() {
        if (!$this->ObjID || !$this->ObjType) return false;
        
        if (!$res = $this->getObjData($this->ObjID, $this->ObjType)) {
            return false;
        } else {
            return true;
        }
    }
}
