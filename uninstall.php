<?php
if( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) 
	exit();

if ( !class_exists('DeBuNeWPNXT') ) {
    include_once('debune-wp-nxt.php');
}

global $DeBuNeNXT;
if (is_null($DeBuNeNXT)) {
    $DeBuNeNXT = new DeBuNeWPNXT();
}

// Uninstall code goes here

if (is_multisite()) 
{
    global $wpdb;
    $blogs = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs}", ARRAY_A);

    if(!empty($blogs))
    {
        foreach($blogs as $blog) 
        {
            switch_to_blog($blog['blog_id']);
            $DeBuNeNXT->uninstall_debune_wp_nxt();
        }
    }
} else {
    $DeBuNeNXT->uninstall_debune_wp_nxt();
}
